'use strict';

var assign = require('object-assign');
var SDK    = require('./core/sdk');

var NovacastSDK = new SDK({
  EventV1: require('./novacast-event-sdk/client')
});

// also exports the HTTPRequest for external use
NovacastSDK.HTTPRequest = require('./core/request');

module.exports = NovacastSDK;