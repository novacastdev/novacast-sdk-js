'use strict';

var ApiError = function(messages, field_messages) {
  var _msgs = [];
  if (messages) {
    _msgs = isArray(messages) ? messages : [messages];
  }

  this.message = _msgs.join(', ');
  this.messages = _msgs;
  this.field_messages = field_messages;

  if (typeof Error.captureStackTrace === 'function') {
    Error.captureStackTrace(this, this.constructor);
  } else {
    this.stack = (new Error(this.message)).stack;
  }
};

ApiError.prototype = Object.create(Error.prototype);

//
// Private Method
//

function isArray(object) {
  return object && object.constructor === Array ? true : false;
}

module.exports = ApiError;