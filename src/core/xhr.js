'use strict';

var assign = require('object-assign');

function createXhr() {
  if (window.XMLHttpRequest) {
    // use native xhr if supported
    return new XMLHttpRequest();
  } else {
    if (window.ActiveXObject) {
      // use ActiveXObject for <= IE9
      try {
        return new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) { }
      }
    }
  }

  throw new Error('Ajax is not supported by this browser');
}

var XHR = function(method, url) {
  var self = this;

  var _xhr     = createXhr();
  var _headers = {};

  this.onComplete = null;
  this.onError = null;

  this.setHeaders = function(headers) {
    _headers = assign({}, headers);
  };

  this.send = function(data) {
    _xhr.open(method, url);

    // set headers
    for (var key in _headers) {
      if (_headers.hasOwnProperty(key)) _xhr.setRequestHeader(key, _headers[key]);
    }

    _xhr.onload = function() {
      if (self.onComplete) self.onComplete(_xhr.responseText, _xhr.status, _xhr.statusText, _xhr);
    };

    _xhr.onerror = function() {
      if (self.onError) self.onError("Network Error: (" + _xhr.status + ") " + _xhr.statusText);
    };

    if (_xhr.upload) {
      _xhr.upload.onerror = function() {
        if (self.onError) self.onError("XHR Request Error: (" + _xhr.status + ") " + _xhr.statusText);
      };
    }

    data ? _xhr.send(data) : _xhr.send();
  };
};

module.exports = XHR;