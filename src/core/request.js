'use strict';

var URL    = require('url');
var assign = require('object-assign');
var ContentType = require('content-type');

var XHR = require('./xhr');

/**
 * HTTP Request object
 * @param {string} method - http request method
 * @param {string} url    - request url (without query string)
 * @constructor
 */
var HTTPRequest = function(method, url) {
  /**
   * Request query data (will be added to the request as query string)
   * @type {Object.<string, *>}
   * @default
   */
  this.query     = null;
  /**
   * Request custom headers
   * @type {Object.<string, *>}
   * @default
   */
  this.headers   = null;
  /**
   * Request body data (ignore if 'form' property is set)
   * @type {*}
   * @default
   */
  this.body      = null;
  /**
   * Request form data
   * @type {Object.<string, *>}
   * @default
   */
  this.form      = null;
  /**
   * Request content type
   * @type {string}
   * @default
   */
  this.content_type = null;
  /**
   * Additional request options
   * @see {@link https://www.npmjs.com/package/request#request-options-callback}
   * @type {Object.<string, *>}
   * @default
   */
  this.options = {};

  /**
   * Send request success callback
   * @callback HTTPRequest~successCallBack
   * @param {*} response body
   * @param {number} response status code
   * @param {Object} xhr object
   */

  /**
   * Send request error callback
   * @callback HTTPRequest~errorCallBack
   * @param {string} error message
   */

  /**
   * Sends the request
   * @param {HTTPRequest~successCallBack} [success_cb] - success callback
   * @param {HTTPRequest~errorCallBack} [fail_cb] - failure callback
   */
  this.send = function(success_cb, fail_cb) {
    var _method  = method.toUpperCase();
    var _url     = url;
    var _data    = null;
    var _headers = this.headers;
    var _content_type = this.content_type;

    // sets options based upon request properties
    if (this.form != null) {
      // this request contains form data
      // content type will be application/x-www-form-urlencoded or multipart/form-data

      if (this.content_type == "multipart/form-data") {
        // multipart/form-data request
        //
        // Build the form data object
        _data = _buildFormData(this.form);

        // set content type to null
        // this let xhr to add the appropriate content type (because of the Boundary)
        _content_type = null;
      } else {
        // application/x-www-form-urlencoded request
        //
        // URL encode the form
        _data = _buildUrlEncodedForm(this.form);
      }
    } else if (this.body != null) {
      // this request has body data
      _data = this.content_type == 'application/json' ? JSON.stringify(this.body) : this.body;
    }

    // append query string
    if (this.query != null) {
      var parsed_url = URL.parse(_url, true);
      // append query string to the url object
      parsed_url.query = assign({}, parsed_url.query, this.query);
      _url = URL.format(parsed_url);
    }

    // sets request headers by merging the provided custom headers with
    // the content type field
    if (_content_type) {
      _headers = assign({}, this.headers, {
        'Content-Type': _content_type
      });
    }

    //
    // setup and execute the xhr request
    //
    var xhr = new XHR(_method, _url);
    xhr.onComplete = function(body, statusCode, status, xhr) {
      if (success_cb) {
        var response_type = ContentType.parse(xhr.getResponseHeader("Content-Type"));
        var parsed = response_type.type == 'application/json' ? JSON.parse(body) : body;
        success_cb(parsed, statusCode, xhr);
      }
    };
    xhr.onError = function(err) {
      if (fail_cb) fail_cb(err);
    };
    if (_headers) xhr.setHeaders(_headers);
    _data ? xhr.send(_data) : xhr.send();
  };


  //
  // Private methods
  //

  function _buildFormData(form) {
    var _data = new FormData();
    for (var key in form) {
      if (form.hasOwnProperty(key)) {
        var val = form[key];
        if (val instanceof Array) {
          for (var i = 0; i < val.length; i++) {
            _data.append(key + "[]", val[i]);
          }
        } else {
          _data.append(key, val);
        }
      }
    }

    return _data;
  }

  function _buildUrlEncodedForm(form) {
    var encoded = [];
    for (var key in form) {
      if (form.hasOwnProperty(key)) {
        var val = form[key];
        var encoded_key = encodeURIComponent(key);

        if (val instanceof Array) {
          for (var i = 0; i < val.length; i++) {
            encoded.push(encoded_key + "[]=" + encodeURIComponent(val[i]));
          }
        } else {
          encoded.push(encoded_key + "=" + encodeURIComponent(val));
        }
      }
    }

    return encoded.join("&");
  }
};

//
// Exports
//

module.exports = HTTPRequest;