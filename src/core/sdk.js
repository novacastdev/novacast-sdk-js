'use strict';

var assign = require('object-assign');

var SDK = function(clients) {
  var self = this;

  var access_token = null;
  var app_uid      = null;
  var app_secret   = null;

  //
  // Build client constructor
  //
  for (var service in clients) {
    // loop through all the service
    if (clients.hasOwnProperty(service)) {
      // create a namespace for each service
      this[service] = {};

      // loop through all apis within the service
      for (var api in clients[service]) {
        if (api == 'Serializer') {
          //  keep the special 'Serializer' property as is
          this[service][api] = clients[service][api];
        } else if (clients[service].hasOwnProperty(api)) {
          // create an client constructor for each api
          this[service][api] = {
            _build: clients[service][api],
            client: function(host_url, options) {
              var opts = assign({}, options);
              var c = this._build(host_url);

              var token  = opts['access_token'] || self.accessToken();
              var uid    = opts['app_uid']      || self.appUid();
              var secret = opts['app_secret']   || self.appSecret();

              c.setAccessToken(token);
              c.setAppCredential(uid, secret);

              return c;
            }
          };
        }
      }
    }
  }

  //
  // Accessors
  //

  this.setAccessToken = function(token) {
    access_token = token;
  };

  this.accessToken = function() {
    return access_token;
  };

  this.setAppCredential = function(uid, secret) {
    app_uid    = uid;
    app_secret = secret;
  };

  this.appUid = function() {
    return app_uid;
  };

  this.appSecret = function() {
    return app_secret;
  };
};

module.exports = SDK;
