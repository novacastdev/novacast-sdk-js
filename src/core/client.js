'use strict';

var urljoin   = require('url-join');
var Operation = require('./operation');

var Base = function(host_url, base_path) {
  var base_url = urljoin(host_url, base_path);
  var access_token = null;
  var app_uid    = null;
  var app_secret = null;
  var additional_creds = {};

  this.baseUrl = function() {
    return base_url;
  };

  this.setAccessToken = function(token) {
    access_token = token;
  };

  this.accessToken = function() {
    return access_token;
  };

  this.setAppCredential = function(uid, secret) {
    app_uid    = uid;
    app_secret = secret;
  };

  this.appUid = function() {
    return app_uid;
  };

  this.appSecret = function() {
    return app_secret;
  };

  this.setAdditionalCredential = function(cred_name, cred) {
    additional_creds[cred_name] = cred;
  };

  this.getCredential = function(cred_name) {
    return additional_creds[cred_name];
  };

  this.buildOperation = function(path, method) {
    return new Operation(this, path, method);
  };
};

var Client = function(host_url, base_path, api) {
  var ClientInstance = function() {
    Base.call(this, host_url, base_path);
  };

  ClientInstance.prototype = Object.create(api);
  ClientInstance.prototype.constructor = ClientInstance;

  return new ClientInstance();
};

//
// Exports
//

module.exports = Client;