'use strict';

var Deferred    = require('deferred');
var assign      = require('object-assign');
var urljoin     = require('url-join');
var uri_tmpl_parser = require('uri-template');

var HTTPRequest = require('./request');

var Operation = function(client, path, method) {
  var self = this;

  var path_tmpl = uri_tmpl_parser.parse(path);

  // default values
  this.consumes = ['application/json'];
  this.produces = ['application/json'];
  this.method = method || "GET";

  this.params  = null;
  this.headers = null;
  this.query   = null;
  this.body    = null;
  this.form    = null;

  this.auths   = [];

  /**
   * Execute this operation
   * @returns {Promise} a promise object of the request execution
   */
  this.execute = function() {
    var defer = Deferred();
    var req   = _buildRequest();

    req.send(function(body, statusCode) {
      if (statusCode >= 200 && statusCode < 300) {
        defer.resolve({
          body:       body,
          statusCode: statusCode
        });
      } else {
        defer.reject({
          body:       body,
          statusCode: statusCode
        });
      }
    }, function(err) {
      var err_obj = new Error(err);
      err_obj.name = 'NovacastSDK::Client::RequestError';
      defer.reject(err_obj);
    });

    return defer.promise;
  };

  this.url = function() {
    return urljoin(client.baseUrl(), this.targetPath());
  };

  this.targetPath = function() {
    return path_tmpl.expand(this.params);
  };

  //
  // Private Methods
  //

  function _buildRequest() {
    var req  = new HTTPRequest(self.method, self.url());
    var opts = {};

    // determines the request content type
    // application/json has priority over other types
    var content_type = null;
    for (var j = self.consumes.length - 1; j >= 0; j--) {
      content_type = self.consumes[j];
      // stop if we found application/json
      if (content_type == 'application/json') break;
    }

    if (self.form != null) {
      var form_type = null;

      // determines which form content type to use
      for (var i = 0; i < self.consumes.length; i++) {
        switch (self.consumes[i]) {
          case "application/x-www-form-urlencoded":
          case "multipart/form-data":
            form_type = self.consumes[i];
            break;
          default:
            break;
        }

        if (form_type) break;
      }

      // raises error if valid form content type is not found
      if (form_type == null) throw new Error("Operation with form data must consumes either application/x-www-form-urlencoded or multipart/form-data");

      // assign the form data to the request object
      req.form = self.form;
      // overrides the content type with the found form_type
      content_type = form_type;
    } else if (self.body != null) {
      // sets the request body
      req.body = self.body;
    }

    var query_params  = self.query;
    var header_params = self.headers;

    // injects authentication credentials
    for (var k = 0; k < self.auths.length; k++) {
      var auth = self.auths[k];
      var cred = null;

      // retrieve the right credential
      switch (auth.name) {
        case 'accessKey':
          cred = client.accessToken();
          break;
        case 'appSecret':
          cred = client.appUid() + '|' + client.appSecret();
          break;
        case 'previewToken':
          cred = client.getCredential('previewToken');
          break;
        default:
          break;
      }

      if (cred) {
        // adds the credential to either the query or the header
        var cred_hash = {};
        cred_hash[auth.key] = cred;
        if (auth.in_query) query_params = assign({}, query_params, cred_hash);
        else header_params = assign({}, header_params, cred_hash);  
      }
    }

    // sets the request headers if needed
    if (header_params) req.headers = header_params;
    // sets the request query if needed
    if (query_params) req.query = query_params;

    // sets the request content type
    req.content_type = content_type || 'application/json';

    // sets the request options
    req.options = opts;

    return req;
  }
};

//
// Exports
//

module.exports = Operation;