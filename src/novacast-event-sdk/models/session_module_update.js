'use strict';

var Serializer = require('../serializer');


var SessionModuleUpdate = function() {
  
  this.module_config = null;
  
  this.module_items = null;
  
  this.module_name = null;
  
  this.is_enabled = null;
  

  this._serialize = function() {
    return SessionModuleUpdate._serialize(this);
  };
};

SessionModuleUpdate._serialize = function(data) {
  var obj = {};

  
  if (data['module_config'] != undefined) obj['module_config'] = Serializer.serialize(data['module_config'], 'Object');
  
  if (data['module_items'] != undefined) obj['module_items'] = Serializer.serialize(data['module_items'], 'Array[SessionModuleUpdateItem]');
  
  obj['module_name'] = Serializer.serialize(data['module_name'], 'String');
  
  obj['is_enabled'] = Serializer.serialize(data['is_enabled'], 'BOOLEAN');
  

  return obj;
};

SessionModuleUpdate._deserialize = function(data) {
  var obj = new SessionModuleUpdate();

  
  obj.module_config = Serializer.deserialize(data['module_config'], 'Object');
  
  obj.module_items = Serializer.deserialize(data['module_items'], 'Array[SessionModuleUpdateItem]');
  
  obj.module_name = Serializer.deserialize(data['module_name'], 'String');
  
  obj.is_enabled = Serializer.deserialize(data['is_enabled'], 'BOOLEAN');
  

  return obj;
};

module.exports = SessionModuleUpdate;

