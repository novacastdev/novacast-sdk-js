'use strict';

var Serializer = require('../serializer');


var StreamSourceCreateRequest = function() {
  
  this.type = null;
  
  this.url_or_key = null;
  

  this._serialize = function() {
    return StreamSourceCreateRequest._serialize(this);
  };
};

StreamSourceCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['type'] = Serializer.serialize(data['type'], 'String');
  
  obj['url_or_key'] = Serializer.serialize(data['url_or_key'], 'String');
  

  return obj;
};

StreamSourceCreateRequest._deserialize = function(data) {
  var obj = new StreamSourceCreateRequest();

  
  obj.type = Serializer.deserialize(data['type'], 'String');
  
  obj.url_or_key = Serializer.deserialize(data['url_or_key'], 'String');
  

  return obj;
};

module.exports = StreamSourceCreateRequest;

