'use strict';

var Serializer = require('../serializer');


var ActiveQuestionnaire = function() {
  
  this.question_manifest_uid = null;
  
  this.question_set = null;
  
  this.status = null;
  

  this._serialize = function() {
    return ActiveQuestionnaire._serialize(this);
  };
};

ActiveQuestionnaire._serialize = function(data) {
  var obj = {};

  
  obj['question_manifest_uid'] = Serializer.serialize(data['question_manifest_uid'], 'String');
  
  obj['question_set'] = Serializer.serialize(data['question_set'], 'QuestionSet');
  
  obj['status'] = Serializer.serialize(data['status'], 'String');
  

  return obj;
};

ActiveQuestionnaire._deserialize = function(data) {
  var obj = new ActiveQuestionnaire();

  
  obj.question_manifest_uid = Serializer.deserialize(data['question_manifest_uid'], 'String');
  
  obj.question_set = Serializer.deserialize(data['question_set'], 'QuestionSet');
  
  obj.status = Serializer.deserialize(data['status'], 'String');
  

  return obj;
};

module.exports = ActiveQuestionnaire;

