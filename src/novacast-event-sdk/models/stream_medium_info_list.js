'use strict';

var Serializer = require('../serializer');


var StreamMediumInfoList = function() {
  
  this.media = null;
  

  this._serialize = function() {
    return StreamMediumInfoList._serialize(this);
  };
};

StreamMediumInfoList._serialize = function(data) {
  var obj = {};

  
  obj['media'] = Serializer.serialize(data['media'], 'Array[StreamMediumInfo]');
  

  return obj;
};

StreamMediumInfoList._deserialize = function(data) {
  var obj = new StreamMediumInfoList();

  
  obj.media = Serializer.deserialize(data['media'], 'Array[StreamMediumInfo]');
  

  return obj;
};

module.exports = StreamMediumInfoList;

