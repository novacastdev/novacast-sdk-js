'use strict';

var Serializer = require('../serializer');


var ActiveQuestionnaireList = function() {
  
  this.questionnaires = null;
  

  this._serialize = function() {
    return ActiveQuestionnaireList._serialize(this);
  };
};

ActiveQuestionnaireList._serialize = function(data) {
  var obj = {};

  
  obj['questionnaires'] = Serializer.serialize(data['questionnaires'], 'Array[ActiveQuestionnaire]');
  

  return obj;
};

ActiveQuestionnaireList._deserialize = function(data) {
  var obj = new ActiveQuestionnaireList();

  
  obj.questionnaires = Serializer.deserialize(data['questionnaires'], 'Array[ActiveQuestionnaire]');
  

  return obj;
};

module.exports = ActiveQuestionnaireList;

