'use strict';

var Serializer = require('../serializer');


var GetAccountDisplayInfoRequest = function() {
  
  this.account_uids = null;
  

  this._serialize = function() {
    return GetAccountDisplayInfoRequest._serialize(this);
  };
};

GetAccountDisplayInfoRequest._serialize = function(data) {
  var obj = {};

  
  obj['account_uids'] = Serializer.serialize(data['account_uids'], 'Array[String]');
  

  return obj;
};

GetAccountDisplayInfoRequest._deserialize = function(data) {
  var obj = new GetAccountDisplayInfoRequest();

  
  obj.account_uids = Serializer.deserialize(data['account_uids'], 'Array[String]');
  

  return obj;
};

module.exports = GetAccountDisplayInfoRequest;

