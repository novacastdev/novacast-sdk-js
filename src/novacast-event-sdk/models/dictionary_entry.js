'use strict';

var Serializer = require('../serializer');


var DictionaryEntry = function() {
  
  this.locale = null;
  
  this.usage_key = null;
  
  this.category = null;
  
  this.context = null;
  
  this.is_sys = null;
  

  this._serialize = function() {
    return DictionaryEntry._serialize(this);
  };
};

DictionaryEntry._serialize = function(data) {
  var obj = {};

  
  if (data['locale'] != undefined) obj['locale'] = Serializer.serialize(data['locale'], 'String');
  
  if (data['usage_key'] != undefined) obj['usage_key'] = Serializer.serialize(data['usage_key'], 'String');
  
  if (data['category'] != undefined) obj['category'] = Serializer.serialize(data['category'], 'String');
  
  if (data['context'] != undefined) obj['context'] = Serializer.serialize(data['context'], 'String');
  
  if (data['is_sys'] != undefined) obj['is_sys'] = Serializer.serialize(data['is_sys'], 'Integer');
  

  return obj;
};

DictionaryEntry._deserialize = function(data) {
  var obj = new DictionaryEntry();

  
  obj.locale = Serializer.deserialize(data['locale'], 'String');
  
  obj.usage_key = Serializer.deserialize(data['usage_key'], 'String');
  
  obj.category = Serializer.deserialize(data['category'], 'String');
  
  obj.context = Serializer.deserialize(data['context'], 'String');
  
  obj.is_sys = Serializer.deserialize(data['is_sys'], 'Integer');
  

  return obj;
};

module.exports = DictionaryEntry;

