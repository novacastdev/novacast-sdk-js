'use strict';

var Serializer = require('../serializer');


var SlideDeckInfo = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.page_count = null;
  

  this._serialize = function() {
    return SlideDeckInfo._serialize(this);
  };
};

SlideDeckInfo._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['page_count'] = Serializer.serialize(data['page_count'], 'Integer');
  

  return obj;
};

SlideDeckInfo._deserialize = function(data) {
  var obj = new SlideDeckInfo();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.page_count = Serializer.deserialize(data['page_count'], 'Integer');
  

  return obj;
};

module.exports = SlideDeckInfo;

