'use strict';

var Serializer = require('../serializer');


var AccountExtendedList = function() {
  
  this.accounts = null;
  

  this._serialize = function() {
    return AccountExtendedList._serialize(this);
  };
};

AccountExtendedList._serialize = function(data) {
  var obj = {};

  
  obj['accounts'] = Serializer.serialize(data['accounts'], 'Array[AccountExtended]');
  

  return obj;
};

AccountExtendedList._deserialize = function(data) {
  var obj = new AccountExtendedList();

  
  obj.accounts = Serializer.deserialize(data['accounts'], 'Array[AccountExtended]');
  

  return obj;
};

module.exports = AccountExtendedList;

