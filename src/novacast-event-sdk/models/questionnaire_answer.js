'use strict';

var Serializer = require('../serializer');


var QuestionnaireAnswer = function() {
  
  this.question_content_uid = null;
  
  this.question_content_type = null;
  
  this.choice = null;
  

  this._serialize = function() {
    return QuestionnaireAnswer._serialize(this);
  };
};

QuestionnaireAnswer._serialize = function(data) {
  var obj = {};

  
  obj['question_content_uid'] = Serializer.serialize(data['question_content_uid'], 'String');
  
  if (data['question_content_type'] != undefined) obj['question_content_type'] = Serializer.serialize(data['question_content_type'], 'String');
  
  obj['choice'] = Serializer.serialize(data['choice'], 'Object');
  

  return obj;
};

QuestionnaireAnswer._deserialize = function(data) {
  var obj = new QuestionnaireAnswer();

  
  obj.question_content_uid = Serializer.deserialize(data['question_content_uid'], 'String');
  
  obj.question_content_type = Serializer.deserialize(data['question_content_type'], 'String');
  
  obj.choice = Serializer.deserialize(data['choice'], 'Object');
  

  return obj;
};

module.exports = QuestionnaireAnswer;

