'use strict';

var Serializer = require('../serializer');


var ForumPostExtendedList = function() {
  
  this.posts = null;
  

  this._serialize = function() {
    return ForumPostExtendedList._serialize(this);
  };
};

ForumPostExtendedList._serialize = function(data) {
  var obj = {};

  
  obj['posts'] = Serializer.serialize(data['posts'], 'Array[ForumPostExtended]');
  

  return obj;
};

ForumPostExtendedList._deserialize = function(data) {
  var obj = new ForumPostExtendedList();

  
  obj.posts = Serializer.deserialize(data['posts'], 'Array[ForumPostExtended]');
  

  return obj;
};

module.exports = ForumPostExtendedList;

