'use strict';

var Serializer = require('../serializer');


var AccountRoleUpdateRequest = function() {
  
  this.assign = null;
  
  this.delete = null;
  

  this._serialize = function() {
    return AccountRoleUpdateRequest._serialize(this);
  };
};

AccountRoleUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['assign'] != undefined) obj['assign'] = Serializer.serialize(data['assign'], 'Array[AccountResourceRoles]');
  
  if (data['delete'] != undefined) obj['delete'] = Serializer.serialize(data['delete'], 'Array[AccountResourceRoles]');
  

  return obj;
};

AccountRoleUpdateRequest._deserialize = function(data) {
  var obj = new AccountRoleUpdateRequest();

  
  obj.assign = Serializer.deserialize(data['assign'], 'Array[AccountResourceRoles]');
  
  obj.delete = Serializer.deserialize(data['delete'], 'Array[AccountResourceRoles]');
  

  return obj;
};

module.exports = AccountRoleUpdateRequest;

