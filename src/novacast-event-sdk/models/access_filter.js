'use strict';

var Serializer = require('../serializer');


var AccessFilter = function() {
  
  this.name = null;
  
  this.config = null;
  
  this.priority = null;
  
  this.enabled = null;
  

  this._serialize = function() {
    return AccessFilter._serialize(this);
  };
};

AccessFilter._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  if (data['config'] != undefined) obj['config'] = Serializer.serialize(data['config'], 'Object');
  
  if (data['priority'] != undefined) obj['priority'] = Serializer.serialize(data['priority'], 'Integer');
  
  if (data['enabled'] != undefined) obj['enabled'] = Serializer.serialize(data['enabled'], 'BOOLEAN');
  

  return obj;
};

AccessFilter._deserialize = function(data) {
  var obj = new AccessFilter();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  
  obj.priority = Serializer.deserialize(data['priority'], 'Integer');
  
  obj.enabled = Serializer.deserialize(data['enabled'], 'BOOLEAN');
  

  return obj;
};

module.exports = AccessFilter;

