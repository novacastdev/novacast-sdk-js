'use strict';

var Serializer = require('../serializer');


var EventSessionInfoRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return EventSessionInfoRequest._serialize(this);
  };
};

EventSessionInfoRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

EventSessionInfoRequest._deserialize = function(data) {
  var obj = new EventSessionInfoRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = EventSessionInfoRequest;

