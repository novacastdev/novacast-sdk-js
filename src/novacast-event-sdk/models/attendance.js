'use strict';

var Serializer = require('../serializer');


var Attendance = function() {
  
  this.event_uid = null;
  
  this.account_type = null;
  
  this.account_uid = null;
  
  this.client_ip = null;
  
  this.user_agent = null;
  
  this.first_request_at = null;
  
  this.last_request_at = null;
  

  this._serialize = function() {
    return Attendance._serialize(this);
  };
};

Attendance._serialize = function(data) {
  var obj = {};

  
  obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  
  obj['account_type'] = Serializer.serialize(data['account_type'], 'String');
  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  if (data['client_ip'] != undefined) obj['client_ip'] = Serializer.serialize(data['client_ip'], 'String');
  
  if (data['user_agent'] != undefined) obj['user_agent'] = Serializer.serialize(data['user_agent'], 'String');
  
  obj['first_request_at'] = Serializer.serialize(data['first_request_at'], 'DateTime');
  
  obj['last_request_at'] = Serializer.serialize(data['last_request_at'], 'DateTime');
  

  return obj;
};

Attendance._deserialize = function(data) {
  var obj = new Attendance();

  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  
  obj.account_type = Serializer.deserialize(data['account_type'], 'String');
  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.client_ip = Serializer.deserialize(data['client_ip'], 'String');
  
  obj.user_agent = Serializer.deserialize(data['user_agent'], 'String');
  
  obj.first_request_at = Serializer.deserialize(data['first_request_at'], 'DateTime');
  
  obj.last_request_at = Serializer.deserialize(data['last_request_at'], 'DateTime');
  

  return obj;
};

module.exports = Attendance;

