'use strict';

var Serializer = require('../serializer');


var AccountRequest = function() {
  
  this.identifier = null;
  
  this.password = null;
  

  this._serialize = function() {
    return AccountRequest._serialize(this);
  };
};

AccountRequest._serialize = function(data) {
  var obj = {};

  
  obj['identifier'] = Serializer.serialize(data['identifier'], 'String');
  
  obj['password'] = Serializer.serialize(data['password'], 'String');
  

  return obj;
};

AccountRequest._deserialize = function(data) {
  var obj = new AccountRequest();

  
  obj.identifier = Serializer.deserialize(data['identifier'], 'String');
  
  obj.password = Serializer.deserialize(data['password'], 'String');
  

  return obj;
};

module.exports = AccountRequest;

