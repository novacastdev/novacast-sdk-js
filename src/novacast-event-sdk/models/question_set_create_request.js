'use strict';

var Serializer = require('../serializer');


var QuestionSetCreateRequest = function() {
  
  this.label = null;
  
  this.state = null;
  

  this._serialize = function() {
    return QuestionSetCreateRequest._serialize(this);
  };
};

QuestionSetCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['state'] = Serializer.serialize(data['state'], 'String');
  

  return obj;
};

QuestionSetCreateRequest._deserialize = function(data) {
  var obj = new QuestionSetCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.state = Serializer.deserialize(data['state'], 'String');
  

  return obj;
};

module.exports = QuestionSetCreateRequest;

