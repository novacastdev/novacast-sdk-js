'use strict';

var Serializer = require('../serializer');


var PreviewTokenInfo = function() {
  
  this.event_uid = null;
  
  this.expires_at = null;
  
  this.session_ttl = null;
  

  this._serialize = function() {
    return PreviewTokenInfo._serialize(this);
  };
};

PreviewTokenInfo._serialize = function(data) {
  var obj = {};

  
  obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  
  if (data['expires_at'] != undefined) obj['expires_at'] = Serializer.serialize(data['expires_at'], 'DateTime');
  
  obj['session_ttl'] = Serializer.serialize(data['session_ttl'], 'Integer');
  

  return obj;
};

PreviewTokenInfo._deserialize = function(data) {
  var obj = new PreviewTokenInfo();

  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  
  obj.expires_at = Serializer.deserialize(data['expires_at'], 'DateTime');
  
  obj.session_ttl = Serializer.deserialize(data['session_ttl'], 'Integer');
  

  return obj;
};

module.exports = PreviewTokenInfo;

