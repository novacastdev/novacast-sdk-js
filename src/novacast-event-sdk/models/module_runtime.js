'use strict';

var Serializer = require('../serializer');


var ModuleRuntime = function() {
  
  this.module_name = null;
  
  this.runtime = null;
  
  this.timestamp = null;
  

  this._serialize = function() {
    return ModuleRuntime._serialize(this);
  };
};

ModuleRuntime._serialize = function(data) {
  var obj = {};

  
  obj['module_name'] = Serializer.serialize(data['module_name'], 'String');
  
  obj['runtime'] = Serializer.serialize(data['runtime'], 'Object');
  
  if (data['timestamp'] != undefined) obj['timestamp'] = Serializer.serialize(data['timestamp'], 'DateTime');
  

  return obj;
};

ModuleRuntime._deserialize = function(data) {
  var obj = new ModuleRuntime();

  
  obj.module_name = Serializer.deserialize(data['module_name'], 'String');
  
  obj.runtime = Serializer.deserialize(data['runtime'], 'Object');
  
  obj.timestamp = Serializer.deserialize(data['timestamp'], 'DateTime');
  

  return obj;
};

module.exports = ModuleRuntime;

