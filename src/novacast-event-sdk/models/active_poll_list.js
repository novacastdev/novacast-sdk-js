'use strict';

var Serializer = require('../serializer');


var ActivePollList = function() {
  
  this.polls = null;
  

  this._serialize = function() {
    return ActivePollList._serialize(this);
  };
};

ActivePollList._serialize = function(data) {
  var obj = {};

  
  obj['polls'] = Serializer.serialize(data['polls'], 'Array[ActivePoll]');
  

  return obj;
};

ActivePollList._deserialize = function(data) {
  var obj = new ActivePollList();

  
  obj.polls = Serializer.deserialize(data['polls'], 'Array[ActivePoll]');
  

  return obj;
};

module.exports = ActivePollList;

