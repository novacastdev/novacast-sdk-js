'use strict';

var Serializer = require('../serializer');


var DictionaryFilterResponse = function() {
  
  this.entries = null;
  

  this._serialize = function() {
    return DictionaryFilterResponse._serialize(this);
  };
};

DictionaryFilterResponse._serialize = function(data) {
  var obj = {};

  
  if (data['entries'] != undefined) obj['entries'] = Serializer.serialize(data['entries'], 'Array[DictionaryEntry]');
  

  return obj;
};

DictionaryFilterResponse._deserialize = function(data) {
  var obj = new DictionaryFilterResponse();

  
  obj.entries = Serializer.deserialize(data['entries'], 'Array[DictionaryEntry]');
  

  return obj;
};

module.exports = DictionaryFilterResponse;

