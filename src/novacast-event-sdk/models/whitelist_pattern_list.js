'use strict';

var Serializer = require('../serializer');


var WhitelistPatternList = function() {
  
  this.patterns = null;
  

  this._serialize = function() {
    return WhitelistPatternList._serialize(this);
  };
};

WhitelistPatternList._serialize = function(data) {
  var obj = {};

  
  if (data['patterns'] != undefined) obj['patterns'] = Serializer.serialize(data['patterns'], 'Array[WhitelistPattern]');
  

  return obj;
};

WhitelistPatternList._deserialize = function(data) {
  var obj = new WhitelistPatternList();

  
  obj.patterns = Serializer.deserialize(data['patterns'], 'Array[WhitelistPattern]');
  

  return obj;
};

module.exports = WhitelistPatternList;

