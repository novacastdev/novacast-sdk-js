'use strict';

var Serializer = require('../serializer');


var EnrollmentField = function() {
  
  this.name = null;
  
  this.data_type = null;
  
  this.label = null;
  
  this.optional = null;
  
  this.index = null;
  

  this._serialize = function() {
    return EnrollmentField._serialize(this);
  };
};

EnrollmentField._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  obj['data_type'] = Serializer.serialize(data['data_type'], 'String');
  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['optional'] = Serializer.serialize(data['optional'], 'BOOLEAN');
  
  obj['index'] = Serializer.serialize(data['index'], 'Integer');
  

  return obj;
};

EnrollmentField._deserialize = function(data) {
  var obj = new EnrollmentField();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.data_type = Serializer.deserialize(data['data_type'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.optional = Serializer.deserialize(data['optional'], 'BOOLEAN');
  
  obj.index = Serializer.deserialize(data['index'], 'Integer');
  

  return obj;
};

module.exports = EnrollmentField;

