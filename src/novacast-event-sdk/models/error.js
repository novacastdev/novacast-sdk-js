'use strict';

var Serializer = require('../serializer');


var Error = function() {
  
  this.klass_name = null;
  
  this.messages = null;
  
  this.fields = null;
  

  this._serialize = function() {
    return Error._serialize(this);
  };
};

Error._serialize = function(data) {
  var obj = {};

  
  obj['klass_name'] = Serializer.serialize(data['klass_name'], 'String');
  
  obj['messages'] = Serializer.serialize(data['messages'], 'Array[String]');
  
  if (data['fields'] != undefined) obj['fields'] = Serializer.serialize(data['fields'], 'Array[ErrorField]');
  

  return obj;
};

Error._deserialize = function(data) {
  var obj = new Error();

  
  obj.klass_name = Serializer.deserialize(data['klass_name'], 'String');
  
  obj.messages = Serializer.deserialize(data['messages'], 'Array[String]');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Array[ErrorField]');
  

  return obj;
};

module.exports = Error;

