'use strict';

var Serializer = require('../serializer');


var SlideUpdateRequest = function() {
  
  this.title = null;
  

  this._serialize = function() {
    return SlideUpdateRequest._serialize(this);
  };
};

SlideUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['title'] != undefined) obj['title'] = Serializer.serialize(data['title'], 'String');
  

  return obj;
};

SlideUpdateRequest._deserialize = function(data) {
  var obj = new SlideUpdateRequest();

  
  obj.title = Serializer.deserialize(data['title'], 'String');
  

  return obj;
};

module.exports = SlideUpdateRequest;

