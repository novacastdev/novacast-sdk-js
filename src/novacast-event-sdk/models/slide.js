'use strict';

var Serializer = require('../serializer');


var Slide = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.page = null;
  
  this.url = null;
  
  this.thumb_url = null;
  
  this.title = null;
  

  this._serialize = function() {
    return Slide._serialize(this);
  };
};

Slide._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['page'] = Serializer.serialize(data['page'], 'Integer');
  
  obj['url'] = Serializer.serialize(data['url'], 'String');
  
  if (data['thumb_url'] != undefined) obj['thumb_url'] = Serializer.serialize(data['thumb_url'], 'String');
  
  if (data['title'] != undefined) obj['title'] = Serializer.serialize(data['title'], 'String');
  

  return obj;
};

Slide._deserialize = function(data) {
  var obj = new Slide();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.page = Serializer.deserialize(data['page'], 'Integer');
  
  obj.url = Serializer.deserialize(data['url'], 'String');
  
  obj.thumb_url = Serializer.deserialize(data['thumb_url'], 'String');
  
  obj.title = Serializer.deserialize(data['title'], 'String');
  

  return obj;
};

module.exports = Slide;

