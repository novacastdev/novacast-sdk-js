'use strict';

var Serializer = require('../serializer');


var ManifestSubmissions = function() {
  
  this.submissions = null;
  
  this.question_manifest = null;
  

  this._serialize = function() {
    return ManifestSubmissions._serialize(this);
  };
};

ManifestSubmissions._serialize = function(data) {
  var obj = {};

  
  obj['submissions'] = Serializer.serialize(data['submissions'], 'Array[QuestionSubmission]');
  
  if (data['question_manifest'] != undefined) obj['question_manifest'] = Serializer.serialize(data['question_manifest'], 'QuestionManifest');
  

  return obj;
};

ManifestSubmissions._deserialize = function(data) {
  var obj = new ManifestSubmissions();

  
  obj.submissions = Serializer.deserialize(data['submissions'], 'Array[QuestionSubmission]');
  
  obj.question_manifest = Serializer.deserialize(data['question_manifest'], 'QuestionManifest');
  

  return obj;
};

module.exports = ManifestSubmissions;

