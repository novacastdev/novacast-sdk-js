'use strict';

var Serializer = require('../serializer');


var WhitelistRequest = function() {
  
  this.enrollment_field = null;
  
  this.pattern_type = null;
  
  this.patterns = null;
  

  this._serialize = function() {
    return WhitelistRequest._serialize(this);
  };
};

WhitelistRequest._serialize = function(data) {
  var obj = {};

  
  obj['enrollment_field'] = Serializer.serialize(data['enrollment_field'], 'String');
  
  obj['pattern_type'] = Serializer.serialize(data['pattern_type'], 'String');
  
  obj['patterns'] = Serializer.serialize(data['patterns'], 'Array[String]');
  

  return obj;
};

WhitelistRequest._deserialize = function(data) {
  var obj = new WhitelistRequest();

  
  obj.enrollment_field = Serializer.deserialize(data['enrollment_field'], 'String');
  
  obj.pattern_type = Serializer.deserialize(data['pattern_type'], 'String');
  
  obj.patterns = Serializer.deserialize(data['patterns'], 'Array[String]');
  

  return obj;
};

module.exports = WhitelistRequest;

