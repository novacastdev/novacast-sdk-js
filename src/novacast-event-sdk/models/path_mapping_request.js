'use strict';

var Serializer = require('../serializer');


var PathMappingRequest = function() {
  
  this.path = null;
  
  this.usage = null;
  
  this.path_mappings = null;
  

  this._serialize = function() {
    return PathMappingRequest._serialize(this);
  };
};

PathMappingRequest._serialize = function(data) {
  var obj = {};

  
  if (data['path'] != undefined) obj['path'] = Serializer.serialize(data['path'], 'String');
  
  if (data['usage'] != undefined) obj['usage'] = Serializer.serialize(data['usage'], 'String');
  
  obj['path_mappings'] = Serializer.serialize(data['path_mappings'], 'Array[PathMapping]');
  

  return obj;
};

PathMappingRequest._deserialize = function(data) {
  var obj = new PathMappingRequest();

  
  obj.path = Serializer.deserialize(data['path'], 'String');
  
  obj.usage = Serializer.deserialize(data['usage'], 'String');
  
  obj.path_mappings = Serializer.deserialize(data['path_mappings'], 'Array[PathMapping]');
  

  return obj;
};

module.exports = PathMappingRequest;

