'use strict';

var Serializer = require('../serializer');


var PreviewToken = function() {
  
  this.event_uid = null;
  
  this.expires_at = null;
  
  this.session_ttl = null;
  
  this.token = null;
  

  this._serialize = function() {
    return PreviewToken._serialize(this);
  };
};

PreviewToken._serialize = function(data) {
  var obj = {};

  
  obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  
  if (data['expires_at'] != undefined) obj['expires_at'] = Serializer.serialize(data['expires_at'], 'DateTime');
  
  obj['session_ttl'] = Serializer.serialize(data['session_ttl'], 'Integer');
  
  obj['token'] = Serializer.serialize(data['token'], 'String');
  

  return obj;
};

PreviewToken._deserialize = function(data) {
  var obj = new PreviewToken();

  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  
  obj.expires_at = Serializer.deserialize(data['expires_at'], 'DateTime');
  
  obj.session_ttl = Serializer.deserialize(data['session_ttl'], 'Integer');
  
  obj.token = Serializer.deserialize(data['token'], 'String');
  

  return obj;
};

module.exports = PreviewToken;

