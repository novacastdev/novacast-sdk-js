'use strict';

var Serializer = require('../serializer');


var SlideDeleteRequest = function() {
  
  this.slide_uids = null;
  

  this._serialize = function() {
    return SlideDeleteRequest._serialize(this);
  };
};

SlideDeleteRequest._serialize = function(data) {
  var obj = {};

  
  obj['slide_uids'] = Serializer.serialize(data['slide_uids'], 'Array[String]');
  

  return obj;
};

SlideDeleteRequest._deserialize = function(data) {
  var obj = new SlideDeleteRequest();

  
  obj.slide_uids = Serializer.deserialize(data['slide_uids'], 'Array[String]');
  

  return obj;
};

module.exports = SlideDeleteRequest;

