'use strict';

var Serializer = require('../serializer');


var SessionModuleUpdateRequest = function() {
  
  this.modules = null;
  

  this._serialize = function() {
    return SessionModuleUpdateRequest._serialize(this);
  };
};

SessionModuleUpdateRequest._serialize = function(data) {
  var obj = {};

  
  obj['modules'] = Serializer.serialize(data['modules'], 'Array[SessionModuleUpdate]');
  

  return obj;
};

SessionModuleUpdateRequest._deserialize = function(data) {
  var obj = new SessionModuleUpdateRequest();

  
  obj.modules = Serializer.deserialize(data['modules'], 'Array[SessionModuleUpdate]');
  

  return obj;
};

module.exports = SessionModuleUpdateRequest;

