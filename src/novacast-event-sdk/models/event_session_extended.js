'use strict';

var Serializer = require('../serializer');


var EventSessionExtended = function() {
  
  this.uid = null;
  
  this.label = null;
  
  this.event_uid = null;
  
  this.event_name = null;
  
  this.is_default = null;
  
  this.modules = null;
  

  this._serialize = function() {
    return EventSessionExtended._serialize(this);
  };
};

EventSessionExtended._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  
  obj['event_name'] = Serializer.serialize(data['event_name'], 'String');
  
  obj['is_default'] = Serializer.serialize(data['is_default'], 'BOOLEAN');
  
  obj['modules'] = Serializer.serialize(data['modules'], 'Array[SessionModule]');
  

  return obj;
};

EventSessionExtended._deserialize = function(data) {
  var obj = new EventSessionExtended();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  
  obj.event_name = Serializer.deserialize(data['event_name'], 'String');
  
  obj.is_default = Serializer.deserialize(data['is_default'], 'BOOLEAN');
  
  obj.modules = Serializer.deserialize(data['modules'], 'Array[SessionModule]');
  

  return obj;
};

module.exports = EventSessionExtended;

