'use strict';

var Serializer = require('../serializer');


var StreamSource = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.url = null;
  
  this.uri = null;
  
  this.source = null;
  

  this._serialize = function() {
    return StreamSource._serialize(this);
  };
};

StreamSource._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['url'] = Serializer.serialize(data['url'], 'String');
  
  if (data['uri'] != undefined) obj['uri'] = Serializer.serialize(data['uri'], 'String');
  
  obj['source'] = Serializer.serialize(data['source'], 'String');
  

  return obj;
};

StreamSource._deserialize = function(data) {
  var obj = new StreamSource();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.url = Serializer.deserialize(data['url'], 'String');
  
  obj.uri = Serializer.deserialize(data['uri'], 'String');
  
  obj.source = Serializer.deserialize(data['source'], 'String');
  

  return obj;
};

module.exports = StreamSource;

