'use strict';

var Serializer = require('../serializer');


var UserSetExtended = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.channel_uid = null;
  
  this.ch_acct_only = null;
  
  this.whitelisted_only = null;
  
  this.full_enrollment = null;
  
  this.passcode = null;
  
  this.auth_provider = null;
  
  this.fields = null;
  

  this._serialize = function() {
    return UserSetExtended._serialize(this);
  };
};

UserSetExtended._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['channel_uid'] = Serializer.serialize(data['channel_uid'], 'String');
  
  obj['ch_acct_only'] = Serializer.serialize(data['ch_acct_only'], 'BOOLEAN');
  
  obj['whitelisted_only'] = Serializer.serialize(data['whitelisted_only'], 'BOOLEAN');
  
  obj['full_enrollment'] = Serializer.serialize(data['full_enrollment'], 'BOOLEAN');
  
  if (data['passcode'] != undefined) obj['passcode'] = Serializer.serialize(data['passcode'], 'String');
  
  if (data['auth_provider'] != undefined) obj['auth_provider'] = Serializer.serialize(data['auth_provider'], 'String');
  
  obj['fields'] = Serializer.serialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

UserSetExtended._deserialize = function(data) {
  var obj = new UserSetExtended();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.channel_uid = Serializer.deserialize(data['channel_uid'], 'String');
  
  obj.ch_acct_only = Serializer.deserialize(data['ch_acct_only'], 'BOOLEAN');
  
  obj.whitelisted_only = Serializer.deserialize(data['whitelisted_only'], 'BOOLEAN');
  
  obj.full_enrollment = Serializer.deserialize(data['full_enrollment'], 'BOOLEAN');
  
  obj.passcode = Serializer.deserialize(data['passcode'], 'String');
  
  obj.auth_provider = Serializer.deserialize(data['auth_provider'], 'String');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

module.exports = UserSetExtended;

