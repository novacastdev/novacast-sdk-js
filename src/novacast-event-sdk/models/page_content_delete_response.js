'use strict';

var Serializer = require('../serializer');


var PageContentDeleteResponse = function() {
  
  this.page_content = null;
  
  this.event_page = null;
  

  this._serialize = function() {
    return PageContentDeleteResponse._serialize(this);
  };
};

PageContentDeleteResponse._serialize = function(data) {
  var obj = {};

  
  obj['page_content'] = Serializer.serialize(data['page_content'], 'PageContent');
  
  obj['event_page'] = Serializer.serialize(data['event_page'], 'EventPage');
  

  return obj;
};

PageContentDeleteResponse._deserialize = function(data) {
  var obj = new PageContentDeleteResponse();

  
  obj.page_content = Serializer.deserialize(data['page_content'], 'PageContent');
  
  obj.event_page = Serializer.deserialize(data['event_page'], 'EventPage');
  

  return obj;
};

module.exports = PageContentDeleteResponse;

