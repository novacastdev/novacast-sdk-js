'use strict';

var Serializer = require('../serializer');


var DictionaryQueryResponse = function() {
  
  this.dictionary = null;
  
  this.locale = null;
  

  this._serialize = function() {
    return DictionaryQueryResponse._serialize(this);
  };
};

DictionaryQueryResponse._serialize = function(data) {
  var obj = {};

  
  if (data['dictionary'] != undefined) obj['dictionary'] = Serializer.serialize(data['dictionary'], 'Object');
  
  if (data['locale'] != undefined) obj['locale'] = Serializer.serialize(data['locale'], 'String');
  

  return obj;
};

DictionaryQueryResponse._deserialize = function(data) {
  var obj = new DictionaryQueryResponse();

  
  obj.dictionary = Serializer.deserialize(data['dictionary'], 'Object');
  
  obj.locale = Serializer.deserialize(data['locale'], 'String');
  

  return obj;
};

module.exports = DictionaryQueryResponse;

