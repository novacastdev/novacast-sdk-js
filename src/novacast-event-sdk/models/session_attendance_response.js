'use strict';

var Serializer = require('../serializer');


var SessionAttendanceResponse = function() {
  
  this.session_attendances = null;
  
  this.attendances = null;
  
  this.account_info = null;
  
  this.session = null;
  

  this._serialize = function() {
    return SessionAttendanceResponse._serialize(this);
  };
};

SessionAttendanceResponse._serialize = function(data) {
  var obj = {};

  
  obj['session_attendances'] = Serializer.serialize(data['session_attendances'], 'Array[SessionAttendance]');
  
  if (data['attendances'] != undefined) obj['attendances'] = Serializer.serialize(data['attendances'], 'Array[Attendance]');
  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  
  if (data['session'] != undefined) obj['session'] = Serializer.serialize(data['session'], 'EventSession');
  

  return obj;
};

SessionAttendanceResponse._deserialize = function(data) {
  var obj = new SessionAttendanceResponse();

  
  obj.session_attendances = Serializer.deserialize(data['session_attendances'], 'Array[SessionAttendance]');
  
  obj.attendances = Serializer.deserialize(data['attendances'], 'Array[Attendance]');
  
  obj.account_info = Serializer.deserialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  
  obj.session = Serializer.deserialize(data['session'], 'EventSession');
  

  return obj;
};

module.exports = SessionAttendanceResponse;

