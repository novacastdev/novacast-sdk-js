'use strict';

var Serializer = require('../serializer');


var QuestionContentType = function() {
  
  this.name = null;
  
  this.desc = null;
  
  this.type_key = null;
  
  this.params = null;
  

  this._serialize = function() {
    return QuestionContentType._serialize(this);
  };
};

QuestionContentType._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  if (data['desc'] != undefined) obj['desc'] = Serializer.serialize(data['desc'], 'String');
  
  obj['type_key'] = Serializer.serialize(data['type_key'], 'String');
  
  obj['params'] = Serializer.serialize(data['params'], 'Object');
  

  return obj;
};

QuestionContentType._deserialize = function(data) {
  var obj = new QuestionContentType();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.desc = Serializer.deserialize(data['desc'], 'String');
  
  obj.type_key = Serializer.deserialize(data['type_key'], 'String');
  
  obj.params = Serializer.deserialize(data['params'], 'Object');
  

  return obj;
};

module.exports = QuestionContentType;

