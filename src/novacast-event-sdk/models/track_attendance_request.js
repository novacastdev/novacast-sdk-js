'use strict';

var Serializer = require('../serializer');


var TrackAttendanceRequest = function() {
  
  this.account_uid = null;
  
  this.client_session_id = null;
  
  this.client_ip = null;
  
  this.user_agent = null;
  
  this.session_uid = null;
  

  this._serialize = function() {
    return TrackAttendanceRequest._serialize(this);
  };
};

TrackAttendanceRequest._serialize = function(data) {
  var obj = {};

  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  obj['client_session_id'] = Serializer.serialize(data['client_session_id'], 'String');
  
  if (data['client_ip'] != undefined) obj['client_ip'] = Serializer.serialize(data['client_ip'], 'String');
  
  if (data['user_agent'] != undefined) obj['user_agent'] = Serializer.serialize(data['user_agent'], 'String');
  
  if (data['session_uid'] != undefined) obj['session_uid'] = Serializer.serialize(data['session_uid'], 'String');
  

  return obj;
};

TrackAttendanceRequest._deserialize = function(data) {
  var obj = new TrackAttendanceRequest();

  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.client_session_id = Serializer.deserialize(data['client_session_id'], 'String');
  
  obj.client_ip = Serializer.deserialize(data['client_ip'], 'String');
  
  obj.user_agent = Serializer.deserialize(data['user_agent'], 'String');
  
  obj.session_uid = Serializer.deserialize(data['session_uid'], 'String');
  

  return obj;
};

module.exports = TrackAttendanceRequest;

