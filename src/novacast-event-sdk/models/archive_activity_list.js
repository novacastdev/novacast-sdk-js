'use strict';

var Serializer = require('../serializer');


var ArchiveActivityList = function() {
  
  this.activities = null;
  

  this._serialize = function() {
    return ArchiveActivityList._serialize(this);
  };
};

ArchiveActivityList._serialize = function(data) {
  var obj = {};

  
  obj['activities'] = Serializer.serialize(data['activities'], 'Array[ArchiveActivity]');
  

  return obj;
};

ArchiveActivityList._deserialize = function(data) {
  var obj = new ArchiveActivityList();

  
  obj.activities = Serializer.deserialize(data['activities'], 'Array[ArchiveActivity]');
  

  return obj;
};

module.exports = ArchiveActivityList;

