'use strict';

var Serializer = require('../serializer');


var SlideDeckInfoList = function() {
  
  this.decks = null;
  

  this._serialize = function() {
    return SlideDeckInfoList._serialize(this);
  };
};

SlideDeckInfoList._serialize = function(data) {
  var obj = {};

  
  obj['decks'] = Serializer.serialize(data['decks'], 'Array[SlideDeckInfo]');
  

  return obj;
};

SlideDeckInfoList._deserialize = function(data) {
  var obj = new SlideDeckInfoList();

  
  obj.decks = Serializer.deserialize(data['decks'], 'Array[SlideDeckInfo]');
  

  return obj;
};

module.exports = SlideDeckInfoList;

