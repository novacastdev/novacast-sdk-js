'use strict';

var Serializer = require('../serializer');


var QuestionContentList = function() {
  
  this.contents = null;
  

  this._serialize = function() {
    return QuestionContentList._serialize(this);
  };
};

QuestionContentList._serialize = function(data) {
  var obj = {};

  
  obj['contents'] = Serializer.serialize(data['contents'], 'Array[QuestionContent]');
  

  return obj;
};

QuestionContentList._deserialize = function(data) {
  var obj = new QuestionContentList();

  
  obj.contents = Serializer.deserialize(data['contents'], 'Array[QuestionContent]');
  

  return obj;
};

module.exports = QuestionContentList;

