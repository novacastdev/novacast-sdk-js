'use strict';

var Serializer = require('../serializer');


var SessionModuleUpdateConfig = function() {
  
  this.module_config = null;
  
  this.module_items = null;
  

  this._serialize = function() {
    return SessionModuleUpdateConfig._serialize(this);
  };
};

SessionModuleUpdateConfig._serialize = function(data) {
  var obj = {};

  
  if (data['module_config'] != undefined) obj['module_config'] = Serializer.serialize(data['module_config'], 'Object');
  
  if (data['module_items'] != undefined) obj['module_items'] = Serializer.serialize(data['module_items'], 'Array[SessionModuleUpdateItem]');
  

  return obj;
};

SessionModuleUpdateConfig._deserialize = function(data) {
  var obj = new SessionModuleUpdateConfig();

  
  obj.module_config = Serializer.deserialize(data['module_config'], 'Object');
  
  obj.module_items = Serializer.deserialize(data['module_items'], 'Array[SessionModuleUpdateItem]');
  

  return obj;
};

module.exports = SessionModuleUpdateConfig;

