'use strict';

var Serializer = require('../serializer');


var EventSessionCommand = function() {
  
  this.command = null;
  
  this.timestamp = null;
  
  this.payload = null;
  

  this._serialize = function() {
    return EventSessionCommand._serialize(this);
  };
};

EventSessionCommand._serialize = function(data) {
  var obj = {};

  
  obj['command'] = Serializer.serialize(data['command'], 'String');
  
  if (data['timestamp'] != undefined) obj['timestamp'] = Serializer.serialize(data['timestamp'], 'DateTime');
  
  obj['payload'] = Serializer.serialize(data['payload'], 'Object');
  

  return obj;
};

EventSessionCommand._deserialize = function(data) {
  var obj = new EventSessionCommand();

  
  obj.command = Serializer.deserialize(data['command'], 'String');
  
  obj.timestamp = Serializer.deserialize(data['timestamp'], 'DateTime');
  
  obj.payload = Serializer.deserialize(data['payload'], 'Object');
  

  return obj;
};

module.exports = EventSessionCommand;

