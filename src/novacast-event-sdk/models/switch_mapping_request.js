'use strict';

var Serializer = require('../serializer');


var SwitchMappingRequest = function() {
  
  this.new_active_uid = null;
  

  this._serialize = function() {
    return SwitchMappingRequest._serialize(this);
  };
};

SwitchMappingRequest._serialize = function(data) {
  var obj = {};

  
  obj['new_active_uid'] = Serializer.serialize(data['new_active_uid'], 'String');
  

  return obj;
};

SwitchMappingRequest._deserialize = function(data) {
  var obj = new SwitchMappingRequest();

  
  obj.new_active_uid = Serializer.deserialize(data['new_active_uid'], 'String');
  

  return obj;
};

module.exports = SwitchMappingRequest;

