'use strict';

var Serializer = require('../serializer');


var Enrollment = function() {
  
  this.account_type = null;
  
  this.account_uid = null;
  
  this.revoked = null;
  

  this._serialize = function() {
    return Enrollment._serialize(this);
  };
};

Enrollment._serialize = function(data) {
  var obj = {};

  
  obj['account_type'] = Serializer.serialize(data['account_type'], 'String');
  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  if (data['revoked'] != undefined) obj['revoked'] = Serializer.serialize(data['revoked'], 'BOOLEAN');
  

  return obj;
};

Enrollment._deserialize = function(data) {
  var obj = new Enrollment();

  
  obj.account_type = Serializer.deserialize(data['account_type'], 'String');
  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.revoked = Serializer.deserialize(data['revoked'], 'BOOLEAN');
  

  return obj;
};

module.exports = Enrollment;

