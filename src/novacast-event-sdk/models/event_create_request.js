'use strict';

var Serializer = require('../serializer');


var EventCreateRequest = function() {
  
  this.name = null;
  
  this.access_policy_uid = null;
  
  this.asset_bundle_uid = null;
  
  this.user_set_uid = null;
  
  this.public_alias_path = null;
  
  this.template_name = null;
  

  this._serialize = function() {
    return EventCreateRequest._serialize(this);
  };
};

EventCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  if (data['access_policy_uid'] != undefined) obj['access_policy_uid'] = Serializer.serialize(data['access_policy_uid'], 'String');
  
  if (data['asset_bundle_uid'] != undefined) obj['asset_bundle_uid'] = Serializer.serialize(data['asset_bundle_uid'], 'String');
  
  if (data['user_set_uid'] != undefined) obj['user_set_uid'] = Serializer.serialize(data['user_set_uid'], 'String');
  
  if (data['public_alias_path'] != undefined) obj['public_alias_path'] = Serializer.serialize(data['public_alias_path'], 'String');
  
  if (data['template_name'] != undefined) obj['template_name'] = Serializer.serialize(data['template_name'], 'String');
  

  return obj;
};

EventCreateRequest._deserialize = function(data) {
  var obj = new EventCreateRequest();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.access_policy_uid = Serializer.deserialize(data['access_policy_uid'], 'String');
  
  obj.asset_bundle_uid = Serializer.deserialize(data['asset_bundle_uid'], 'String');
  
  obj.user_set_uid = Serializer.deserialize(data['user_set_uid'], 'String');
  
  obj.public_alias_path = Serializer.deserialize(data['public_alias_path'], 'String');
  
  obj.template_name = Serializer.deserialize(data['template_name'], 'String');
  

  return obj;
};

module.exports = EventCreateRequest;

