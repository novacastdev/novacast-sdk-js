'use strict';

var Serializer = require('../serializer');


var SlideOrderRequest = function() {
  
  this.ordered_uids = null;
  

  this._serialize = function() {
    return SlideOrderRequest._serialize(this);
  };
};

SlideOrderRequest._serialize = function(data) {
  var obj = {};

  
  obj['ordered_uids'] = Serializer.serialize(data['ordered_uids'], 'Array[String]');
  

  return obj;
};

SlideOrderRequest._deserialize = function(data) {
  var obj = new SlideOrderRequest();

  
  obj.ordered_uids = Serializer.deserialize(data['ordered_uids'], 'Array[String]');
  

  return obj;
};

module.exports = SlideOrderRequest;

