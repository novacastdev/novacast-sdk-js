'use strict';

var Serializer = require('../serializer');


var QuestionSetUpdateRequest = function() {
  
  this.label = null;
  
  this.state = null;
  

  this._serialize = function() {
    return QuestionSetUpdateRequest._serialize(this);
  };
};

QuestionSetUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['state'] != undefined) obj['state'] = Serializer.serialize(data['state'], 'String');
  

  return obj;
};

QuestionSetUpdateRequest._deserialize = function(data) {
  var obj = new QuestionSetUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.state = Serializer.deserialize(data['state'], 'String');
  

  return obj;
};

module.exports = QuestionSetUpdateRequest;

