'use strict';

var Serializer = require('../serializer');


var ForumPostUpdateRequest = function() {
  
  this.content = null;
  
  this.approved = null;
  
  this.hidden = null;
  
  this.deleted = null;
  
  this.starred = null;
  

  this._serialize = function() {
    return ForumPostUpdateRequest._serialize(this);
  };
};

ForumPostUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['content'] != undefined) obj['content'] = Serializer.serialize(data['content'], 'String');
  
  if (data['approved'] != undefined) obj['approved'] = Serializer.serialize(data['approved'], 'BOOLEAN');
  
  if (data['hidden'] != undefined) obj['hidden'] = Serializer.serialize(data['hidden'], 'BOOLEAN');
  
  if (data['deleted'] != undefined) obj['deleted'] = Serializer.serialize(data['deleted'], 'BOOLEAN');
  
  if (data['starred'] != undefined) obj['starred'] = Serializer.serialize(data['starred'], 'BOOLEAN');
  

  return obj;
};

ForumPostUpdateRequest._deserialize = function(data) {
  var obj = new ForumPostUpdateRequest();

  
  obj.content = Serializer.deserialize(data['content'], 'String');
  
  obj.approved = Serializer.deserialize(data['approved'], 'BOOLEAN');
  
  obj.hidden = Serializer.deserialize(data['hidden'], 'BOOLEAN');
  
  obj.deleted = Serializer.deserialize(data['deleted'], 'BOOLEAN');
  
  obj.starred = Serializer.deserialize(data['starred'], 'BOOLEAN');
  

  return obj;
};

module.exports = ForumPostUpdateRequest;

