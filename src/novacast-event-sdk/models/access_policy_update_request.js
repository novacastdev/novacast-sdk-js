'use strict';

var Serializer = require('../serializer');


var AccessPolicyUpdateRequest = function() {
  
  this.label = null;
  
  this.filters = null;
  

  this._serialize = function() {
    return AccessPolicyUpdateRequest._serialize(this);
  };
};

AccessPolicyUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['filters'] != undefined) obj['filters'] = Serializer.serialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

AccessPolicyUpdateRequest._deserialize = function(data) {
  var obj = new AccessPolicyUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.filters = Serializer.deserialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

module.exports = AccessPolicyUpdateRequest;

