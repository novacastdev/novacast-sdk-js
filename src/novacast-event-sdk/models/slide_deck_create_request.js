'use strict';

var Serializer = require('../serializer');


var SlideDeckCreateRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return SlideDeckCreateRequest._serialize(this);
  };
};

SlideDeckCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

SlideDeckCreateRequest._deserialize = function(data) {
  var obj = new SlideDeckCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = SlideDeckCreateRequest;

