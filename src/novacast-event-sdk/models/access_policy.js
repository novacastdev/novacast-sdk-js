'use strict';

var Serializer = require('../serializer');


var AccessPolicy = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  

  this._serialize = function() {
    return AccessPolicy._serialize(this);
  };
};

AccessPolicy._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

AccessPolicy._deserialize = function(data) {
  var obj = new AccessPolicy();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = AccessPolicy;

