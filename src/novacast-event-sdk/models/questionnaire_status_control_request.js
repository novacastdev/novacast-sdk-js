'use strict';

var Serializer = require('../serializer');


var QuestionnaireStatusControlRequest = function() {
  
  this.question_set_uid = null;
  

  this._serialize = function() {
    return QuestionnaireStatusControlRequest._serialize(this);
  };
};

QuestionnaireStatusControlRequest._serialize = function(data) {
  var obj = {};

  
  obj['question_set_uid'] = Serializer.serialize(data['question_set_uid'], 'String');
  

  return obj;
};

QuestionnaireStatusControlRequest._deserialize = function(data) {
  var obj = new QuestionnaireStatusControlRequest();

  
  obj.question_set_uid = Serializer.deserialize(data['question_set_uid'], 'String');
  

  return obj;
};

module.exports = QuestionnaireStatusControlRequest;

