'use strict';

var Serializer = require('../serializer');


var UserFeedback = function() {
  
  this.account_uid = null;
  
  this.content = null;
  
  this.submitted_at = null;
  

  this._serialize = function() {
    return UserFeedback._serialize(this);
  };
};

UserFeedback._serialize = function(data) {
  var obj = {};

  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  
  obj['submitted_at'] = Serializer.serialize(data['submitted_at'], 'DateTime');
  

  return obj;
};

UserFeedback._deserialize = function(data) {
  var obj = new UserFeedback();

  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'String');
  
  obj.submitted_at = Serializer.deserialize(data['submitted_at'], 'DateTime');
  

  return obj;
};

module.exports = UserFeedback;

