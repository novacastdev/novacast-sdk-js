'use strict';

var Serializer = require('../serializer');


var QuestionContent = function() {
  
  this.uid = null;
  
  this.question = null;
  
  this.order = null;
  
  this.content = null;
  
  this.question_set_uid = null;
  
  this.question_content_type = null;
  

  this._serialize = function() {
    return QuestionContent._serialize(this);
  };
};

QuestionContent._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['question'] = Serializer.serialize(data['question'], 'String');
  
  obj['order'] = Serializer.serialize(data['order'], 'Integer');
  
  obj['content'] = Serializer.serialize(data['content'], 'Object');
  
  if (data['question_set_uid'] != undefined) obj['question_set_uid'] = Serializer.serialize(data['question_set_uid'], 'String');
  
  obj['question_content_type'] = Serializer.serialize(data['question_content_type'], 'QuestionContentType');
  

  return obj;
};

QuestionContent._deserialize = function(data) {
  var obj = new QuestionContent();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.question = Serializer.deserialize(data['question'], 'String');
  
  obj.order = Serializer.deserialize(data['order'], 'Integer');
  
  obj.content = Serializer.deserialize(data['content'], 'Object');
  
  obj.question_set_uid = Serializer.deserialize(data['question_set_uid'], 'String');
  
  obj.question_content_type = Serializer.deserialize(data['question_content_type'], 'QuestionContentType');
  

  return obj;
};

module.exports = QuestionContent;

