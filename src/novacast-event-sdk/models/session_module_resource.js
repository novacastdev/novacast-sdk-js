'use strict';

var Serializer = require('../serializer');


var SessionModuleResource = function() {
  
  this.usage = null;
  
  this.rn = null;
  

  this._serialize = function() {
    return SessionModuleResource._serialize(this);
  };
};

SessionModuleResource._serialize = function(data) {
  var obj = {};

  
  obj['usage'] = Serializer.serialize(data['usage'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  

  return obj;
};

SessionModuleResource._deserialize = function(data) {
  var obj = new SessionModuleResource();

  
  obj.usage = Serializer.deserialize(data['usage'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  

  return obj;
};

module.exports = SessionModuleResource;

