'use strict';

var Serializer = require('../serializer');


var EventSession = function() {
  
  this.uid = null;
  
  this.label = null;
  
  this.event_uid = null;
  
  this.event_name = null;
  
  this.is_default = null;
  

  this._serialize = function() {
    return EventSession._serialize(this);
  };
};

EventSession._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  
  obj['event_name'] = Serializer.serialize(data['event_name'], 'String');
  
  obj['is_default'] = Serializer.serialize(data['is_default'], 'BOOLEAN');
  

  return obj;
};

EventSession._deserialize = function(data) {
  var obj = new EventSession();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  
  obj.event_name = Serializer.deserialize(data['event_name'], 'String');
  
  obj.is_default = Serializer.deserialize(data['is_default'], 'BOOLEAN');
  

  return obj;
};

module.exports = EventSession;

