'use strict';

var Serializer = require('../serializer');


var GetAccountDisplayInfoResponse = function() {
  
  this.account_info = null;
  

  this._serialize = function() {
    return GetAccountDisplayInfoResponse._serialize(this);
  };
};

GetAccountDisplayInfoResponse._serialize = function(data) {
  var obj = {};

  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  

  return obj;
};

GetAccountDisplayInfoResponse._deserialize = function(data) {
  var obj = new GetAccountDisplayInfoResponse();

  
  obj.account_info = Serializer.deserialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  

  return obj;
};

module.exports = GetAccountDisplayInfoResponse;

