'use strict';

var Serializer = require('../serializer');


var EventUpdateRequest = function() {
  
  this.name = null;
  
  this.access_policy_uid = null;
  
  this.asset_bundle_uid = null;
  
  this.user_set_uid = null;
  
  this.preview_enabled = null;
  

  this._serialize = function() {
    return EventUpdateRequest._serialize(this);
  };
};

EventUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['name'] != undefined) obj['name'] = Serializer.serialize(data['name'], 'String');
  
  if (data['access_policy_uid'] != undefined) obj['access_policy_uid'] = Serializer.serialize(data['access_policy_uid'], 'String');
  
  if (data['asset_bundle_uid'] != undefined) obj['asset_bundle_uid'] = Serializer.serialize(data['asset_bundle_uid'], 'String');
  
  if (data['user_set_uid'] != undefined) obj['user_set_uid'] = Serializer.serialize(data['user_set_uid'], 'String');
  
  if (data['preview_enabled'] != undefined) obj['preview_enabled'] = Serializer.serialize(data['preview_enabled'], 'BOOLEAN');
  

  return obj;
};

EventUpdateRequest._deserialize = function(data) {
  var obj = new EventUpdateRequest();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.access_policy_uid = Serializer.deserialize(data['access_policy_uid'], 'String');
  
  obj.asset_bundle_uid = Serializer.deserialize(data['asset_bundle_uid'], 'String');
  
  obj.user_set_uid = Serializer.deserialize(data['user_set_uid'], 'String');
  
  obj.preview_enabled = Serializer.deserialize(data['preview_enabled'], 'BOOLEAN');
  

  return obj;
};

module.exports = EventUpdateRequest;

