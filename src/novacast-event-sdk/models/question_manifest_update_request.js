'use strict';

var Serializer = require('../serializer');


var QuestionManifestUpdateRequest = function() {
  
  this.label = null;
  
  this.manifest_type = null;
  
  this.question_set_uid = null;
  

  this._serialize = function() {
    return QuestionManifestUpdateRequest._serialize(this);
  };
};

QuestionManifestUpdateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['manifest_type'] = Serializer.serialize(data['manifest_type'], 'String');
  
  obj['question_set_uid'] = Serializer.serialize(data['question_set_uid'], 'String');
  

  return obj;
};

QuestionManifestUpdateRequest._deserialize = function(data) {
  var obj = new QuestionManifestUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.manifest_type = Serializer.deserialize(data['manifest_type'], 'String');
  
  obj.question_set_uid = Serializer.deserialize(data['question_set_uid'], 'String');
  

  return obj;
};

module.exports = QuestionManifestUpdateRequest;

