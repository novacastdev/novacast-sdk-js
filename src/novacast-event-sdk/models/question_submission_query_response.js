'use strict';

var Serializer = require('../serializer');


var QuestionSubmissionQueryResponse = function() {
  
  this.submissions_list = null;
  

  this._serialize = function() {
    return QuestionSubmissionQueryResponse._serialize(this);
  };
};

QuestionSubmissionQueryResponse._serialize = function(data) {
  var obj = {};

  
  obj['submissions_list'] = Serializer.serialize(data['submissions_list'], 'ManifestSubmissions');
  

  return obj;
};

QuestionSubmissionQueryResponse._deserialize = function(data) {
  var obj = new QuestionSubmissionQueryResponse();

  
  obj.submissions_list = Serializer.deserialize(data['submissions_list'], 'ManifestSubmissions');
  

  return obj;
};

module.exports = QuestionSubmissionQueryResponse;

