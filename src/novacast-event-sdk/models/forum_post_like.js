'use strict';

var Serializer = require('../serializer');


var ForumPostLike = function() {
  
  this.account_uid = null;
  
  this.account_display_name = null;
  
  this.forum_post_uid = null;
  
  this.liked = null;
  

  this._serialize = function() {
    return ForumPostLike._serialize(this);
  };
};

ForumPostLike._serialize = function(data) {
  var obj = {};

  
  if (data['account_uid'] != undefined) obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  if (data['account_display_name'] != undefined) obj['account_display_name'] = Serializer.serialize(data['account_display_name'], 'String');
  
  obj['forum_post_uid'] = Serializer.serialize(data['forum_post_uid'], 'String');
  
  obj['liked'] = Serializer.serialize(data['liked'], 'BOOLEAN');
  

  return obj;
};

ForumPostLike._deserialize = function(data) {
  var obj = new ForumPostLike();

  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.account_display_name = Serializer.deserialize(data['account_display_name'], 'String');
  
  obj.forum_post_uid = Serializer.deserialize(data['forum_post_uid'], 'String');
  
  obj.liked = Serializer.deserialize(data['liked'], 'BOOLEAN');
  

  return obj;
};

module.exports = ForumPostLike;

