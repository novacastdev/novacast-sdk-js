'use strict';

var Serializer = require('../serializer');


var PageRuntime = function() {
  
  this.session_uid = null;
  
  this.modules = null;
  
  this.info = null;
  

  this._serialize = function() {
    return PageRuntime._serialize(this);
  };
};

PageRuntime._serialize = function(data) {
  var obj = {};

  
  if (data['session_uid'] != undefined) obj['session_uid'] = Serializer.serialize(data['session_uid'], 'String');
  
  if (data['modules'] != undefined) obj['modules'] = Serializer.serialize(data['modules'], 'Array[ModuleRuntime]');
  
  if (data['info'] != undefined) obj['info'] = Serializer.serialize(data['info'], 'RuntimeInfo');
  

  return obj;
};

PageRuntime._deserialize = function(data) {
  var obj = new PageRuntime();

  
  obj.session_uid = Serializer.deserialize(data['session_uid'], 'String');
  
  obj.modules = Serializer.deserialize(data['modules'], 'Array[ModuleRuntime]');
  
  obj.info = Serializer.deserialize(data['info'], 'RuntimeInfo');
  

  return obj;
};

module.exports = PageRuntime;

