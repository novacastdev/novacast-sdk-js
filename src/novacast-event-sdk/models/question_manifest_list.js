'use strict';

var Serializer = require('../serializer');


var QuestionManifestList = function() {
  
  this.manifests = null;
  

  this._serialize = function() {
    return QuestionManifestList._serialize(this);
  };
};

QuestionManifestList._serialize = function(data) {
  var obj = {};

  
  obj['manifests'] = Serializer.serialize(data['manifests'], 'Array[QuestionManifest]');
  

  return obj;
};

QuestionManifestList._deserialize = function(data) {
  var obj = new QuestionManifestList();

  
  obj.manifests = Serializer.deserialize(data['manifests'], 'Array[QuestionManifest]');
  

  return obj;
};

module.exports = QuestionManifestList;

