'use strict';

var Serializer = require('../serializer');


var EnrollmentExtended = function() {
  
  this.account_type = null;
  
  this.account_uid = null;
  
  this.revoked = null;
  
  this.fields = null;
  
  this.enrolled_at = null;
  

  this._serialize = function() {
    return EnrollmentExtended._serialize(this);
  };
};

EnrollmentExtended._serialize = function(data) {
  var obj = {};

  
  obj['account_type'] = Serializer.serialize(data['account_type'], 'String');
  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  if (data['revoked'] != undefined) obj['revoked'] = Serializer.serialize(data['revoked'], 'BOOLEAN');
  
  obj['fields'] = Serializer.serialize(data['fields'], 'Hash[String, String]');
  
  obj['enrolled_at'] = Serializer.serialize(data['enrolled_at'], 'DateTime');
  

  return obj;
};

EnrollmentExtended._deserialize = function(data) {
  var obj = new EnrollmentExtended();

  
  obj.account_type = Serializer.deserialize(data['account_type'], 'String');
  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.revoked = Serializer.deserialize(data['revoked'], 'BOOLEAN');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Hash[String, String]');
  
  obj.enrolled_at = Serializer.deserialize(data['enrolled_at'], 'DateTime');
  

  return obj;
};

module.exports = EnrollmentExtended;

