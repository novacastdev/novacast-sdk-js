'use strict';

var Serializer = require('../serializer');


var AssetBundleCreateRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return AssetBundleCreateRequest._serialize(this);
  };
};

AssetBundleCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

AssetBundleCreateRequest._deserialize = function(data) {
  var obj = new AssetBundleCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = AssetBundleCreateRequest;

