'use strict';

var Serializer = require('../serializer');


var TranslationFilterOptionsResponse = function() {
  
  this.locales = null;
  
  this.categories = null;
  

  this._serialize = function() {
    return TranslationFilterOptionsResponse._serialize(this);
  };
};

TranslationFilterOptionsResponse._serialize = function(data) {
  var obj = {};

  
  if (data['locales'] != undefined) obj['locales'] = Serializer.serialize(data['locales'], 'Array[String]');
  
  if (data['categories'] != undefined) obj['categories'] = Serializer.serialize(data['categories'], 'Array[String]');
  

  return obj;
};

TranslationFilterOptionsResponse._deserialize = function(data) {
  var obj = new TranslationFilterOptionsResponse();

  
  obj.locales = Serializer.deserialize(data['locales'], 'Array[String]');
  
  obj.categories = Serializer.deserialize(data['categories'], 'Array[String]');
  

  return obj;
};

module.exports = TranslationFilterOptionsResponse;

