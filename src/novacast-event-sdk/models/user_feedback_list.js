'use strict';

var Serializer = require('../serializer');


var UserFeedbackList = function() {
  
  this.feedbacks = null;
  

  this._serialize = function() {
    return UserFeedbackList._serialize(this);
  };
};

UserFeedbackList._serialize = function(data) {
  var obj = {};

  
  obj['feedbacks'] = Serializer.serialize(data['feedbacks'], 'Array[UserFeedback]');
  

  return obj;
};

UserFeedbackList._deserialize = function(data) {
  var obj = new UserFeedbackList();

  
  obj.feedbacks = Serializer.deserialize(data['feedbacks'], 'Array[UserFeedback]');
  

  return obj;
};

module.exports = UserFeedbackList;

