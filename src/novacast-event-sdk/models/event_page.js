'use strict';

var Serializer = require('../serializer');


var EventPage = function() {
  
  this.uid = null;
  
  this.label = null;
  
  this.page_config = null;
  
  this.default_locale = null;
  

  this._serialize = function() {
    return EventPage._serialize(this);
  };
};

EventPage._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['page_config'] != undefined) obj['page_config'] = Serializer.serialize(data['page_config'], 'String');
  
  obj['default_locale'] = Serializer.serialize(data['default_locale'], 'String');
  

  return obj;
};

EventPage._deserialize = function(data) {
  var obj = new EventPage();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.page_config = Serializer.deserialize(data['page_config'], 'String');
  
  obj.default_locale = Serializer.deserialize(data['default_locale'], 'String');
  

  return obj;
};

module.exports = EventPage;

