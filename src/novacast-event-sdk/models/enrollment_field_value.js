'use strict';

var Serializer = require('../serializer');


var EnrollmentFieldValue = function() {
  
  this.field_name = null;
  
  this.value = null;
  

  this._serialize = function() {
    return EnrollmentFieldValue._serialize(this);
  };
};

EnrollmentFieldValue._serialize = function(data) {
  var obj = {};

  
  obj['field_name'] = Serializer.serialize(data['field_name'], 'String');
  
  obj['value'] = Serializer.serialize(data['value'], 'String');
  

  return obj;
};

EnrollmentFieldValue._deserialize = function(data) {
  var obj = new EnrollmentFieldValue();

  
  obj.field_name = Serializer.deserialize(data['field_name'], 'String');
  
  obj.value = Serializer.deserialize(data['value'], 'String');
  

  return obj;
};

module.exports = EnrollmentFieldValue;

