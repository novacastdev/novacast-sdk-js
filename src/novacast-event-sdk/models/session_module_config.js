'use strict';

var Serializer = require('../serializer');


var SessionModuleConfig = function() {
  
  this.config = null;
  
  this.resources = null;
  

  this._serialize = function() {
    return SessionModuleConfig._serialize(this);
  };
};

SessionModuleConfig._serialize = function(data) {
  var obj = {};

  
  obj['config'] = Serializer.serialize(data['config'], 'Object');
  
  if (data['resources'] != undefined) obj['resources'] = Serializer.serialize(data['resources'], 'Array[SessionModuleResource]');
  

  return obj;
};

SessionModuleConfig._deserialize = function(data) {
  var obj = new SessionModuleConfig();

  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  
  obj.resources = Serializer.deserialize(data['resources'], 'Array[SessionModuleResource]');
  

  return obj;
};

module.exports = SessionModuleConfig;

