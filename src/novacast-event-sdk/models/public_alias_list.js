'use strict';

var Serializer = require('../serializer');


var PublicAliasList = function() {
  
  this.aliases = null;
  

  this._serialize = function() {
    return PublicAliasList._serialize(this);
  };
};

PublicAliasList._serialize = function(data) {
  var obj = {};

  
  obj['aliases'] = Serializer.serialize(data['aliases'], 'Array[PublicAlias]');
  

  return obj;
};

PublicAliasList._deserialize = function(data) {
  var obj = new PublicAliasList();

  
  obj.aliases = Serializer.deserialize(data['aliases'], 'Array[PublicAlias]');
  

  return obj;
};

module.exports = PublicAliasList;

