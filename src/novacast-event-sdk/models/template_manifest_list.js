'use strict';

var Serializer = require('../serializer');


var TemplateManifestList = function() {
  
  this.template_manifests = null;
  

  this._serialize = function() {
    return TemplateManifestList._serialize(this);
  };
};

TemplateManifestList._serialize = function(data) {
  var obj = {};

  
  obj['template_manifests'] = Serializer.serialize(data['template_manifests'], 'Array[TemplateManifest]');
  

  return obj;
};

TemplateManifestList._deserialize = function(data) {
  var obj = new TemplateManifestList();

  
  obj.template_manifests = Serializer.deserialize(data['template_manifests'], 'Array[TemplateManifest]');
  

  return obj;
};

module.exports = TemplateManifestList;

