'use strict';

var Serializer = require('../serializer');


var AddquestionnaireStatRequest = function() {
  
  this.question_set_uid = null;
  
  this.answers = null;
  

  this._serialize = function() {
    return AddquestionnaireStatRequest._serialize(this);
  };
};

AddquestionnaireStatRequest._serialize = function(data) {
  var obj = {};

  
  obj['question_set_uid'] = Serializer.serialize(data['question_set_uid'], 'String');
  
  obj['answers'] = Serializer.serialize(data['answers'], 'Array[QuestionnaireAnswer]');
  

  return obj;
};

AddquestionnaireStatRequest._deserialize = function(data) {
  var obj = new AddquestionnaireStatRequest();

  
  obj.question_set_uid = Serializer.deserialize(data['question_set_uid'], 'String');
  
  obj.answers = Serializer.deserialize(data['answers'], 'Array[QuestionnaireAnswer]');
  

  return obj;
};

module.exports = AddquestionnaireStatRequest;

