'use strict';

var Serializer = require('../serializer');


var PollStatus = function() {
  
  this.start_time = null;
  
  this.end_time = null;
  
  this.status = null;
  
  this.statistics = null;
  

  this._serialize = function() {
    return PollStatus._serialize(this);
  };
};

PollStatus._serialize = function(data) {
  var obj = {};

  
  obj['start_time'] = Serializer.serialize(data['start_time'], 'Integer');
  
  obj['end_time'] = Serializer.serialize(data['end_time'], 'Integer');
  
  obj['status'] = Serializer.serialize(data['status'], 'String');
  
  obj['statistics'] = Serializer.serialize(data['statistics'], 'Object');
  

  return obj;
};

PollStatus._deserialize = function(data) {
  var obj = new PollStatus();

  
  obj.start_time = Serializer.deserialize(data['start_time'], 'Integer');
  
  obj.end_time = Serializer.deserialize(data['end_time'], 'Integer');
  
  obj.status = Serializer.deserialize(data['status'], 'String');
  
  obj.statistics = Serializer.deserialize(data['statistics'], 'Object');
  

  return obj;
};

module.exports = PollStatus;

