'use strict';

var Serializer = require('../serializer');


var SlideDeck = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.page_count = null;
  
  this.slides = null;
  

  this._serialize = function() {
    return SlideDeck._serialize(this);
  };
};

SlideDeck._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['page_count'] = Serializer.serialize(data['page_count'], 'Integer');
  
  obj['slides'] = Serializer.serialize(data['slides'], 'Array[Slide]');
  

  return obj;
};

SlideDeck._deserialize = function(data) {
  var obj = new SlideDeck();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.page_count = Serializer.deserialize(data['page_count'], 'Integer');
  
  obj.slides = Serializer.deserialize(data['slides'], 'Array[Slide]');
  

  return obj;
};

module.exports = SlideDeck;

