'use strict';

var Serializer = require('../serializer');


var EventSessionList = function() {
  
  this.sessions = null;
  

  this._serialize = function() {
    return EventSessionList._serialize(this);
  };
};

EventSessionList._serialize = function(data) {
  var obj = {};

  
  obj['sessions'] = Serializer.serialize(data['sessions'], 'Array[EventSession]');
  

  return obj;
};

EventSessionList._deserialize = function(data) {
  var obj = new EventSessionList();

  
  obj.sessions = Serializer.deserialize(data['sessions'], 'Array[EventSession]');
  

  return obj;
};

module.exports = EventSessionList;

