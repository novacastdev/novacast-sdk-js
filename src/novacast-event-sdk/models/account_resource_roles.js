'use strict';

var Serializer = require('../serializer');


var AccountResourceRoles = function() {
  
  this.resource = null;
  
  this.roles = null;
  

  this._serialize = function() {
    return AccountResourceRoles._serialize(this);
  };
};

AccountResourceRoles._serialize = function(data) {
  var obj = {};

  
  obj['resource'] = Serializer.serialize(data['resource'], 'String');
  
  obj['roles'] = Serializer.serialize(data['roles'], 'Array[String]');
  

  return obj;
};

AccountResourceRoles._deserialize = function(data) {
  var obj = new AccountResourceRoles();

  
  obj.resource = Serializer.deserialize(data['resource'], 'String');
  
  obj.roles = Serializer.deserialize(data['roles'], 'Array[String]');
  

  return obj;
};

module.exports = AccountResourceRoles;

