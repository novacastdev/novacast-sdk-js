'use strict';

var Serializer = require('../serializer');


var PublicAlias = function() {
  
  this.uid = null;
  
  this.path = null;
  
  this.channel_uid = null;
  
  this.event_uid = null;
  

  this._serialize = function() {
    return PublicAlias._serialize(this);
  };
};

PublicAlias._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['path'] = Serializer.serialize(data['path'], 'String');
  
  obj['channel_uid'] = Serializer.serialize(data['channel_uid'], 'String');
  
  if (data['event_uid'] != undefined) obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  

  return obj;
};

PublicAlias._deserialize = function(data) {
  var obj = new PublicAlias();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.path = Serializer.deserialize(data['path'], 'String');
  
  obj.channel_uid = Serializer.deserialize(data['channel_uid'], 'String');
  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  

  return obj;
};

module.exports = PublicAlias;

