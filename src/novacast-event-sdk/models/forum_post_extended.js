'use strict';

var Serializer = require('../serializer');


var ForumPostExtended = function() {
  
  this.uid = null;
  
  this.account_uid = null;
  
  this.content = null;
  
  this.likes = null;
  
  this.visible = null;
  
  this.submitted_at = null;
  
  this.account_info = null;
  
  this.approved = null;
  
  this.hidden = null;
  
  this.deleted = null;
  
  this.starred = null;
  

  this._serialize = function() {
    return ForumPostExtended._serialize(this);
  };
};

ForumPostExtended._serialize = function(data) {
  var obj = {};

  
  if (data['uid'] != undefined) obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  if (data['account_uid'] != undefined) obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  
  if (data['likes'] != undefined) obj['likes'] = Serializer.serialize(data['likes'], 'Integer');
  
  obj['visible'] = Serializer.serialize(data['visible'], 'BOOLEAN');
  
  obj['submitted_at'] = Serializer.serialize(data['submitted_at'], 'DateTime');
  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'AccountDisplayInfo');
  
  obj['approved'] = Serializer.serialize(data['approved'], 'BOOLEAN');
  
  obj['hidden'] = Serializer.serialize(data['hidden'], 'BOOLEAN');
  
  obj['deleted'] = Serializer.serialize(data['deleted'], 'BOOLEAN');
  
  obj['starred'] = Serializer.serialize(data['starred'], 'BOOLEAN');
  

  return obj;
};

ForumPostExtended._deserialize = function(data) {
  var obj = new ForumPostExtended();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'String');
  
  obj.likes = Serializer.deserialize(data['likes'], 'Integer');
  
  obj.visible = Serializer.deserialize(data['visible'], 'BOOLEAN');
  
  obj.submitted_at = Serializer.deserialize(data['submitted_at'], 'DateTime');
  
  obj.account_info = Serializer.deserialize(data['account_info'], 'AccountDisplayInfo');
  
  obj.approved = Serializer.deserialize(data['approved'], 'BOOLEAN');
  
  obj.hidden = Serializer.deserialize(data['hidden'], 'BOOLEAN');
  
  obj.deleted = Serializer.deserialize(data['deleted'], 'BOOLEAN');
  
  obj.starred = Serializer.deserialize(data['starred'], 'BOOLEAN');
  

  return obj;
};

module.exports = ForumPostExtended;

