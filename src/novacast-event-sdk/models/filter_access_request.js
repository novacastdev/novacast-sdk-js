'use strict';

var Serializer = require('../serializer');


var FilterAccessRequest = function() {
  
  this.client_request = null;
  

  this._serialize = function() {
    return FilterAccessRequest._serialize(this);
  };
};

FilterAccessRequest._serialize = function(data) {
  var obj = {};

  
  obj['client_request'] = Serializer.serialize(data['client_request'], 'ClientRequestInfo');
  

  return obj;
};

FilterAccessRequest._deserialize = function(data) {
  var obj = new FilterAccessRequest();

  
  obj.client_request = Serializer.deserialize(data['client_request'], 'ClientRequestInfo');
  

  return obj;
};

module.exports = FilterAccessRequest;

