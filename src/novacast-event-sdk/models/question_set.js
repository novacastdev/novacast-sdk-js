'use strict';

var Serializer = require('../serializer');


var QuestionSet = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.state = null;
  
  this.contents = null;
  

  this._serialize = function() {
    return QuestionSet._serialize(this);
  };
};

QuestionSet._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['state'] = Serializer.serialize(data['state'], 'String');
  
  if (data['contents'] != undefined) obj['contents'] = Serializer.serialize(data['contents'], 'Array[QuestionContent]');
  

  return obj;
};

QuestionSet._deserialize = function(data) {
  var obj = new QuestionSet();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.state = Serializer.deserialize(data['state'], 'String');
  
  obj.contents = Serializer.deserialize(data['contents'], 'Array[QuestionContent]');
  

  return obj;
};

module.exports = QuestionSet;

