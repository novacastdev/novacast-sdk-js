'use strict';

var Serializer = require('../serializer');


var EventPageUpdateRequest = function() {
  
  this.label = null;
  
  this.default_locale = null;
  

  this._serialize = function() {
    return EventPageUpdateRequest._serialize(this);
  };
};

EventPageUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['default_locale'] != undefined) obj['default_locale'] = Serializer.serialize(data['default_locale'], 'String');
  

  return obj;
};

EventPageUpdateRequest._deserialize = function(data) {
  var obj = new EventPageUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.default_locale = Serializer.deserialize(data['default_locale'], 'String');
  

  return obj;
};

module.exports = EventPageUpdateRequest;

