'use strict';

var Serializer = require('../serializer');


var Account = function() {
  
  this.uid = null;
  
  this.identifier = null;
  
  this.domain = null;
  
  this.provider = null;
  

  this._serialize = function() {
    return Account._serialize(this);
  };
};

Account._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['identifier'] = Serializer.serialize(data['identifier'], 'String');
  
  obj['domain'] = Serializer.serialize(data['domain'], 'String');
  
  obj['provider'] = Serializer.serialize(data['provider'], 'String');
  

  return obj;
};

Account._deserialize = function(data) {
  var obj = new Account();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.identifier = Serializer.deserialize(data['identifier'], 'String');
  
  obj.domain = Serializer.deserialize(data['domain'], 'String');
  
  obj.provider = Serializer.deserialize(data['provider'], 'String');
  

  return obj;
};

module.exports = Account;

