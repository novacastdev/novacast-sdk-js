'use strict';

var Serializer = require('../serializer');


var AccountResourceRolesList = function() {
  
  this.resource_roles = null;
  

  this._serialize = function() {
    return AccountResourceRolesList._serialize(this);
  };
};

AccountResourceRolesList._serialize = function(data) {
  var obj = {};

  
  obj['resource_roles'] = Serializer.serialize(data['resource_roles'], 'Array[AccountResourceRoles]');
  

  return obj;
};

AccountResourceRolesList._deserialize = function(data) {
  var obj = new AccountResourceRolesList();

  
  obj.resource_roles = Serializer.deserialize(data['resource_roles'], 'Array[AccountResourceRoles]');
  

  return obj;
};

module.exports = AccountResourceRolesList;

