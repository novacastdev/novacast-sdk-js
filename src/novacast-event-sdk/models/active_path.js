'use strict';

var Serializer = require('../serializer');


var ActivePath = function() {
  
  this.uid = null;
  
  this.path = null;
  
  this.path_mappings = null;
  

  this._serialize = function() {
    return ActivePath._serialize(this);
  };
};

ActivePath._serialize = function(data) {
  var obj = {};

  
  if (data['uid'] != undefined) obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['path'] = Serializer.serialize(data['path'], 'String');
  
  obj['path_mappings'] = Serializer.serialize(data['path_mappings'], 'Array[PathMapping]');
  

  return obj;
};

ActivePath._deserialize = function(data) {
  var obj = new ActivePath();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.path = Serializer.deserialize(data['path'], 'String');
  
  obj.path_mappings = Serializer.deserialize(data['path_mappings'], 'Array[PathMapping]');
  

  return obj;
};

module.exports = ActivePath;

