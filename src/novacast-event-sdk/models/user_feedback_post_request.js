'use strict';

var Serializer = require('../serializer');


var UserFeedbackPostRequest = function() {
  
  this.content = null;
  

  this._serialize = function() {
    return UserFeedbackPostRequest._serialize(this);
  };
};

UserFeedbackPostRequest._serialize = function(data) {
  var obj = {};

  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  

  return obj;
};

UserFeedbackPostRequest._deserialize = function(data) {
  var obj = new UserFeedbackPostRequest();

  
  obj.content = Serializer.deserialize(data['content'], 'String');
  

  return obj;
};

module.exports = UserFeedbackPostRequest;

