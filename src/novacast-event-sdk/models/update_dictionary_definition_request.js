'use strict';

var Serializer = require('../serializer');


var UpdateDictionaryDefinitionRequest = function() {
  
  this.operation = null;
  
  this.newEntry = null;
  

  this._serialize = function() {
    return UpdateDictionaryDefinitionRequest._serialize(this);
  };
};

UpdateDictionaryDefinitionRequest._serialize = function(data) {
  var obj = {};

  
  if (data['operation'] != undefined) obj['operation'] = Serializer.serialize(data['operation'], 'String');
  
  if (data['newEntry'] != undefined) obj['newEntry'] = Serializer.serialize(data['newEntry'], 'DictionaryEntry');
  

  return obj;
};

UpdateDictionaryDefinitionRequest._deserialize = function(data) {
  var obj = new UpdateDictionaryDefinitionRequest();

  
  obj.operation = Serializer.deserialize(data['operation'], 'String');
  
  obj.newEntry = Serializer.deserialize(data['newEntry'], 'DictionaryEntry');
  

  return obj;
};

module.exports = UpdateDictionaryDefinitionRequest;

