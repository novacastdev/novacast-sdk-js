'use strict';

var Serializer = require('../serializer');


var DataSetCreateRequest = function() {
  
  this.label = null;
  
  this.production = null;
  

  this._serialize = function() {
    return DataSetCreateRequest._serialize(this);
  };
};

DataSetCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['production'] != undefined) obj['production'] = Serializer.serialize(data['production'], 'BOOLEAN');
  

  return obj;
};

DataSetCreateRequest._deserialize = function(data) {
  var obj = new DataSetCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.production = Serializer.deserialize(data['production'], 'BOOLEAN');
  

  return obj;
};

module.exports = DataSetCreateRequest;

