'use strict';

var Serializer = require('../serializer');


var EnrollmentListResponse = function() {
  
  this.enrollments = null;
  
  this.account_info = null;
  
  this.fields = null;
  

  this._serialize = function() {
    return EnrollmentListResponse._serialize(this);
  };
};

EnrollmentListResponse._serialize = function(data) {
  var obj = {};

  
  obj['enrollments'] = Serializer.serialize(data['enrollments'], 'Array[EnrollmentExtended]');
  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  
  if (data['fields'] != undefined) obj['fields'] = Serializer.serialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

EnrollmentListResponse._deserialize = function(data) {
  var obj = new EnrollmentListResponse();

  
  obj.enrollments = Serializer.deserialize(data['enrollments'], 'Array[EnrollmentExtended]');
  
  obj.account_info = Serializer.deserialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

module.exports = EnrollmentListResponse;

