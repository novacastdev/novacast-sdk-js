'use strict';

var Serializer = require('../serializer');


var AssetBundleInfoList = function() {
  
  this.bundles = null;
  

  this._serialize = function() {
    return AssetBundleInfoList._serialize(this);
  };
};

AssetBundleInfoList._serialize = function(data) {
  var obj = {};

  
  obj['bundles'] = Serializer.serialize(data['bundles'], 'Array[AssetBundleInfo]');
  

  return obj;
};

AssetBundleInfoList._deserialize = function(data) {
  var obj = new AssetBundleInfoList();

  
  obj.bundles = Serializer.deserialize(data['bundles'], 'Array[AssetBundleInfo]');
  

  return obj;
};

module.exports = AssetBundleInfoList;

