'use strict';

var Serializer = require('../serializer');


var AssetBundleInfo = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  

  this._serialize = function() {
    return AssetBundleInfo._serialize(this);
  };
};

AssetBundleInfo._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

AssetBundleInfo._deserialize = function(data) {
  var obj = new AssetBundleInfo();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = AssetBundleInfo;

