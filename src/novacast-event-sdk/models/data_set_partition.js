'use strict';

var Serializer = require('../serializer');


var DataSetPartition = function() {
  
  this.uid = null;
  
  this.event_session_uid = null;
  

  this._serialize = function() {
    return DataSetPartition._serialize(this);
  };
};

DataSetPartition._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['event_session_uid'] = Serializer.serialize(data['event_session_uid'], 'String');
  

  return obj;
};

DataSetPartition._deserialize = function(data) {
  var obj = new DataSetPartition();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.event_session_uid = Serializer.deserialize(data['event_session_uid'], 'String');
  

  return obj;
};

module.exports = DataSetPartition;

