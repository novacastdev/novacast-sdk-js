'use strict';

var Serializer = require('../serializer');


var ForumPostLikeRequest = function() {
  
  this.like = null;
  

  this._serialize = function() {
    return ForumPostLikeRequest._serialize(this);
  };
};

ForumPostLikeRequest._serialize = function(data) {
  var obj = {};

  
  obj['like'] = Serializer.serialize(data['like'], 'BOOLEAN');
  

  return obj;
};

ForumPostLikeRequest._deserialize = function(data) {
  var obj = new ForumPostLikeRequest();

  
  obj.like = Serializer.deserialize(data['like'], 'BOOLEAN');
  

  return obj;
};

module.exports = ForumPostLikeRequest;

