'use strict';

var Serializer = require('../serializer');


var AccessPolicyCreateRequest = function() {
  
  this.label = null;
  
  this.filters = null;
  

  this._serialize = function() {
    return AccessPolicyCreateRequest._serialize(this);
  };
};

AccessPolicyCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['filters'] = Serializer.serialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

AccessPolicyCreateRequest._deserialize = function(data) {
  var obj = new AccessPolicyCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.filters = Serializer.deserialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

module.exports = AccessPolicyCreateRequest;

