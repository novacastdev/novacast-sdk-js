'use strict';

var Serializer = require('../serializer');


var ForumPostCreateRequest = function() {
  
  this.content = null;
  

  this._serialize = function() {
    return ForumPostCreateRequest._serialize(this);
  };
};

ForumPostCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  

  return obj;
};

ForumPostCreateRequest._deserialize = function(data) {
  var obj = new ForumPostCreateRequest();

  
  obj.content = Serializer.deserialize(data['content'], 'String');
  

  return obj;
};

module.exports = ForumPostCreateRequest;

