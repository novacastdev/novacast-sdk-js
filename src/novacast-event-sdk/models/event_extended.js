'use strict';

var Serializer = require('../serializer');


var EventExtended = function() {
  
  this.channel_uid = null;
  
  this.uid = null;
  
  this.name = null;
  
  this.stage = null;
  
  this.preview_enabled = null;
  
  this.default_session_uid = null;
  
  this.access_policy_uid = null;
  
  this.asset_bundle_uid = null;
  
  this.user_set_uid = null;
  
  this.public_aliases = null;
  
  this.event_pages = null;
  
  this.active_data_set_uid = null;
  

  this._serialize = function() {
    return EventExtended._serialize(this);
  };
};

EventExtended._serialize = function(data) {
  var obj = {};

  
  if (data['channel_uid'] != undefined) obj['channel_uid'] = Serializer.serialize(data['channel_uid'], 'String');
  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  obj['stage'] = Serializer.serialize(data['stage'], 'String');
  
  obj['preview_enabled'] = Serializer.serialize(data['preview_enabled'], 'BOOLEAN');
  
  obj['default_session_uid'] = Serializer.serialize(data['default_session_uid'], 'String');
  
  if (data['access_policy_uid'] != undefined) obj['access_policy_uid'] = Serializer.serialize(data['access_policy_uid'], 'String');
  
  if (data['asset_bundle_uid'] != undefined) obj['asset_bundle_uid'] = Serializer.serialize(data['asset_bundle_uid'], 'String');
  
  if (data['user_set_uid'] != undefined) obj['user_set_uid'] = Serializer.serialize(data['user_set_uid'], 'String');
  
  obj['public_aliases'] = Serializer.serialize(data['public_aliases'], 'Array[PublicAlias]');
  
  obj['event_pages'] = Serializer.serialize(data['event_pages'], 'Array[EventPage]');
  
  if (data['active_data_set_uid'] != undefined) obj['active_data_set_uid'] = Serializer.serialize(data['active_data_set_uid'], 'String');
  

  return obj;
};

EventExtended._deserialize = function(data) {
  var obj = new EventExtended();

  
  obj.channel_uid = Serializer.deserialize(data['channel_uid'], 'String');
  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.stage = Serializer.deserialize(data['stage'], 'String');
  
  obj.preview_enabled = Serializer.deserialize(data['preview_enabled'], 'BOOLEAN');
  
  obj.default_session_uid = Serializer.deserialize(data['default_session_uid'], 'String');
  
  obj.access_policy_uid = Serializer.deserialize(data['access_policy_uid'], 'String');
  
  obj.asset_bundle_uid = Serializer.deserialize(data['asset_bundle_uid'], 'String');
  
  obj.user_set_uid = Serializer.deserialize(data['user_set_uid'], 'String');
  
  obj.public_aliases = Serializer.deserialize(data['public_aliases'], 'Array[PublicAlias]');
  
  obj.event_pages = Serializer.deserialize(data['event_pages'], 'Array[EventPage]');
  
  obj.active_data_set_uid = Serializer.deserialize(data['active_data_set_uid'], 'String');
  

  return obj;
};

module.exports = EventExtended;

