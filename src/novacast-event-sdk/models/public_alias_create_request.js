'use strict';

var Serializer = require('../serializer');


var PublicAliasCreateRequest = function() {
  
  this.path = null;
  
  this.event_uid = null;
  

  this._serialize = function() {
    return PublicAliasCreateRequest._serialize(this);
  };
};

PublicAliasCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['path'] = Serializer.serialize(data['path'], 'String');
  
  if (data['event_uid'] != undefined) obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  

  return obj;
};

PublicAliasCreateRequest._deserialize = function(data) {
  var obj = new PublicAliasCreateRequest();

  
  obj.path = Serializer.deserialize(data['path'], 'String');
  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  

  return obj;
};

module.exports = PublicAliasCreateRequest;

