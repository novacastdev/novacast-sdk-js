'use strict';

var Serializer = require('../serializer');


var QuestionSubmissionCreateRequest = function() {
  
  this.question_manifest_uid = null;
  
  this.submissions = null;
  

  this._serialize = function() {
    return QuestionSubmissionCreateRequest._serialize(this);
  };
};

QuestionSubmissionCreateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['question_manifest_uid'] != undefined) obj['question_manifest_uid'] = Serializer.serialize(data['question_manifest_uid'], 'String');
  
  obj['submissions'] = Serializer.serialize(data['submissions'], 'Array[QuestionSubmissionCreate]');
  

  return obj;
};

QuestionSubmissionCreateRequest._deserialize = function(data) {
  var obj = new QuestionSubmissionCreateRequest();

  
  obj.question_manifest_uid = Serializer.deserialize(data['question_manifest_uid'], 'String');
  
  obj.submissions = Serializer.deserialize(data['submissions'], 'Array[QuestionSubmissionCreate]');
  

  return obj;
};

module.exports = QuestionSubmissionCreateRequest;

