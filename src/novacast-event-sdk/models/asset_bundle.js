'use strict';

var Serializer = require('../serializer');


var AssetBundle = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.contents = null;
  

  this._serialize = function() {
    return AssetBundle._serialize(this);
  };
};

AssetBundle._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['contents'] = Serializer.serialize(data['contents'], 'Array[BundleContent]');
  

  return obj;
};

AssetBundle._deserialize = function(data) {
  var obj = new AssetBundle();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.contents = Serializer.deserialize(data['contents'], 'Array[BundleContent]');
  

  return obj;
};

module.exports = AssetBundle;

