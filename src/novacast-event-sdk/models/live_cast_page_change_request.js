'use strict';

var Serializer = require('../serializer');


var LiveCastPageChangeRequest = function() {
  
  this.page = null;
  

  this._serialize = function() {
    return LiveCastPageChangeRequest._serialize(this);
  };
};

LiveCastPageChangeRequest._serialize = function(data) {
  var obj = {};

  
  obj['page'] = Serializer.serialize(data['page'], 'Integer');
  

  return obj;
};

LiveCastPageChangeRequest._deserialize = function(data) {
  var obj = new LiveCastPageChangeRequest();

  
  obj.page = Serializer.deserialize(data['page'], 'Integer');
  

  return obj;
};

module.exports = LiveCastPageChangeRequest;

