'use strict';

var Serializer = require('../serializer');


var AddPollStatRequest = function() {
  
  this.question_content_uid = null;
  
  this.choice = null;
  

  this._serialize = function() {
    return AddPollStatRequest._serialize(this);
  };
};

AddPollStatRequest._serialize = function(data) {
  var obj = {};

  
  obj['question_content_uid'] = Serializer.serialize(data['question_content_uid'], 'String');
  
  obj['choice'] = Serializer.serialize(data['choice'], 'Object');
  

  return obj;
};

AddPollStatRequest._deserialize = function(data) {
  var obj = new AddPollStatRequest();

  
  obj.question_content_uid = Serializer.deserialize(data['question_content_uid'], 'String');
  
  obj.choice = Serializer.deserialize(data['choice'], 'Object');
  

  return obj;
};

module.exports = AddPollStatRequest;

