'use strict';

var Serializer = require('../serializer');


var QuestionSubmissionsList = function() {
  
  this.submissions = null;
  

  this._serialize = function() {
    return QuestionSubmissionsList._serialize(this);
  };
};

QuestionSubmissionsList._serialize = function(data) {
  var obj = {};

  
  obj['submissions'] = Serializer.serialize(data['submissions'], 'Array[QuestionSubmission]');
  

  return obj;
};

QuestionSubmissionsList._deserialize = function(data) {
  var obj = new QuestionSubmissionsList();

  
  obj.submissions = Serializer.deserialize(data['submissions'], 'Array[QuestionSubmission]');
  

  return obj;
};

module.exports = QuestionSubmissionsList;

