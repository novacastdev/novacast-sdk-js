'use strict';

var Serializer = require('../serializer');


var EventList = function() {
  
  this.events = null;
  

  this._serialize = function() {
    return EventList._serialize(this);
  };
};

EventList._serialize = function(data) {
  var obj = {};

  
  obj['events'] = Serializer.serialize(data['events'], 'Array[Event]');
  

  return obj;
};

EventList._deserialize = function(data) {
  var obj = new EventList();

  
  obj.events = Serializer.deserialize(data['events'], 'Array[Event]');
  

  return obj;
};

module.exports = EventList;

