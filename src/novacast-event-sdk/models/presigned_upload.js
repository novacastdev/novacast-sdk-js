'use strict';

var Serializer = require('../serializer');


var PresignedUpload = function() {
  
  this.url = null;
  
  this.fields = null;
  
  this.callback_data = null;
  

  this._serialize = function() {
    return PresignedUpload._serialize(this);
  };
};

PresignedUpload._serialize = function(data) {
  var obj = {};

  
  obj['url'] = Serializer.serialize(data['url'], 'String');
  
  obj['fields'] = Serializer.serialize(data['fields'], 'Object');
  
  obj['callback_data'] = Serializer.serialize(data['callback_data'], 'Object');
  

  return obj;
};

PresignedUpload._deserialize = function(data) {
  var obj = new PresignedUpload();

  
  obj.url = Serializer.deserialize(data['url'], 'String');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Object');
  
  obj.callback_data = Serializer.deserialize(data['callback_data'], 'Object');
  

  return obj;
};

module.exports = PresignedUpload;

