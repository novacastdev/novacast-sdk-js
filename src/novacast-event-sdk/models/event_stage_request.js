'use strict';

var Serializer = require('../serializer');


var EventStageRequest = function() {
  
  this.stage = null;
  

  this._serialize = function() {
    return EventStageRequest._serialize(this);
  };
};

EventStageRequest._serialize = function(data) {
  var obj = {};

  
  obj['stage'] = Serializer.serialize(data['stage'], 'String');
  

  return obj;
};

EventStageRequest._deserialize = function(data) {
  var obj = new EventStageRequest();

  
  obj.stage = Serializer.deserialize(data['stage'], 'String');
  

  return obj;
};

module.exports = EventStageRequest;

