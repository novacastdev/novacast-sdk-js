'use strict';

var Serializer = require('../serializer');


var EventPageList = function() {
  
  this.pages = null;
  

  this._serialize = function() {
    return EventPageList._serialize(this);
  };
};

EventPageList._serialize = function(data) {
  var obj = {};

  
  obj['pages'] = Serializer.serialize(data['pages'], 'Array[EventPage]');
  

  return obj;
};

EventPageList._deserialize = function(data) {
  var obj = new EventPageList();

  
  obj.pages = Serializer.deserialize(data['pages'], 'Array[EventPage]');
  

  return obj;
};

module.exports = EventPageList;

