'use strict';

var Serializer = require('../serializer');


var ActivePoll = function() {
  
  this.question_manifest_uid = null;
  
  this.question_content = null;
  
  this.status = null;
  

  this._serialize = function() {
    return ActivePoll._serialize(this);
  };
};

ActivePoll._serialize = function(data) {
  var obj = {};

  
  obj['question_manifest_uid'] = Serializer.serialize(data['question_manifest_uid'], 'String');
  
  obj['question_content'] = Serializer.serialize(data['question_content'], 'QuestionContent');
  
  obj['status'] = Serializer.serialize(data['status'], 'String');
  

  return obj;
};

ActivePoll._deserialize = function(data) {
  var obj = new ActivePoll();

  
  obj.question_manifest_uid = Serializer.deserialize(data['question_manifest_uid'], 'String');
  
  obj.question_content = Serializer.deserialize(data['question_content'], 'QuestionContent');
  
  obj.status = Serializer.deserialize(data['status'], 'String');
  

  return obj;
};

module.exports = ActivePoll;

