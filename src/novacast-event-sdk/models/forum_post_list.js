'use strict';

var Serializer = require('../serializer');


var ForumPostList = function() {
  
  this.posts = null;
  

  this._serialize = function() {
    return ForumPostList._serialize(this);
  };
};

ForumPostList._serialize = function(data) {
  var obj = {};

  
  obj['posts'] = Serializer.serialize(data['posts'], 'Array[ForumPost]');
  

  return obj;
};

ForumPostList._deserialize = function(data) {
  var obj = new ForumPostList();

  
  obj.posts = Serializer.deserialize(data['posts'], 'Array[ForumPost]');
  

  return obj;
};

module.exports = ForumPostList;

