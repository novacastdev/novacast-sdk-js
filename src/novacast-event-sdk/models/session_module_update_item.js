'use strict';

var Serializer = require('../serializer');


var SessionModuleUpdateItem = function() {
  
  this.usage = null;
  
  this.detail = null;
  

  this._serialize = function() {
    return SessionModuleUpdateItem._serialize(this);
  };
};

SessionModuleUpdateItem._serialize = function(data) {
  var obj = {};

  
  obj['usage'] = Serializer.serialize(data['usage'], 'String');
  
  obj['detail'] = Serializer.serialize(data['detail'], 'Object');
  

  return obj;
};

SessionModuleUpdateItem._deserialize = function(data) {
  var obj = new SessionModuleUpdateItem();

  
  obj.usage = Serializer.deserialize(data['usage'], 'String');
  
  obj.detail = Serializer.deserialize(data['detail'], 'Object');
  

  return obj;
};

module.exports = SessionModuleUpdateItem;

