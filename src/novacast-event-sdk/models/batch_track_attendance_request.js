'use strict';

var Serializer = require('../serializer');


var BatchTrackAttendanceRequest = function() {
  
  this.requests = null;
  

  this._serialize = function() {
    return BatchTrackAttendanceRequest._serialize(this);
  };
};

BatchTrackAttendanceRequest._serialize = function(data) {
  var obj = {};

  
  if (data['requests'] != undefined) obj['requests'] = Serializer.serialize(data['requests'], 'Array[TrackAttendanceRequest]');
  

  return obj;
};

BatchTrackAttendanceRequest._deserialize = function(data) {
  var obj = new BatchTrackAttendanceRequest();

  
  obj.requests = Serializer.deserialize(data['requests'], 'Array[TrackAttendanceRequest]');
  

  return obj;
};

module.exports = BatchTrackAttendanceRequest;

