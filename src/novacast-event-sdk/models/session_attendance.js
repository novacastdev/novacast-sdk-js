'use strict';

var Serializer = require('../serializer');


var SessionAttendance = function() {
  
  this.account_uid = null;
  
  this.session_uid = null;
  
  this.first_request_at = null;
  
  this.last_request_at = null;
  

  this._serialize = function() {
    return SessionAttendance._serialize(this);
  };
};

SessionAttendance._serialize = function(data) {
  var obj = {};

  
  if (data['account_uid'] != undefined) obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  obj['session_uid'] = Serializer.serialize(data['session_uid'], 'String');
  
  obj['first_request_at'] = Serializer.serialize(data['first_request_at'], 'DateTime');
  
  obj['last_request_at'] = Serializer.serialize(data['last_request_at'], 'DateTime');
  

  return obj;
};

SessionAttendance._deserialize = function(data) {
  var obj = new SessionAttendance();

  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.session_uid = Serializer.deserialize(data['session_uid'], 'String');
  
  obj.first_request_at = Serializer.deserialize(data['first_request_at'], 'DateTime');
  
  obj.last_request_at = Serializer.deserialize(data['last_request_at'], 'DateTime');
  

  return obj;
};

module.exports = SessionAttendance;

