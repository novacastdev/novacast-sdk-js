'use strict';

var Serializer = require('../serializer');


var TemplateManifest = function() {
  
  this.template_name = null;
  
  this.label = null;
  
  this.preview_thumbnail = null;
  
  this.config = null;
  

  this._serialize = function() {
    return TemplateManifest._serialize(this);
  };
};

TemplateManifest._serialize = function(data) {
  var obj = {};

  
  if (data['template_name'] != undefined) obj['template_name'] = Serializer.serialize(data['template_name'], 'String');
  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['preview_thumbnail'] != undefined) obj['preview_thumbnail'] = Serializer.serialize(data['preview_thumbnail'], 'String');
  
  if (data['config'] != undefined) obj['config'] = Serializer.serialize(data['config'], 'Object');
  

  return obj;
};

TemplateManifest._deserialize = function(data) {
  var obj = new TemplateManifest();

  
  obj.template_name = Serializer.deserialize(data['template_name'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.preview_thumbnail = Serializer.deserialize(data['preview_thumbnail'], 'String');
  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  

  return obj;
};

module.exports = TemplateManifest;

