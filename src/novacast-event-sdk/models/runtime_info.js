'use strict';

var Serializer = require('../serializer');


var RuntimeInfo = function() {
  
  this.session_label = null;
  
  this.event_name = null;
  

  this._serialize = function() {
    return RuntimeInfo._serialize(this);
  };
};

RuntimeInfo._serialize = function(data) {
  var obj = {};

  
  obj['session_label'] = Serializer.serialize(data['session_label'], 'String');
  
  obj['event_name'] = Serializer.serialize(data['event_name'], 'String');
  

  return obj;
};

RuntimeInfo._deserialize = function(data) {
  var obj = new RuntimeInfo();

  
  obj.session_label = Serializer.deserialize(data['session_label'], 'String');
  
  obj.event_name = Serializer.deserialize(data['event_name'], 'String');
  

  return obj;
};

module.exports = RuntimeInfo;

