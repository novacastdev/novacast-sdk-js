'use strict';

var Serializer = require('../serializer');


var AccountList = function() {
  
  this.accounts = null;
  

  this._serialize = function() {
    return AccountList._serialize(this);
  };
};

AccountList._serialize = function(data) {
  var obj = {};

  
  obj['accounts'] = Serializer.serialize(data['accounts'], 'Array[Account]');
  

  return obj;
};

AccountList._deserialize = function(data) {
  var obj = new AccountList();

  
  obj.accounts = Serializer.deserialize(data['accounts'], 'Array[Account]');
  

  return obj;
};

module.exports = AccountList;

