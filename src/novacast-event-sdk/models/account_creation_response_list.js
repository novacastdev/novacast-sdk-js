'use strict';

var Serializer = require('../serializer');


var AccountCreationResponseList = function() {
  
  this.responses = null;
  

  this._serialize = function() {
    return AccountCreationResponseList._serialize(this);
  };
};

AccountCreationResponseList._serialize = function(data) {
  var obj = {};

  
  obj['responses'] = Serializer.serialize(data['responses'], 'Array[AccountCreationResponse]');
  

  return obj;
};

AccountCreationResponseList._deserialize = function(data) {
  var obj = new AccountCreationResponseList();

  
  obj.responses = Serializer.deserialize(data['responses'], 'Array[AccountCreationResponse]');
  

  return obj;
};

module.exports = AccountCreationResponseList;

