'use strict';

var Serializer = require('../serializer');


var QuestionSubmissionCreate = function() {
  
  this.answer = null;
  
  this.question_content_uid = null;
  

  this._serialize = function() {
    return QuestionSubmissionCreate._serialize(this);
  };
};

QuestionSubmissionCreate._serialize = function(data) {
  var obj = {};

  
  obj['answer'] = Serializer.serialize(data['answer'], 'Object');
  
  obj['question_content_uid'] = Serializer.serialize(data['question_content_uid'], 'String');
  

  return obj;
};

QuestionSubmissionCreate._deserialize = function(data) {
  var obj = new QuestionSubmissionCreate();

  
  obj.answer = Serializer.deserialize(data['answer'], 'Object');
  
  obj.question_content_uid = Serializer.deserialize(data['question_content_uid'], 'String');
  

  return obj;
};

module.exports = QuestionSubmissionCreate;

