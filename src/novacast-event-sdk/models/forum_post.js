'use strict';

var Serializer = require('../serializer');


var ForumPost = function() {
  
  this.uid = null;
  
  this.account_uid = null;
  
  this.content = null;
  
  this.likes = null;
  
  this.visible = null;
  
  this.submitted_at = null;
  

  this._serialize = function() {
    return ForumPost._serialize(this);
  };
};

ForumPost._serialize = function(data) {
  var obj = {};

  
  if (data['uid'] != undefined) obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  if (data['account_uid'] != undefined) obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  
  if (data['likes'] != undefined) obj['likes'] = Serializer.serialize(data['likes'], 'Integer');
  
  obj['visible'] = Serializer.serialize(data['visible'], 'BOOLEAN');
  
  obj['submitted_at'] = Serializer.serialize(data['submitted_at'], 'DateTime');
  

  return obj;
};

ForumPost._deserialize = function(data) {
  var obj = new ForumPost();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'String');
  
  obj.likes = Serializer.deserialize(data['likes'], 'Integer');
  
  obj.visible = Serializer.deserialize(data['visible'], 'BOOLEAN');
  
  obj.submitted_at = Serializer.deserialize(data['submitted_at'], 'DateTime');
  

  return obj;
};

module.exports = ForumPost;

