'use strict';

var Serializer = require('../serializer');


var QuestionContentTypeList = function() {
  
  this.types = null;
  

  this._serialize = function() {
    return QuestionContentTypeList._serialize(this);
  };
};

QuestionContentTypeList._serialize = function(data) {
  var obj = {};

  
  obj['types'] = Serializer.serialize(data['types'], 'Array[QuestionContentType]');
  

  return obj;
};

QuestionContentTypeList._deserialize = function(data) {
  var obj = new QuestionContentTypeList();

  
  obj.types = Serializer.deserialize(data['types'], 'Array[QuestionContentType]');
  

  return obj;
};

module.exports = QuestionContentTypeList;

