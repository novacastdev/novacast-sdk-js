'use strict';

var Serializer = require('../serializer');


var StreamMediumCreateRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return StreamMediumCreateRequest._serialize(this);
  };
};

StreamMediumCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

StreamMediumCreateRequest._deserialize = function(data) {
  var obj = new StreamMediumCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = StreamMediumCreateRequest;

