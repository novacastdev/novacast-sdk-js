'use strict';

var Serializer = require('../serializer');


var BundleContentPreUploadRequest = function() {
  
  this.file_path = null;
  

  this._serialize = function() {
    return BundleContentPreUploadRequest._serialize(this);
  };
};

BundleContentPreUploadRequest._serialize = function(data) {
  var obj = {};

  
  obj['file_path'] = Serializer.serialize(data['file_path'], 'String');
  

  return obj;
};

BundleContentPreUploadRequest._deserialize = function(data) {
  var obj = new BundleContentPreUploadRequest();

  
  obj.file_path = Serializer.deserialize(data['file_path'], 'String');
  

  return obj;
};

module.exports = BundleContentPreUploadRequest;

