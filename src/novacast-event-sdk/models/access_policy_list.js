'use strict';

var Serializer = require('../serializer');


var AccessPolicyList = function() {
  
  this.policies = null;
  

  this._serialize = function() {
    return AccessPolicyList._serialize(this);
  };
};

AccessPolicyList._serialize = function(data) {
  var obj = {};

  
  obj['policies'] = Serializer.serialize(data['policies'], 'Array[AccessPolicy]');
  

  return obj;
};

AccessPolicyList._deserialize = function(data) {
  var obj = new AccessPolicyList();

  
  obj.policies = Serializer.deserialize(data['policies'], 'Array[AccessPolicy]');
  

  return obj;
};

module.exports = AccessPolicyList;

