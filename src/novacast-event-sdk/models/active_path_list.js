'use strict';

var Serializer = require('../serializer');


var ActivePathList = function() {
  
  this.active_paths = null;
  

  this._serialize = function() {
    return ActivePathList._serialize(this);
  };
};

ActivePathList._serialize = function(data) {
  var obj = {};

  
  obj['active_paths'] = Serializer.serialize(data['active_paths'], 'Array[ActivePath]');
  

  return obj;
};

ActivePathList._deserialize = function(data) {
  var obj = new ActivePathList();

  
  obj.active_paths = Serializer.deserialize(data['active_paths'], 'Array[ActivePath]');
  

  return obj;
};

module.exports = ActivePathList;

