'use strict';

var Serializer = require('../serializer');


var PageContentList = function() {
  
  this.contents = null;
  

  this._serialize = function() {
    return PageContentList._serialize(this);
  };
};

PageContentList._serialize = function(data) {
  var obj = {};

  
  obj['contents'] = Serializer.serialize(data['contents'], 'Array[PageContent]');
  

  return obj;
};

PageContentList._deserialize = function(data) {
  var obj = new PageContentList();

  
  obj.contents = Serializer.deserialize(data['contents'], 'Array[PageContent]');
  

  return obj;
};

module.exports = PageContentList;

