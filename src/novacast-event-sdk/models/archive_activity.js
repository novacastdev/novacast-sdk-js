'use strict';

var Serializer = require('../serializer');


var ArchiveActivity = function() {
  
  this.uid = null;
  
  this.time_code = null;
  
  this.activity_type = null;
  
  this.config = null;
  

  this._serialize = function() {
    return ArchiveActivity._serialize(this);
  };
};

ArchiveActivity._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['time_code'] = Serializer.serialize(data['time_code'], 'Integer');
  
  obj['activity_type'] = Serializer.serialize(data['activity_type'], 'String');
  
  obj['config'] = Serializer.serialize(data['config'], 'Object');
  

  return obj;
};

ArchiveActivity._deserialize = function(data) {
  var obj = new ArchiveActivity();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.time_code = Serializer.deserialize(data['time_code'], 'Integer');
  
  obj.activity_type = Serializer.deserialize(data['activity_type'], 'String');
  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  

  return obj;
};

module.exports = ArchiveActivity;

