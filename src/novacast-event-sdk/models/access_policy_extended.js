'use strict';

var Serializer = require('../serializer');


var AccessPolicyExtended = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.filters = null;
  

  this._serialize = function() {
    return AccessPolicyExtended._serialize(this);
  };
};

AccessPolicyExtended._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['filters'] != undefined) obj['filters'] = Serializer.serialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

AccessPolicyExtended._deserialize = function(data) {
  var obj = new AccessPolicyExtended();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.filters = Serializer.deserialize(data['filters'], 'Array[AccessFilter]');
  

  return obj;
};

module.exports = AccessPolicyExtended;

