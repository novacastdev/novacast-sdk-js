'use strict';

var Serializer = require('../serializer');


var ErrorField = function() {
  
  this.field = null;
  
  this.message = null;
  

  this._serialize = function() {
    return ErrorField._serialize(this);
  };
};

ErrorField._serialize = function(data) {
  var obj = {};

  
  obj['field'] = Serializer.serialize(data['field'], 'String');
  
  obj['message'] = Serializer.serialize(data['message'], 'String');
  

  return obj;
};

ErrorField._deserialize = function(data) {
  var obj = new ErrorField();

  
  obj.field = Serializer.deserialize(data['field'], 'String');
  
  obj.message = Serializer.deserialize(data['message'], 'String');
  

  return obj;
};

module.exports = ErrorField;

