'use strict';

var Serializer = require('../serializer');


var UserSetEnrollRequest = function() {
  
  this.account_uid = null;
  
  this.fields = null;
  

  this._serialize = function() {
    return UserSetEnrollRequest._serialize(this);
  };
};

UserSetEnrollRequest._serialize = function(data) {
  var obj = {};

  
  obj['account_uid'] = Serializer.serialize(data['account_uid'], 'String');
  
  if (data['fields'] != undefined) obj['fields'] = Serializer.serialize(data['fields'], 'Array[EnrollmentFieldValue]');
  

  return obj;
};

UserSetEnrollRequest._deserialize = function(data) {
  var obj = new UserSetEnrollRequest();

  
  obj.account_uid = Serializer.deserialize(data['account_uid'], 'String');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Array[EnrollmentFieldValue]');
  

  return obj;
};

module.exports = UserSetEnrollRequest;

