'use strict';

var Serializer = require('../serializer');


var ChannelBatchRequest = function() {
  
  this.channel_uids = null;
  

  this._serialize = function() {
    return ChannelBatchRequest._serialize(this);
  };
};

ChannelBatchRequest._serialize = function(data) {
  var obj = {};

  
  obj['channel_uids'] = Serializer.serialize(data['channel_uids'], 'Array[String]');
  

  return obj;
};

ChannelBatchRequest._deserialize = function(data) {
  var obj = new ChannelBatchRequest();

  
  obj.channel_uids = Serializer.deserialize(data['channel_uids'], 'Array[String]');
  

  return obj;
};

module.exports = ChannelBatchRequest;

