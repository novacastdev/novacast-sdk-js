'use strict';

var Serializer = require('../serializer');


var WhitelistPattern = function() {
  
  this.uid = null;
  
  this.enrollment_field = null;
  
  this.pattern_type = null;
  
  this.pattern = null;
  

  this._serialize = function() {
    return WhitelistPattern._serialize(this);
  };
};

WhitelistPattern._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['enrollment_field'] = Serializer.serialize(data['enrollment_field'], 'String');
  
  obj['pattern_type'] = Serializer.serialize(data['pattern_type'], 'String');
  
  obj['pattern'] = Serializer.serialize(data['pattern'], 'String');
  

  return obj;
};

WhitelistPattern._deserialize = function(data) {
  var obj = new WhitelistPattern();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.enrollment_field = Serializer.deserialize(data['enrollment_field'], 'String');
  
  obj.pattern_type = Serializer.deserialize(data['pattern_type'], 'String');
  
  obj.pattern = Serializer.deserialize(data['pattern'], 'String');
  

  return obj;
};

module.exports = WhitelistPattern;

