'use strict';

var Serializer = require('../serializer');


var LiveCastSlidePage = function() {
  
  this.active = null;
  
  this.pre_loads = null;
  

  this._serialize = function() {
    return LiveCastSlidePage._serialize(this);
  };
};

LiveCastSlidePage._serialize = function(data) {
  var obj = {};

  
  obj['active'] = Serializer.serialize(data['active'], 'Slide');
  
  if (data['pre_loads'] != undefined) obj['pre_loads'] = Serializer.serialize(data['pre_loads'], 'Array[Slide]');
  

  return obj;
};

LiveCastSlidePage._deserialize = function(data) {
  var obj = new LiveCastSlidePage();

  
  obj.active = Serializer.deserialize(data['active'], 'Slide');
  
  obj.pre_loads = Serializer.deserialize(data['pre_loads'], 'Array[Slide]');
  

  return obj;
};

module.exports = LiveCastSlidePage;

