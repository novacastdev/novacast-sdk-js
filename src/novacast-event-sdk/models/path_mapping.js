'use strict';

var Serializer = require('../serializer');


var PathMapping = function() {
  
  this.uid = null;
  
  this.label = null;
  
  this.is_active = null;
  
  this.public = null;
  
  this.event_session_uid = null;
  
  this.event_page_uid = null;
  
  this.event_session_label = null;
  
  this.event_page_label = null;
  

  this._serialize = function() {
    return PathMapping._serialize(this);
  };
};

PathMapping._serialize = function(data) {
  var obj = {};

  
  if (data['uid'] != undefined) obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['is_active'] = Serializer.serialize(data['is_active'], 'BOOLEAN');
  
  obj['public'] = Serializer.serialize(data['public'], 'BOOLEAN');
  
  obj['event_session_uid'] = Serializer.serialize(data['event_session_uid'], 'String');
  
  obj['event_page_uid'] = Serializer.serialize(data['event_page_uid'], 'String');
  
  if (data['event_session_label'] != undefined) obj['event_session_label'] = Serializer.serialize(data['event_session_label'], 'String');
  
  if (data['event_page_label'] != undefined) obj['event_page_label'] = Serializer.serialize(data['event_page_label'], 'String');
  

  return obj;
};

PathMapping._deserialize = function(data) {
  var obj = new PathMapping();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.is_active = Serializer.deserialize(data['is_active'], 'BOOLEAN');
  
  obj.public = Serializer.deserialize(data['public'], 'BOOLEAN');
  
  obj.event_session_uid = Serializer.deserialize(data['event_session_uid'], 'String');
  
  obj.event_page_uid = Serializer.deserialize(data['event_page_uid'], 'String');
  
  obj.event_session_label = Serializer.deserialize(data['event_session_label'], 'String');
  
  obj.event_page_label = Serializer.deserialize(data['event_page_label'], 'String');
  

  return obj;
};

module.exports = PathMapping;

