'use strict';

var Serializer = require('../serializer');


var DataSetPartitionList = function() {
  
  this.partitions = null;
  

  this._serialize = function() {
    return DataSetPartitionList._serialize(this);
  };
};

DataSetPartitionList._serialize = function(data) {
  var obj = {};

  
  obj['partitions'] = Serializer.serialize(data['partitions'], 'Array[DataSetPartition]');
  

  return obj;
};

DataSetPartitionList._deserialize = function(data) {
  var obj = new DataSetPartitionList();

  
  obj.partitions = Serializer.deserialize(data['partitions'], 'Array[DataSetPartition]');
  

  return obj;
};

module.exports = DataSetPartitionList;

