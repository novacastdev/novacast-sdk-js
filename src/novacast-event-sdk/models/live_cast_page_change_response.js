'use strict';

var Serializer = require('../serializer');


var LiveCastPageChangeResponse = function() {
  
  this.state = null;
  
  this.sync_errors = null;
  

  this._serialize = function() {
    return LiveCastPageChangeResponse._serialize(this);
  };
};

LiveCastPageChangeResponse._serialize = function(data) {
  var obj = {};

  
  obj['state'] = Serializer.serialize(data['state'], 'LiveCastState');
  
  if (data['sync_errors'] != undefined) obj['sync_errors'] = Serializer.serialize(data['sync_errors'], 'Array[String]');
  

  return obj;
};

LiveCastPageChangeResponse._deserialize = function(data) {
  var obj = new LiveCastPageChangeResponse();

  
  obj.state = Serializer.deserialize(data['state'], 'LiveCastState');
  
  obj.sync_errors = Serializer.deserialize(data['sync_errors'], 'Array[String]');
  

  return obj;
};

module.exports = LiveCastPageChangeResponse;

