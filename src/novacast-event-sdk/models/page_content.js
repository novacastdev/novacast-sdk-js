'use strict';

var Serializer = require('../serializer');


var PageContent = function() {
  
  this.event_page_uid = null;
  
  this.locale = null;
  
  this.content = null;
  

  this._serialize = function() {
    return PageContent._serialize(this);
  };
};

PageContent._serialize = function(data) {
  var obj = {};

  
  obj['event_page_uid'] = Serializer.serialize(data['event_page_uid'], 'String');
  
  obj['locale'] = Serializer.serialize(data['locale'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  

  return obj;
};

PageContent._deserialize = function(data) {
  var obj = new PageContent();

  
  obj.event_page_uid = Serializer.deserialize(data['event_page_uid'], 'String');
  
  obj.locale = Serializer.deserialize(data['locale'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'String');
  

  return obj;
};

module.exports = PageContent;

