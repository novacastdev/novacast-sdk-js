'use strict';

var Serializer = require('../serializer');


var UserSetList = function() {
  
  this.user_sets = null;
  

  this._serialize = function() {
    return UserSetList._serialize(this);
  };
};

UserSetList._serialize = function(data) {
  var obj = {};

  
  obj['user_sets'] = Serializer.serialize(data['user_sets'], 'Array[UserSet]');
  

  return obj;
};

UserSetList._deserialize = function(data) {
  var obj = new UserSetList();

  
  obj.user_sets = Serializer.deserialize(data['user_sets'], 'Array[UserSet]');
  

  return obj;
};

module.exports = UserSetList;

