'use strict';

var Serializer = require('../serializer');


var BundleContent = function() {
  
  this.uid = null;
  
  this.meta_data = null;
  
  this.url = null;
  
  this.file_path = null;
  

  this._serialize = function() {
    return BundleContent._serialize(this);
  };
};

BundleContent._serialize = function(data) {
  var obj = {};

  
  if (data['uid'] != undefined) obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  if (data['meta_data'] != undefined) obj['meta_data'] = Serializer.serialize(data['meta_data'], 'Object');
  
  obj['url'] = Serializer.serialize(data['url'], 'String');
  
  obj['file_path'] = Serializer.serialize(data['file_path'], 'String');
  

  return obj;
};

BundleContent._deserialize = function(data) {
  var obj = new BundleContent();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.meta_data = Serializer.deserialize(data['meta_data'], 'Object');
  
  obj.url = Serializer.deserialize(data['url'], 'String');
  
  obj.file_path = Serializer.deserialize(data['file_path'], 'String');
  

  return obj;
};

module.exports = BundleContent;

