'use strict';

var Serializer = require('../serializer');


var ChannelData = function() {
  
  this.name = null;
  

  this._serialize = function() {
    return ChannelData._serialize(this);
  };
};

ChannelData._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  

  return obj;
};

ChannelData._deserialize = function(data) {
  var obj = new ChannelData();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  

  return obj;
};

module.exports = ChannelData;

