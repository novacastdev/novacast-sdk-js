'use strict';

var Serializer = require('../serializer');


var QuestionSetInfoList = function() {
  
  this.sets = null;
  

  this._serialize = function() {
    return QuestionSetInfoList._serialize(this);
  };
};

QuestionSetInfoList._serialize = function(data) {
  var obj = {};

  
  obj['sets'] = Serializer.serialize(data['sets'], 'Array[QuestionSetInfo]');
  

  return obj;
};

QuestionSetInfoList._deserialize = function(data) {
  var obj = new QuestionSetInfoList();

  
  obj.sets = Serializer.deserialize(data['sets'], 'Array[QuestionSetInfo]');
  

  return obj;
};

module.exports = QuestionSetInfoList;

