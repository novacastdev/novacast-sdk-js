'use strict';

var Serializer = require('../serializer');


var QuestionSubmission = function() {
  
  this.user_uid = null;
  
  this.account_info = null;
  
  this.question_content_uid = null;
  
  this.question_manifest_uid = null;
  
  this.answer = null;
  
  this.created_at = null;
  

  this._serialize = function() {
    return QuestionSubmission._serialize(this);
  };
};

QuestionSubmission._serialize = function(data) {
  var obj = {};

  
  if (data['user_uid'] != undefined) obj['user_uid'] = Serializer.serialize(data['user_uid'], 'String');
  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'AccountDisplayInfo');
  
  if (data['question_content_uid'] != undefined) obj['question_content_uid'] = Serializer.serialize(data['question_content_uid'], 'String');
  
  if (data['question_manifest_uid'] != undefined) obj['question_manifest_uid'] = Serializer.serialize(data['question_manifest_uid'], 'String');
  
  obj['answer'] = Serializer.serialize(data['answer'], 'Object');
  
  obj['created_at'] = Serializer.serialize(data['created_at'], 'DateTime');
  

  return obj;
};

QuestionSubmission._deserialize = function(data) {
  var obj = new QuestionSubmission();

  
  obj.user_uid = Serializer.deserialize(data['user_uid'], 'String');
  
  obj.account_info = Serializer.deserialize(data['account_info'], 'AccountDisplayInfo');
  
  obj.question_content_uid = Serializer.deserialize(data['question_content_uid'], 'String');
  
  obj.question_manifest_uid = Serializer.deserialize(data['question_manifest_uid'], 'String');
  
  obj.answer = Serializer.deserialize(data['answer'], 'Object');
  
  obj.created_at = Serializer.deserialize(data['created_at'], 'DateTime');
  

  return obj;
};

module.exports = QuestionSubmission;

