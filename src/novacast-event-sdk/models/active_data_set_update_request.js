'use strict';

var Serializer = require('../serializer');


var ActiveDataSetUpdateRequest = function() {
  
  this.data_set_uid = null;
  

  this._serialize = function() {
    return ActiveDataSetUpdateRequest._serialize(this);
  };
};

ActiveDataSetUpdateRequest._serialize = function(data) {
  var obj = {};

  
  obj['data_set_uid'] = Serializer.serialize(data['data_set_uid'], 'String');
  

  return obj;
};

ActiveDataSetUpdateRequest._deserialize = function(data) {
  var obj = new ActiveDataSetUpdateRequest();

  
  obj.data_set_uid = Serializer.deserialize(data['data_set_uid'], 'String');
  

  return obj;
};

module.exports = ActiveDataSetUpdateRequest;

