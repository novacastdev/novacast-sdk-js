'use strict';

var Serializer = require('../serializer');


var QuestionManifestDeleteResponse = function() {
  
  this.question_manifest_uid = null;
  

  this._serialize = function() {
    return QuestionManifestDeleteResponse._serialize(this);
  };
};

QuestionManifestDeleteResponse._serialize = function(data) {
  var obj = {};

  
  if (data['question_manifest_uid'] != undefined) obj['question_manifest_uid'] = Serializer.serialize(data['question_manifest_uid'], 'String');
  

  return obj;
};

QuestionManifestDeleteResponse._deserialize = function(data) {
  var obj = new QuestionManifestDeleteResponse();

  
  obj.question_manifest_uid = Serializer.deserialize(data['question_manifest_uid'], 'String');
  

  return obj;
};

module.exports = QuestionManifestDeleteResponse;

