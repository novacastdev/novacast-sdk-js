'use strict';

var Serializer = require('../serializer');


var EventPageCreateRequest = function() {
  
  this.label = null;
  
  this.default_locale = null;
  

  this._serialize = function() {
    return EventPageCreateRequest._serialize(this);
  };
};

EventPageCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['default_locale'] = Serializer.serialize(data['default_locale'], 'String');
  

  return obj;
};

EventPageCreateRequest._deserialize = function(data) {
  var obj = new EventPageCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.default_locale = Serializer.deserialize(data['default_locale'], 'String');
  

  return obj;
};

module.exports = EventPageCreateRequest;

