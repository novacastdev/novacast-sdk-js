'use strict';

var Serializer = require('../serializer');


var StreamSourcePreUploadRequest = function() {
  
  this.filename = null;
  

  this._serialize = function() {
    return StreamSourcePreUploadRequest._serialize(this);
  };
};

StreamSourcePreUploadRequest._serialize = function(data) {
  var obj = {};

  
  obj['filename'] = Serializer.serialize(data['filename'], 'String');
  

  return obj;
};

StreamSourcePreUploadRequest._deserialize = function(data) {
  var obj = new StreamSourcePreUploadRequest();

  
  obj.filename = Serializer.deserialize(data['filename'], 'String');
  

  return obj;
};

module.exports = StreamSourcePreUploadRequest;

