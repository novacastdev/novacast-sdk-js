'use strict';

var Serializer = require('../serializer');


var PublicAliasUpdateRequest = function() {
  
  this.path = null;
  
  this.event_uid = null;
  

  this._serialize = function() {
    return PublicAliasUpdateRequest._serialize(this);
  };
};

PublicAliasUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['path'] != undefined) obj['path'] = Serializer.serialize(data['path'], 'String');
  
  if (data['event_uid'] != undefined) obj['event_uid'] = Serializer.serialize(data['event_uid'], 'String');
  

  return obj;
};

PublicAliasUpdateRequest._deserialize = function(data) {
  var obj = new PublicAliasUpdateRequest();

  
  obj.path = Serializer.deserialize(data['path'], 'String');
  
  obj.event_uid = Serializer.deserialize(data['event_uid'], 'String');
  

  return obj;
};

module.exports = PublicAliasUpdateRequest;

