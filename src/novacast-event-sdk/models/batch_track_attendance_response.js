'use strict';

var Serializer = require('../serializer');


var BatchTrackAttendanceResponse = function() {
  
  this.count = null;
  

  this._serialize = function() {
    return BatchTrackAttendanceResponse._serialize(this);
  };
};

BatchTrackAttendanceResponse._serialize = function(data) {
  var obj = {};

  
  obj['count'] = Serializer.serialize(data['count'], 'Integer');
  

  return obj;
};

BatchTrackAttendanceResponse._deserialize = function(data) {
  var obj = new BatchTrackAttendanceResponse();

  
  obj.count = Serializer.deserialize(data['count'], 'Integer');
  

  return obj;
};

module.exports = BatchTrackAttendanceResponse;

