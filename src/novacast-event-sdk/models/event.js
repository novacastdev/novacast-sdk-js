'use strict';

var Serializer = require('../serializer');


var Event = function() {
  
  this.channel_uid = null;
  
  this.uid = null;
  
  this.name = null;
  
  this.stage = null;
  
  this.preview_enabled = null;
  

  this._serialize = function() {
    return Event._serialize(this);
  };
};

Event._serialize = function(data) {
  var obj = {};

  
  if (data['channel_uid'] != undefined) obj['channel_uid'] = Serializer.serialize(data['channel_uid'], 'String');
  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  obj['stage'] = Serializer.serialize(data['stage'], 'String');
  
  obj['preview_enabled'] = Serializer.serialize(data['preview_enabled'], 'BOOLEAN');
  

  return obj;
};

Event._deserialize = function(data) {
  var obj = new Event();

  
  obj.channel_uid = Serializer.deserialize(data['channel_uid'], 'String');
  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.stage = Serializer.deserialize(data['stage'], 'String');
  
  obj.preview_enabled = Serializer.deserialize(data['preview_enabled'], 'BOOLEAN');
  

  return obj;
};

module.exports = Event;

