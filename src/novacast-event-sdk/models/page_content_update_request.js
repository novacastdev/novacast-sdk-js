'use strict';

var Serializer = require('../serializer');


var PageContentUpdateRequest = function() {
  
  this.content_locale = null;
  
  this.content = null;
  

  this._serialize = function() {
    return PageContentUpdateRequest._serialize(this);
  };
};

PageContentUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['content_locale'] != undefined) obj['content_locale'] = Serializer.serialize(data['content_locale'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'String');
  

  return obj;
};

PageContentUpdateRequest._deserialize = function(data) {
  var obj = new PageContentUpdateRequest();

  
  obj.content_locale = Serializer.deserialize(data['content_locale'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'String');
  

  return obj;
};

module.exports = PageContentUpdateRequest;

