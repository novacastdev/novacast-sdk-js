'use strict';

var Serializer = require('../serializer');


var StreamMedium = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.sources = null;
  

  this._serialize = function() {
    return StreamMedium._serialize(this);
  };
};

StreamMedium._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['sources'] = Serializer.serialize(data['sources'], 'Array[StreamSource]');
  

  return obj;
};

StreamMedium._deserialize = function(data) {
  var obj = new StreamMedium();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.sources = Serializer.deserialize(data['sources'], 'Array[StreamSource]');
  

  return obj;
};

module.exports = StreamMedium;

