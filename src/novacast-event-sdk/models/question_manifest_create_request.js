'use strict';

var Serializer = require('../serializer');


var QuestionManifestCreateRequest = function() {
  
  this.label = null;
  
  this.manifest_type = null;
  
  this.question_set_uid = null;
  

  this._serialize = function() {
    return QuestionManifestCreateRequest._serialize(this);
  };
};

QuestionManifestCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['manifest_type'] = Serializer.serialize(data['manifest_type'], 'String');
  
  obj['question_set_uid'] = Serializer.serialize(data['question_set_uid'], 'String');
  

  return obj;
};

QuestionManifestCreateRequest._deserialize = function(data) {
  var obj = new QuestionManifestCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.manifest_type = Serializer.deserialize(data['manifest_type'], 'String');
  
  obj.question_set_uid = Serializer.deserialize(data['question_set_uid'], 'String');
  

  return obj;
};

module.exports = QuestionManifestCreateRequest;

