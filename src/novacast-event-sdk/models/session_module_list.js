'use strict';

var Serializer = require('../serializer');


var SessionModuleList = function() {
  
  this.modules = null;
  

  this._serialize = function() {
    return SessionModuleList._serialize(this);
  };
};

SessionModuleList._serialize = function(data) {
  var obj = {};

  
  obj['modules'] = Serializer.serialize(data['modules'], 'Array[SessionModule]');
  

  return obj;
};

SessionModuleList._deserialize = function(data) {
  var obj = new SessionModuleList();

  
  obj.modules = Serializer.deserialize(data['modules'], 'Array[SessionModule]');
  

  return obj;
};

module.exports = SessionModuleList;

