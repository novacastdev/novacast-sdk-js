'use strict';

var Serializer = require('../serializer');


var FilterAccessResponse = function() {
  
  this.preview_mode = null;
  
  this.view_event_permission = null;
  
  this.action = null;
  
  this.action_target = null;
  

  this._serialize = function() {
    return FilterAccessResponse._serialize(this);
  };
};

FilterAccessResponse._serialize = function(data) {
  var obj = {};

  
  if (data['preview_mode'] != undefined) obj['preview_mode'] = Serializer.serialize(data['preview_mode'], 'BOOLEAN');
  
  if (data['view_event_permission'] != undefined) obj['view_event_permission'] = Serializer.serialize(data['view_event_permission'], 'BOOLEAN');
  
  if (data['action'] != undefined) obj['action'] = Serializer.serialize(data['action'], 'String');
  
  if (data['action_target'] != undefined) obj['action_target'] = Serializer.serialize(data['action_target'], 'String');
  

  return obj;
};

FilterAccessResponse._deserialize = function(data) {
  var obj = new FilterAccessResponse();

  
  obj.preview_mode = Serializer.deserialize(data['preview_mode'], 'BOOLEAN');
  
  obj.view_event_permission = Serializer.deserialize(data['view_event_permission'], 'BOOLEAN');
  
  obj.action = Serializer.deserialize(data['action'], 'String');
  
  obj.action_target = Serializer.deserialize(data['action_target'], 'String');
  

  return obj;
};

module.exports = FilterAccessResponse;

