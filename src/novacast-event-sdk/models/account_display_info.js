'use strict';

var Serializer = require('../serializer');


var AccountDisplayInfo = function() {
  
  this.display_name = null;
  
  this.username = null;
  
  this.name = null;
  
  this.email = null;
  
  this.company = null;
  
  this.title = null;
  
  this.country = null;
  

  this._serialize = function() {
    return AccountDisplayInfo._serialize(this);
  };
};

AccountDisplayInfo._serialize = function(data) {
  var obj = {};

  
  if (data['display_name'] != undefined) obj['display_name'] = Serializer.serialize(data['display_name'], 'String');
  
  if (data['username'] != undefined) obj['username'] = Serializer.serialize(data['username'], 'String');
  
  if (data['name'] != undefined) obj['name'] = Serializer.serialize(data['name'], 'String');
  
  if (data['email'] != undefined) obj['email'] = Serializer.serialize(data['email'], 'String');
  
  if (data['company'] != undefined) obj['company'] = Serializer.serialize(data['company'], 'String');
  
  if (data['title'] != undefined) obj['title'] = Serializer.serialize(data['title'], 'String');
  
  if (data['country'] != undefined) obj['country'] = Serializer.serialize(data['country'], 'String');
  

  return obj;
};

AccountDisplayInfo._deserialize = function(data) {
  var obj = new AccountDisplayInfo();

  
  obj.display_name = Serializer.deserialize(data['display_name'], 'String');
  
  obj.username = Serializer.deserialize(data['username'], 'String');
  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.email = Serializer.deserialize(data['email'], 'String');
  
  obj.company = Serializer.deserialize(data['company'], 'String');
  
  obj.title = Serializer.deserialize(data['title'], 'String');
  
  obj.country = Serializer.deserialize(data['country'], 'String');
  

  return obj;
};

module.exports = AccountDisplayInfo;

