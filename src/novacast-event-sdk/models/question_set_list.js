'use strict';

var Serializer = require('../serializer');


var QuestionSetList = function() {
  
  this.sets = null;
  

  this._serialize = function() {
    return QuestionSetList._serialize(this);
  };
};

QuestionSetList._serialize = function(data) {
  var obj = {};

  
  obj['sets'] = Serializer.serialize(data['sets'], 'Array[QuestionSet]');
  

  return obj;
};

QuestionSetList._deserialize = function(data) {
  var obj = new QuestionSetList();

  
  obj.sets = Serializer.deserialize(data['sets'], 'Array[QuestionSet]');
  

  return obj;
};

module.exports = QuestionSetList;

