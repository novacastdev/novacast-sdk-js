'use strict';

var Serializer = require('../serializer');


var QuestionContentUpdateRequest = function() {
  
  this.question = null;
  
  this.order = null;
  
  this.content = null;
  
  this.type = null;
  

  this._serialize = function() {
    return QuestionContentUpdateRequest._serialize(this);
  };
};

QuestionContentUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['question'] != undefined) obj['question'] = Serializer.serialize(data['question'], 'String');
  
  if (data['order'] != undefined) obj['order'] = Serializer.serialize(data['order'], 'String');
  
  if (data['content'] != undefined) obj['content'] = Serializer.serialize(data['content'], 'Object');
  
  if (data['type'] != undefined) obj['type'] = Serializer.serialize(data['type'], 'String');
  

  return obj;
};

QuestionContentUpdateRequest._deserialize = function(data) {
  var obj = new QuestionContentUpdateRequest();

  
  obj.question = Serializer.deserialize(data['question'], 'String');
  
  obj.order = Serializer.deserialize(data['order'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'Object');
  
  obj.type = Serializer.deserialize(data['type'], 'String');
  

  return obj;
};

module.exports = QuestionContentUpdateRequest;

