'use strict';

var Serializer = require('../serializer');


var QuestionSetInfo = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.state = null;
  

  this._serialize = function() {
    return QuestionSetInfo._serialize(this);
  };
};

QuestionSetInfo._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['state'] = Serializer.serialize(data['state'], 'String');
  

  return obj;
};

QuestionSetInfo._deserialize = function(data) {
  var obj = new QuestionSetInfo();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.state = Serializer.deserialize(data['state'], 'String');
  

  return obj;
};

module.exports = QuestionSetInfo;

