'use strict';

var Serializer = require('../serializer');


var SessionModule = function() {
  
  this.config = null;
  
  this.resources = null;
  
  this.module_name = null;
  

  this._serialize = function() {
    return SessionModule._serialize(this);
  };
};

SessionModule._serialize = function(data) {
  var obj = {};

  
  obj['config'] = Serializer.serialize(data['config'], 'Object');
  
  if (data['resources'] != undefined) obj['resources'] = Serializer.serialize(data['resources'], 'Array[SessionModuleResource]');
  
  obj['module_name'] = Serializer.serialize(data['module_name'], 'String');
  

  return obj;
};

SessionModule._deserialize = function(data) {
  var obj = new SessionModule();

  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  
  obj.resources = Serializer.deserialize(data['resources'], 'Array[SessionModuleResource]');
  
  obj.module_name = Serializer.deserialize(data['module_name'], 'String');
  

  return obj;
};

module.exports = SessionModule;

