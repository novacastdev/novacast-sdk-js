'use strict';

var Serializer = require('../serializer');


var DataSet = function() {
  
  this.uid = null;
  
  this.label = null;
  
  this.production = null;
  

  this._serialize = function() {
    return DataSet._serialize(this);
  };
};

DataSet._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['production'] = Serializer.serialize(data['production'], 'BOOLEAN');
  

  return obj;
};

DataSet._deserialize = function(data) {
  var obj = new DataSet();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.production = Serializer.deserialize(data['production'], 'BOOLEAN');
  

  return obj;
};

module.exports = DataSet;

