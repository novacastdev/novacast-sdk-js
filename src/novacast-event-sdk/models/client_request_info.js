'use strict';

var Serializer = require('../serializer');


var ClientRequestInfo = function() {
  
  this.method = null;
  
  this.protocol = null;
  
  this.remote_ip = null;
  
  this.referrer = null;
  
  this.origin = null;
  

  this._serialize = function() {
    return ClientRequestInfo._serialize(this);
  };
};

ClientRequestInfo._serialize = function(data) {
  var obj = {};

  
  obj['method'] = Serializer.serialize(data['method'], 'String');
  
  obj['protocol'] = Serializer.serialize(data['protocol'], 'String');
  
  obj['remote_ip'] = Serializer.serialize(data['remote_ip'], 'String');
  
  if (data['referrer'] != undefined) obj['referrer'] = Serializer.serialize(data['referrer'], 'String');
  
  if (data['origin'] != undefined) obj['origin'] = Serializer.serialize(data['origin'], 'String');
  

  return obj;
};

ClientRequestInfo._deserialize = function(data) {
  var obj = new ClientRequestInfo();

  
  obj.method = Serializer.deserialize(data['method'], 'String');
  
  obj.protocol = Serializer.deserialize(data['protocol'], 'String');
  
  obj.remote_ip = Serializer.deserialize(data['remote_ip'], 'String');
  
  obj.referrer = Serializer.deserialize(data['referrer'], 'String');
  
  obj.origin = Serializer.deserialize(data['origin'], 'String');
  

  return obj;
};

module.exports = ClientRequestInfo;

