'use strict';

var Serializer = require('../serializer');


var BundleContentConfirmRequest = function() {
  
  this.token = null;
  
  this.file_path = null;
  

  this._serialize = function() {
    return BundleContentConfirmRequest._serialize(this);
  };
};

BundleContentConfirmRequest._serialize = function(data) {
  var obj = {};

  
  obj['token'] = Serializer.serialize(data['token'], 'String');
  
  obj['file_path'] = Serializer.serialize(data['file_path'], 'String');
  

  return obj;
};

BundleContentConfirmRequest._deserialize = function(data) {
  var obj = new BundleContentConfirmRequest();

  
  obj.token = Serializer.deserialize(data['token'], 'String');
  
  obj.file_path = Serializer.deserialize(data['file_path'], 'String');
  

  return obj;
};

module.exports = BundleContentConfirmRequest;

