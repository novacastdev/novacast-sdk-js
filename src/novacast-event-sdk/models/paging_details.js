'use strict';

var Serializer = require('../serializer');


var PagingDetails = function() {
  
  this.current = null;
  
  this.total = null;
  
  this.per_page = null;
  
  this.item_count = null;
  

  this._serialize = function() {
    return PagingDetails._serialize(this);
  };
};

PagingDetails._serialize = function(data) {
  var obj = {};

  
  obj['current'] = Serializer.serialize(data['current'], 'Integer');
  
  obj['total'] = Serializer.serialize(data['total'], 'Integer');
  
  obj['per_page'] = Serializer.serialize(data['per_page'], 'Integer');
  
  obj['item_count'] = Serializer.serialize(data['item_count'], 'Integer');
  

  return obj;
};

PagingDetails._deserialize = function(data) {
  var obj = new PagingDetails();

  
  obj.current = Serializer.deserialize(data['current'], 'Integer');
  
  obj.total = Serializer.deserialize(data['total'], 'Integer');
  
  obj.per_page = Serializer.deserialize(data['per_page'], 'Integer');
  
  obj.item_count = Serializer.deserialize(data['item_count'], 'Integer');
  

  return obj;
};

module.exports = PagingDetails;

