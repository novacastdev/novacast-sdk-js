'use strict';

var Serializer = require('../serializer');


var EventContent = function() {
  
  this.type = null;
  
  this.value = null;
  
  this.public = null;
  
  this.locale = null;
  
  this.session_uid = null;
  
  this.timestamp = null;
  

  this._serialize = function() {
    return EventContent._serialize(this);
  };
};

EventContent._serialize = function(data) {
  var obj = {};

  
  obj['type'] = Serializer.serialize(data['type'], 'String');
  
  obj['value'] = Serializer.serialize(data['value'], 'String');
  
  if (data['public'] != undefined) obj['public'] = Serializer.serialize(data['public'], 'BOOLEAN');
  
  if (data['locale'] != undefined) obj['locale'] = Serializer.serialize(data['locale'], 'String');
  
  if (data['session_uid'] != undefined) obj['session_uid'] = Serializer.serialize(data['session_uid'], 'String');
  
  if (data['timestamp'] != undefined) obj['timestamp'] = Serializer.serialize(data['timestamp'], 'DateTime');
  

  return obj;
};

EventContent._deserialize = function(data) {
  var obj = new EventContent();

  
  obj.type = Serializer.deserialize(data['type'], 'String');
  
  obj.value = Serializer.deserialize(data['value'], 'String');
  
  obj.public = Serializer.deserialize(data['public'], 'BOOLEAN');
  
  obj.locale = Serializer.deserialize(data['locale'], 'String');
  
  obj.session_uid = Serializer.deserialize(data['session_uid'], 'String');
  
  obj.timestamp = Serializer.deserialize(data['timestamp'], 'DateTime');
  

  return obj;
};

module.exports = EventContent;

