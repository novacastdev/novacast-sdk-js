'use strict';

var Serializer = require('../serializer');


var StreamSourceConfirmRequest = function() {
  
  this.token = null;
  

  this._serialize = function() {
    return StreamSourceConfirmRequest._serialize(this);
  };
};

StreamSourceConfirmRequest._serialize = function(data) {
  var obj = {};

  
  obj['token'] = Serializer.serialize(data['token'], 'String');
  

  return obj;
};

StreamSourceConfirmRequest._deserialize = function(data) {
  var obj = new StreamSourceConfirmRequest();

  
  obj.token = Serializer.deserialize(data['token'], 'String');
  

  return obj;
};

module.exports = StreamSourceConfirmRequest;

