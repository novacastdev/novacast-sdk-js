'use strict';

var Serializer = require('../serializer');


var AccountExtended = function() {
  
  this.uid = null;
  
  this.identifier = null;
  
  this.domain = null;
  
  this.provider = null;
  
  this.resource_roles = null;
  

  this._serialize = function() {
    return AccountExtended._serialize(this);
  };
};

AccountExtended._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  obj['identifier'] = Serializer.serialize(data['identifier'], 'String');
  
  obj['domain'] = Serializer.serialize(data['domain'], 'String');
  
  obj['provider'] = Serializer.serialize(data['provider'], 'String');
  
  if (data['resource_roles'] != undefined) obj['resource_roles'] = Serializer.serialize(data['resource_roles'], 'Array[AccountResourceRoles]');
  

  return obj;
};

AccountExtended._deserialize = function(data) {
  var obj = new AccountExtended();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.identifier = Serializer.deserialize(data['identifier'], 'String');
  
  obj.domain = Serializer.deserialize(data['domain'], 'String');
  
  obj.provider = Serializer.deserialize(data['provider'], 'String');
  
  obj.resource_roles = Serializer.deserialize(data['resource_roles'], 'Array[AccountResourceRoles]');
  

  return obj;
};

module.exports = AccountExtended;

