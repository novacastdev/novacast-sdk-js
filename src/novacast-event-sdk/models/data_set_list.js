'use strict';

var Serializer = require('../serializer');


var DataSetList = function() {
  
  this.data_sets = null;
  

  this._serialize = function() {
    return DataSetList._serialize(this);
  };
};

DataSetList._serialize = function(data) {
  var obj = {};

  
  obj['data_sets'] = Serializer.serialize(data['data_sets'], 'Array[DataSet]');
  

  return obj;
};

DataSetList._deserialize = function(data) {
  var obj = new DataSetList();

  
  obj.data_sets = Serializer.deserialize(data['data_sets'], 'Array[DataSet]');
  

  return obj;
};

module.exports = DataSetList;

