'use strict';

var Serializer = require('../serializer');


var QuestionContentCreateRequest = function() {
  
  this.question = null;
  
  this.order = null;
  
  this.content = null;
  
  this.type = null;
  

  this._serialize = function() {
    return QuestionContentCreateRequest._serialize(this);
  };
};

QuestionContentCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['question'] = Serializer.serialize(data['question'], 'String');
  
  obj['order'] = Serializer.serialize(data['order'], 'String');
  
  obj['content'] = Serializer.serialize(data['content'], 'Object');
  
  obj['type'] = Serializer.serialize(data['type'], 'String');
  

  return obj;
};

QuestionContentCreateRequest._deserialize = function(data) {
  var obj = new QuestionContentCreateRequest();

  
  obj.question = Serializer.deserialize(data['question'], 'String');
  
  obj.order = Serializer.deserialize(data['order'], 'String');
  
  obj.content = Serializer.deserialize(data['content'], 'Object');
  
  obj.type = Serializer.deserialize(data['type'], 'String');
  

  return obj;
};

module.exports = QuestionContentCreateRequest;

