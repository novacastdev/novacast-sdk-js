'use strict';

var Serializer = require('../serializer');


var ChannelList = function() {
  
  this.channels = null;
  

  this._serialize = function() {
    return ChannelList._serialize(this);
  };
};

ChannelList._serialize = function(data) {
  var obj = {};

  
  obj['channels'] = Serializer.serialize(data['channels'], 'Array[Channel]');
  

  return obj;
};

ChannelList._deserialize = function(data) {
  var obj = new ChannelList();

  
  obj.channels = Serializer.deserialize(data['channels'], 'Array[Channel]');
  

  return obj;
};

module.exports = ChannelList;

