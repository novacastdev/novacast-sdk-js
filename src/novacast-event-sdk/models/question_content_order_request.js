'use strict';

var Serializer = require('../serializer');


var QuestionContentOrderRequest = function() {
  
  this.content_orders = null;
  

  this._serialize = function() {
    return QuestionContentOrderRequest._serialize(this);
  };
};

QuestionContentOrderRequest._serialize = function(data) {
  var obj = {};

  
  obj['content_orders'] = Serializer.serialize(data['content_orders'], 'Hash[String, Integer]');
  

  return obj;
};

QuestionContentOrderRequest._deserialize = function(data) {
  var obj = new QuestionContentOrderRequest();

  
  obj.content_orders = Serializer.deserialize(data['content_orders'], 'Hash[String, Integer]');
  

  return obj;
};

module.exports = QuestionContentOrderRequest;

