'use strict';

var Serializer = require('../serializer');


var AttendanceResponse = function() {
  
  this.attendances = null;
  
  this.account_info = null;
  

  this._serialize = function() {
    return AttendanceResponse._serialize(this);
  };
};

AttendanceResponse._serialize = function(data) {
  var obj = {};

  
  obj['attendances'] = Serializer.serialize(data['attendances'], 'Array[Attendance]');
  
  if (data['account_info'] != undefined) obj['account_info'] = Serializer.serialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  

  return obj;
};

AttendanceResponse._deserialize = function(data) {
  var obj = new AttendanceResponse();

  
  obj.attendances = Serializer.deserialize(data['attendances'], 'Array[Attendance]');
  
  obj.account_info = Serializer.deserialize(data['account_info'], 'Hash[String, AccountDisplayInfo]');
  

  return obj;
};

module.exports = AttendanceResponse;

