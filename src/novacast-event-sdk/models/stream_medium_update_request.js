'use strict';

var Serializer = require('../serializer');


var StreamMediumUpdateRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return StreamMediumUpdateRequest._serialize(this);
  };
};

StreamMediumUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

StreamMediumUpdateRequest._deserialize = function(data) {
  var obj = new StreamMediumUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = StreamMediumUpdateRequest;

