'use strict';

var Serializer = require('../serializer');


var Paging = function() {
  
  this.paging = null;
  

  this._serialize = function() {
    return Paging._serialize(this);
  };
};

Paging._serialize = function(data) {
  var obj = {};

  
  if (data['paging'] != undefined) obj['paging'] = Serializer.serialize(data['paging'], 'PagingDetails');
  

  return obj;
};

Paging._deserialize = function(data) {
  var obj = new Paging();

  
  obj.paging = Serializer.deserialize(data['paging'], 'PagingDetails');
  

  return obj;
};

module.exports = Paging;

