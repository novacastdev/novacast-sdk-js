'use strict';

var Serializer = require('../serializer');


var UserSetCreateRequest = function() {
  
  this.label = null;
  
  this.ch_acct_only = null;
  
  this.whitelisted_only = null;
  
  this.full_enrollment = null;
  
  this.passcode = null;
  
  this.fields = null;
  

  this._serialize = function() {
    return UserSetCreateRequest._serialize(this);
  };
};

UserSetCreateRequest._serialize = function(data) {
  var obj = {};

  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  if (data['ch_acct_only'] != undefined) obj['ch_acct_only'] = Serializer.serialize(data['ch_acct_only'], 'BOOLEAN');
  
  if (data['whitelisted_only'] != undefined) obj['whitelisted_only'] = Serializer.serialize(data['whitelisted_only'], 'BOOLEAN');
  
  if (data['full_enrollment'] != undefined) obj['full_enrollment'] = Serializer.serialize(data['full_enrollment'], 'BOOLEAN');
  
  if (data['passcode'] != undefined) obj['passcode'] = Serializer.serialize(data['passcode'], 'String');
  
  if (data['fields'] != undefined) obj['fields'] = Serializer.serialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

UserSetCreateRequest._deserialize = function(data) {
  var obj = new UserSetCreateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.ch_acct_only = Serializer.deserialize(data['ch_acct_only'], 'BOOLEAN');
  
  obj.whitelisted_only = Serializer.deserialize(data['whitelisted_only'], 'BOOLEAN');
  
  obj.full_enrollment = Serializer.deserialize(data['full_enrollment'], 'BOOLEAN');
  
  obj.passcode = Serializer.deserialize(data['passcode'], 'String');
  
  obj.fields = Serializer.deserialize(data['fields'], 'Array[EnrollmentField]');
  

  return obj;
};

module.exports = UserSetCreateRequest;

