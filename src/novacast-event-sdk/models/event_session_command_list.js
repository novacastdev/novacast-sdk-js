'use strict';

var Serializer = require('../serializer');


var EventSessionCommandList = function() {
  
  this.commands = null;
  

  this._serialize = function() {
    return EventSessionCommandList._serialize(this);
  };
};

EventSessionCommandList._serialize = function(data) {
  var obj = {};

  
  obj['commands'] = Serializer.serialize(data['commands'], 'Array[EventSessionCommand]');
  

  return obj;
};

EventSessionCommandList._deserialize = function(data) {
  var obj = new EventSessionCommandList();

  
  obj.commands = Serializer.deserialize(data['commands'], 'Array[EventSessionCommand]');
  

  return obj;
};

module.exports = EventSessionCommandList;

