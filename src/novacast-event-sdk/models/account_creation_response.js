'use strict';

var Serializer = require('../serializer');


var AccountCreationResponse = function() {
  
  this.account = null;
  
  this.error = null;
  

  this._serialize = function() {
    return AccountCreationResponse._serialize(this);
  };
};

AccountCreationResponse._serialize = function(data) {
  var obj = {};

  
  if (data['account'] != undefined) obj['account'] = Serializer.serialize(data['account'], 'Account');
  
  if (data['error'] != undefined) obj['error'] = Serializer.serialize(data['error'], 'Error');
  

  return obj;
};

AccountCreationResponse._deserialize = function(data) {
  var obj = new AccountCreationResponse();

  
  obj.account = Serializer.deserialize(data['account'], 'Account');
  
  obj.error = Serializer.deserialize(data['error'], 'Error');
  

  return obj;
};

module.exports = AccountCreationResponse;

