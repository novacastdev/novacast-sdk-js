'use strict';

var Serializer = require('../serializer');


var PollStatusControlRequest = function() {
  
  this.question_content_uid = null;
  

  this._serialize = function() {
    return PollStatusControlRequest._serialize(this);
  };
};

PollStatusControlRequest._serialize = function(data) {
  var obj = {};

  
  obj['question_content_uid'] = Serializer.serialize(data['question_content_uid'], 'String');
  

  return obj;
};

PollStatusControlRequest._deserialize = function(data) {
  var obj = new PollStatusControlRequest();

  
  obj.question_content_uid = Serializer.deserialize(data['question_content_uid'], 'String');
  

  return obj;
};

module.exports = PollStatusControlRequest;

