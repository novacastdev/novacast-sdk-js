'use strict';

var Serializer = require('../serializer');


var QuestionManifest = function() {
  
  this.uid = null;
  
  this.rn = null;
  
  this.label = null;
  
  this.manifest_type = null;
  
  this.config = null;
  
  this.created_timestamp = null;
  
  this.question_set = null;
  

  this._serialize = function() {
    return QuestionManifest._serialize(this);
  };
};

QuestionManifest._serialize = function(data) {
  var obj = {};

  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  
  if (data['rn'] != undefined) obj['rn'] = Serializer.serialize(data['rn'], 'String');
  
  obj['label'] = Serializer.serialize(data['label'], 'String');
  
  obj['manifest_type'] = Serializer.serialize(data['manifest_type'], 'String');
  
  if (data['config'] != undefined) obj['config'] = Serializer.serialize(data['config'], 'Object');
  
  if (data['created_timestamp'] != undefined) obj['created_timestamp'] = Serializer.serialize(data['created_timestamp'], 'Integer');
  
  obj['question_set'] = Serializer.serialize(data['question_set'], 'QuestionSetInfo');
  

  return obj;
};

QuestionManifest._deserialize = function(data) {
  var obj = new QuestionManifest();

  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  
  obj.rn = Serializer.deserialize(data['rn'], 'String');
  
  obj.label = Serializer.deserialize(data['label'], 'String');
  
  obj.manifest_type = Serializer.deserialize(data['manifest_type'], 'String');
  
  obj.config = Serializer.deserialize(data['config'], 'Object');
  
  obj.created_timestamp = Serializer.deserialize(data['created_timestamp'], 'Integer');
  
  obj.question_set = Serializer.deserialize(data['question_set'], 'QuestionSetInfo');
  

  return obj;
};

module.exports = QuestionManifest;

