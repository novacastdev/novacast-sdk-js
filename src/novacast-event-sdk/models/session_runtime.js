'use strict';

var Serializer = require('../serializer');


var SessionRuntime = function() {
  
  this.session_uid = null;
  
  this.modules = null;
  

  this._serialize = function() {
    return SessionRuntime._serialize(this);
  };
};

SessionRuntime._serialize = function(data) {
  var obj = {};

  
  obj['session_uid'] = Serializer.serialize(data['session_uid'], 'String');
  
  obj['modules'] = Serializer.serialize(data['modules'], 'Array[ModuleRuntime]');
  

  return obj;
};

SessionRuntime._deserialize = function(data) {
  var obj = new SessionRuntime();

  
  obj.session_uid = Serializer.deserialize(data['session_uid'], 'String');
  
  obj.modules = Serializer.deserialize(data['modules'], 'Array[ModuleRuntime]');
  

  return obj;
};

module.exports = SessionRuntime;

