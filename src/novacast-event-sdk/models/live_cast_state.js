'use strict';

var Serializer = require('../serializer');


var LiveCastState = function() {
  
  this.page = null;
  

  this._serialize = function() {
    return LiveCastState._serialize(this);
  };
};

LiveCastState._serialize = function(data) {
  var obj = {};

  
  if (data['page'] != undefined) obj['page'] = Serializer.serialize(data['page'], 'Integer');
  

  return obj;
};

LiveCastState._deserialize = function(data) {
  var obj = new LiveCastState();

  
  obj.page = Serializer.deserialize(data['page'], 'Integer');
  

  return obj;
};

module.exports = LiveCastState;

