'use strict';

var Serializer = require('../serializer');


var SlideDeckUpdateRequest = function() {
  
  this.label = null;
  

  this._serialize = function() {
    return SlideDeckUpdateRequest._serialize(this);
  };
};

SlideDeckUpdateRequest._serialize = function(data) {
  var obj = {};

  
  if (data['label'] != undefined) obj['label'] = Serializer.serialize(data['label'], 'String');
  

  return obj;
};

SlideDeckUpdateRequest._deserialize = function(data) {
  var obj = new SlideDeckUpdateRequest();

  
  obj.label = Serializer.deserialize(data['label'], 'String');
  

  return obj;
};

module.exports = SlideDeckUpdateRequest;

