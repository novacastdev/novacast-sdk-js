'use strict';

var Serializer = require('../serializer');


var Channel = function() {
  
  this.name = null;
  
  this.uid = null;
  

  this._serialize = function() {
    return Channel._serialize(this);
  };
};

Channel._serialize = function(data) {
  var obj = {};

  
  obj['name'] = Serializer.serialize(data['name'], 'String');
  
  obj['uid'] = Serializer.serialize(data['uid'], 'String');
  

  return obj;
};

Channel._deserialize = function(data) {
  var obj = new Channel();

  
  obj.name = Serializer.deserialize(data['name'], 'String');
  
  obj.uid = Serializer.deserialize(data['uid'], 'String');
  

  return obj;
};

module.exports = Channel;

