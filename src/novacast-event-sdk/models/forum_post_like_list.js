'use strict';

var Serializer = require('../serializer');


var ForumPostLikeList = function() {
  
  this.likes = null;
  

  this._serialize = function() {
    return ForumPostLikeList._serialize(this);
  };
};

ForumPostLikeList._serialize = function(data) {
  var obj = {};

  
  obj['likes'] = Serializer.serialize(data['likes'], 'Array[ForumPostLike]');
  

  return obj;
};

ForumPostLikeList._deserialize = function(data) {
  var obj = new ForumPostLikeList();

  
  obj.likes = Serializer.deserialize(data['likes'], 'Array[ForumPostLike]');
  

  return obj;
};

module.exports = ForumPostLikeList;

