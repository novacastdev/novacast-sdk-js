'use strict';

var MODELS = {};

MODELS['AccessFilter'] = require('./models/access_filter');

MODELS['AccessPolicy'] = require('./models/access_policy');

MODELS['AccessPolicyCreateRequest'] = require('./models/access_policy_create_request');

MODELS['AccessPolicyExtended'] = require('./models/access_policy_extended');

MODELS['AccessPolicyList'] = require('./models/access_policy_list');

MODELS['AccessPolicyUpdateRequest'] = require('./models/access_policy_update_request');

MODELS['Account'] = require('./models/account');

MODELS['AccountCreationResponse'] = require('./models/account_creation_response');

MODELS['AccountCreationResponseList'] = require('./models/account_creation_response_list');

MODELS['AccountDisplayInfo'] = require('./models/account_display_info');

MODELS['AccountExtended'] = require('./models/account_extended');

MODELS['AccountExtendedList'] = require('./models/account_extended_list');

MODELS['AccountList'] = require('./models/account_list');

MODELS['AccountRequest'] = require('./models/account_request');

MODELS['AccountResourceRoles'] = require('./models/account_resource_roles');

MODELS['AccountResourceRolesList'] = require('./models/account_resource_roles_list');

MODELS['AccountRoleUpdateRequest'] = require('./models/account_role_update_request');

MODELS['ActiveDataSetUpdateRequest'] = require('./models/active_data_set_update_request');

MODELS['ActivePath'] = require('./models/active_path');

MODELS['ActivePathList'] = require('./models/active_path_list');

MODELS['ActivePoll'] = require('./models/active_poll');

MODELS['ActivePollList'] = require('./models/active_poll_list');

MODELS['ActiveQuestionnaire'] = require('./models/active_questionnaire');

MODELS['ActiveQuestionnaireList'] = require('./models/active_questionnaire_list');

MODELS['AddPollStatRequest'] = require('./models/add_poll_stat_request');

MODELS['AddquestionnaireStatRequest'] = require('./models/addquestionnaire_stat_request');

MODELS['ArchiveActivity'] = require('./models/archive_activity');

MODELS['ArchiveActivityList'] = require('./models/archive_activity_list');

MODELS['AssetBundle'] = require('./models/asset_bundle');

MODELS['AssetBundleCreateRequest'] = require('./models/asset_bundle_create_request');

MODELS['AssetBundleInfo'] = require('./models/asset_bundle_info');

MODELS['AssetBundleInfoList'] = require('./models/asset_bundle_info_list');

MODELS['Attendance'] = require('./models/attendance');

MODELS['AttendanceResponse'] = require('./models/attendance_response');

MODELS['BatchTrackAttendanceRequest'] = require('./models/batch_track_attendance_request');

MODELS['BatchTrackAttendanceResponse'] = require('./models/batch_track_attendance_response');

MODELS['BundleContent'] = require('./models/bundle_content');

MODELS['BundleContentConfirmRequest'] = require('./models/bundle_content_confirm_request');

MODELS['BundleContentPreUploadRequest'] = require('./models/bundle_content_pre_upload_request');

MODELS['Channel'] = require('./models/channel');

MODELS['ChannelBatchRequest'] = require('./models/channel_batch_request');

MODELS['ChannelData'] = require('./models/channel_data');

MODELS['ChannelList'] = require('./models/channel_list');

MODELS['ClientRequestInfo'] = require('./models/client_request_info');

MODELS['DataSet'] = require('./models/data_set');

MODELS['DataSetCreateRequest'] = require('./models/data_set_create_request');

MODELS['DataSetList'] = require('./models/data_set_list');

MODELS['DataSetPartition'] = require('./models/data_set_partition');

MODELS['DataSetPartitionList'] = require('./models/data_set_partition_list');

MODELS['DictionaryEntry'] = require('./models/dictionary_entry');

MODELS['DictionaryFilterResponse'] = require('./models/dictionary_filter_response');

MODELS['DictionaryQueryResponse'] = require('./models/dictionary_query_response');

MODELS['Enrollment'] = require('./models/enrollment');

MODELS['EnrollmentExtended'] = require('./models/enrollment_extended');

MODELS['EnrollmentField'] = require('./models/enrollment_field');

MODELS['EnrollmentFieldValue'] = require('./models/enrollment_field_value');

MODELS['EnrollmentListResponse'] = require('./models/enrollment_list_response');

MODELS['Error'] = require('./models/error');

MODELS['ErrorField'] = require('./models/error_field');

MODELS['Event'] = require('./models/event');

MODELS['EventContent'] = require('./models/event_content');

MODELS['EventCreateRequest'] = require('./models/event_create_request');

MODELS['EventExtended'] = require('./models/event_extended');

MODELS['EventList'] = require('./models/event_list');

MODELS['EventPage'] = require('./models/event_page');

MODELS['EventPageCreateRequest'] = require('./models/event_page_create_request');

MODELS['EventPageList'] = require('./models/event_page_list');

MODELS['EventPageUpdateRequest'] = require('./models/event_page_update_request');

MODELS['EventSession'] = require('./models/event_session');

MODELS['EventSessionCommand'] = require('./models/event_session_command');

MODELS['EventSessionCommandList'] = require('./models/event_session_command_list');

MODELS['EventSessionExtended'] = require('./models/event_session_extended');

MODELS['EventSessionInfoRequest'] = require('./models/event_session_info_request');

MODELS['EventSessionList'] = require('./models/event_session_list');

MODELS['EventStageRequest'] = require('./models/event_stage_request');

MODELS['EventUpdateRequest'] = require('./models/event_update_request');

MODELS['FilterAccessRequest'] = require('./models/filter_access_request');

MODELS['FilterAccessResponse'] = require('./models/filter_access_response');

MODELS['ForumPost'] = require('./models/forum_post');

MODELS['ForumPostCreateRequest'] = require('./models/forum_post_create_request');

MODELS['ForumPostExtended'] = require('./models/forum_post_extended');

MODELS['ForumPostExtendedList'] = require('./models/forum_post_extended_list');

MODELS['ForumPostLike'] = require('./models/forum_post_like');

MODELS['ForumPostLikeList'] = require('./models/forum_post_like_list');

MODELS['ForumPostLikeRequest'] = require('./models/forum_post_like_request');

MODELS['ForumPostList'] = require('./models/forum_post_list');

MODELS['ForumPostUpdateRequest'] = require('./models/forum_post_update_request');

MODELS['GetAccountDisplayInfoRequest'] = require('./models/get_account_display_info_request');

MODELS['GetAccountDisplayInfoResponse'] = require('./models/get_account_display_info_response');

MODELS['LiveCastPageChangeRequest'] = require('./models/live_cast_page_change_request');

MODELS['LiveCastPageChangeResponse'] = require('./models/live_cast_page_change_response');

MODELS['LiveCastSlidePage'] = require('./models/live_cast_slide_page');

MODELS['LiveCastState'] = require('./models/live_cast_state');

MODELS['ManifestSubmissions'] = require('./models/manifest_submissions');

MODELS['ModuleRuntime'] = require('./models/module_runtime');

MODELS['PageContent'] = require('./models/page_content');

MODELS['PageContentDeleteResponse'] = require('./models/page_content_delete_response');

MODELS['PageContentList'] = require('./models/page_content_list');

MODELS['PageContentUpdateRequest'] = require('./models/page_content_update_request');

MODELS['PageRuntime'] = require('./models/page_runtime');

MODELS['Paging'] = require('./models/paging');

MODELS['PagingDetails'] = require('./models/paging_details');

MODELS['PathMapping'] = require('./models/path_mapping');

MODELS['PathMappingRequest'] = require('./models/path_mapping_request');

MODELS['PollStatus'] = require('./models/poll_status');

MODELS['PollStatusControlRequest'] = require('./models/poll_status_control_request');

MODELS['PresignedUpload'] = require('./models/presigned_upload');

MODELS['PreviewToken'] = require('./models/preview_token');

MODELS['PreviewTokenInfo'] = require('./models/preview_token_info');

MODELS['PublicAlias'] = require('./models/public_alias');

MODELS['PublicAliasCreateRequest'] = require('./models/public_alias_create_request');

MODELS['PublicAliasList'] = require('./models/public_alias_list');

MODELS['PublicAliasUpdateRequest'] = require('./models/public_alias_update_request');

MODELS['QuestionContent'] = require('./models/question_content');

MODELS['QuestionContentCreateRequest'] = require('./models/question_content_create_request');

MODELS['QuestionContentList'] = require('./models/question_content_list');

MODELS['QuestionContentOrderRequest'] = require('./models/question_content_order_request');

MODELS['QuestionContentType'] = require('./models/question_content_type');

MODELS['QuestionContentTypeList'] = require('./models/question_content_type_list');

MODELS['QuestionContentUpdateRequest'] = require('./models/question_content_update_request');

MODELS['QuestionManifest'] = require('./models/question_manifest');

MODELS['QuestionManifestCreateRequest'] = require('./models/question_manifest_create_request');

MODELS['QuestionManifestDeleteResponse'] = require('./models/question_manifest_delete_response');

MODELS['QuestionManifestList'] = require('./models/question_manifest_list');

MODELS['QuestionManifestUpdateRequest'] = require('./models/question_manifest_update_request');

MODELS['QuestionSet'] = require('./models/question_set');

MODELS['QuestionSetCreateRequest'] = require('./models/question_set_create_request');

MODELS['QuestionSetInfo'] = require('./models/question_set_info');

MODELS['QuestionSetInfoList'] = require('./models/question_set_info_list');

MODELS['QuestionSetList'] = require('./models/question_set_list');

MODELS['QuestionSetUpdateRequest'] = require('./models/question_set_update_request');

MODELS['QuestionSubmission'] = require('./models/question_submission');

MODELS['QuestionSubmissionCreate'] = require('./models/question_submission_create');

MODELS['QuestionSubmissionCreateRequest'] = require('./models/question_submission_create_request');

MODELS['QuestionSubmissionQueryResponse'] = require('./models/question_submission_query_response');

MODELS['QuestionSubmissionsList'] = require('./models/question_submissions_list');

MODELS['QuestionnaireAnswer'] = require('./models/questionnaire_answer');

MODELS['QuestionnaireStatus'] = require('./models/questionnaire_status');

MODELS['QuestionnaireStatusControlRequest'] = require('./models/questionnaire_status_control_request');

MODELS['RuntimeInfo'] = require('./models/runtime_info');

MODELS['SessionAttendance'] = require('./models/session_attendance');

MODELS['SessionAttendanceResponse'] = require('./models/session_attendance_response');

MODELS['SessionModule'] = require('./models/session_module');

MODELS['SessionModuleConfig'] = require('./models/session_module_config');

MODELS['SessionModuleList'] = require('./models/session_module_list');

MODELS['SessionModuleResource'] = require('./models/session_module_resource');

MODELS['SessionModuleUpdate'] = require('./models/session_module_update');

MODELS['SessionModuleUpdateConfig'] = require('./models/session_module_update_config');

MODELS['SessionModuleUpdateItem'] = require('./models/session_module_update_item');

MODELS['SessionModuleUpdateRequest'] = require('./models/session_module_update_request');

MODELS['SessionRuntime'] = require('./models/session_runtime');

MODELS['Slide'] = require('./models/slide');

MODELS['SlideDeck'] = require('./models/slide_deck');

MODELS['SlideDeckCreateRequest'] = require('./models/slide_deck_create_request');

MODELS['SlideDeckInfo'] = require('./models/slide_deck_info');

MODELS['SlideDeckInfoList'] = require('./models/slide_deck_info_list');

MODELS['SlideDeckUpdateRequest'] = require('./models/slide_deck_update_request');

MODELS['SlideDeleteRequest'] = require('./models/slide_delete_request');

MODELS['SlideOrderRequest'] = require('./models/slide_order_request');

MODELS['SlideUpdateRequest'] = require('./models/slide_update_request');

MODELS['StreamMedium'] = require('./models/stream_medium');

MODELS['StreamMediumCreateRequest'] = require('./models/stream_medium_create_request');

MODELS['StreamMediumInfo'] = require('./models/stream_medium_info');

MODELS['StreamMediumInfoList'] = require('./models/stream_medium_info_list');

MODELS['StreamMediumUpdateRequest'] = require('./models/stream_medium_update_request');

MODELS['StreamSource'] = require('./models/stream_source');

MODELS['StreamSourceConfirmRequest'] = require('./models/stream_source_confirm_request');

MODELS['StreamSourceCreateRequest'] = require('./models/stream_source_create_request');

MODELS['StreamSourcePreUploadRequest'] = require('./models/stream_source_pre_upload_request');

MODELS['SwitchMappingRequest'] = require('./models/switch_mapping_request');

MODELS['TemplateManifest'] = require('./models/template_manifest');

MODELS['TemplateManifestList'] = require('./models/template_manifest_list');

MODELS['TrackAttendanceRequest'] = require('./models/track_attendance_request');

MODELS['TranslationFilterOptionsResponse'] = require('./models/translation_filter_options_response');

MODELS['UpdateDictionaryDefinitionRequest'] = require('./models/update_dictionary_definition_request');

MODELS['UserFeedback'] = require('./models/user_feedback');

MODELS['UserFeedbackList'] = require('./models/user_feedback_list');

MODELS['UserFeedbackPostRequest'] = require('./models/user_feedback_post_request');

MODELS['UserSet'] = require('./models/user_set');

MODELS['UserSetCreateRequest'] = require('./models/user_set_create_request');

MODELS['UserSetEnrollRequest'] = require('./models/user_set_enroll_request');

MODELS['UserSetExtended'] = require('./models/user_set_extended');

MODELS['UserSetList'] = require('./models/user_set_list');

MODELS['UserSetUpdateRequest'] = require('./models/user_set_update_request');

MODELS['WhitelistPattern'] = require('./models/whitelist_pattern');

MODELS['WhitelistPatternList'] = require('./models/whitelist_pattern_list');

MODELS['WhitelistRequest'] = require('./models/whitelist_request');


module.exports = MODELS;
