'use strict';

var BaseClient = require('../core/client');

// Requires API

var DefaultApi = require('./api/default_api');

var InternalApi = require('./api/internal_api');


// Exports
module.exports = {
  // Model Serializer
  Serializer: require('./serializer'),

  // APIs
  
  DefaultApi: function(host_url) {
    return new BaseClient(host_url, '/api/v1', DefaultApi);
  },
  
  InternalApi: function(host_url) {
    return new BaseClient(host_url, '/api/v1', InternalApi);
  }
  
};
