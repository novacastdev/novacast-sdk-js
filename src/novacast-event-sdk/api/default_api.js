'use strict';

var assign     = require('object-assign');
var Deferred   = require('deferred');

var ApiError   = require('../../core/api_error');
var Serializer = require('../serializer');

var OPERATIONS = {
  
  /*
   * 
   * add statistics for the given poll status\n
   * @param session_uid - the session that this poll is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {QuestionSubmissionsList}
   */
  addPollStat : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/{question_manifest_uid}/add_stat', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AddPollStatRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSubmissionsList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * add statistics for the given questionnaire status\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {QuestionSubmissionsList}
   */
  addQuestionnaireStat : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/{question_manifest_uid}/add_stat', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AddquestionnaireStatRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSubmissionsList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Add one or more whitelisted patterns\n
   * @param user_set_uid - user set uid
   * @param body - request body
   * @return {WhitelistPatternList}
   */
  addWhitelistPatterns : function(user_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}/whitelist', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'WhitelistRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'WhitelistPatternList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Pre upload asset bundle content\n
   * @param asset_bundle_uid - asset bundle uid
   * @param body - request body
   * @return {PresignedUpload}
   */
  bundleContentPreUpload : function(asset_bundle_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof asset_bundle_uid === 'undefined' || asset_bundle_uid === null) throw new Error('Required parameter "asset_bundle_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/assets/{asset_bundle_uid}/pre_upload', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'asset_bundle_uid': Serializer.serialize(asset_bundle_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'BundleContentPreUploadRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PresignedUpload',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the event stage\n
   * @param event_uid - event uid
   * @param body - event stage update request data
   * @return {Event}
   */
  changeEventStage : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/stage', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventStageRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Event',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Close all the closable panels for this session\n
   * @param session_uid - event session uid
   * @return {nil}
   */
  closeSessionPanels : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/close_panels', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: '',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Confirm a pre-upload asset bundle content\n
   * @param asset_bundle_uid - asset bundle uid
   * @param body - request body
   * @return {BundleContent}
   */
  confirmBundleContent : function(asset_bundle_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof asset_bundle_uid === 'undefined' || asset_bundle_uid === null) throw new Error('Required parameter "asset_bundle_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/assets/{asset_bundle_uid}/confirm', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'asset_bundle_uid': Serializer.serialize(asset_bundle_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'BundleContentConfirmRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'BundleContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Confirm the successful uploading of remote stream source\n
   * @param stream_source_uid - stream source uid
   * @param body - request body
   * @return {StreamSource}
   */
  confirmStreamSource : function(stream_source_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_source_uid === 'undefined' || stream_source_uid === null) throw new Error('Required parameter "stream_source_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/stream_sources/{stream_source_uid}/confirm', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_source_uid': Serializer.serialize(stream_source_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'StreamSourceConfirmRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamSource',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new access policy\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {AccessPolicyExtended}
   */
  createAccessPolicy : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/access_policies', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AccessPolicyCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'AccessPolicyExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new active path with mapping(s)\n
   * @param event_uid - uid of the event
   * @param body - request body
   * @return {ActivePath}
   */
  createActivePath : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PathMappingRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'ActivePath',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new asset bundle\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {AssetBundleInfo}
   */
  createAssetBundle : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/assets', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AssetBundleCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'AssetBundleInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new channel\n
   * @param channel - channel data
   * @return {Channel}
   */
  createChannel : function(channel) {
    
    

    // check if all required parameters are present
    
    if (typeof channel === 'undefined' || channel === null) throw new Error('Required parameter "channel" is missing');
    

    var op = this.buildOperation('/channels', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(channel, 'ChannelData');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'Channel',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new channel account\n
   * @param channel_uid - uid of the channel
   * @param body - request body
   * @return {Account}
   */
  createChannelAccount : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/accounts', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AccountRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'Account',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new data set for the event\n
   * @param event_uid - event uid
   * @param body - request body
   * @return {DataSet}
   */
  createDataSet : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/data_sets', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'DataSetCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'DataSet',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new event\n
   * @param channel_uid - channel uid
   * @param body - event creation request data
   * @return {EventExtended}
   */
  createEvent : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/events', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'EventExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new page for a event\n
   * @param event_uid - event uid
   * @param body - request body
   * @return {EventPage}
   */
  createEventPage : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/event_pages', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventPageCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'EventPage',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new session for the event\n
   * @param event_uid - event uid
   * @param body - create session request body
   * @return {EventSession}
   */
  createEventSession : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/sessions', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventSessionInfoRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'EventSession',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new forum post\n
   * @param session_uid - uid of the session
   * @param body - request body
   * @return {ForumPost}
   */
  createForumPost : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/mods/forum_post/sessions/{session_uid}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'ForumPostCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'ForumPost',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new event public alias\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {PublicAlias}
   */
  createPublicAlias : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/public_aliases', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PublicAliasCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new question content\n
   * @param question_set_uid - question set uid
   * @param body - request body
   * @return {QuestionContent}
   */
  createQuestionContent : function(question_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}/question_contents', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionContentCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'QuestionContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new question manifest\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {QuestionManifest}
   */
  createQuestionManifest : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/question_manifests', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionManifestCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'QuestionManifest',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new question set\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {QuestionSetInfo}
   */
  createQuestionSet : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/question_sets', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionSetCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'QuestionSetInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create submissions for a given question manifest\n
   * @param question_manifest_uid - question manifest uid
   * @param session_uid - the session uid
   * @param body - request body
   * @return {ManifestSubmissions}
   */
  createQuestionSubmission : function(question_manifest_uid, session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/question_submissions/{question_manifest_uid}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String'), 
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionSubmissionCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'ManifestSubmissions',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new slide deck\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {SlideDeckInfo}
   */
  createSlideDeck : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/slide_decks', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SlideDeckCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'SlideDeckInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new stream medium\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {StreamMediumInfo}
   */
  createStreamMedium : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/streams', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'StreamMediumCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'StreamMediumInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new source for a stream medium\n
   * @param stream_medium_uid - stream medium uid
   * @param body - request body
   * @return {StreamSource}
   */
  createStreamSource : function(stream_medium_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_medium_uid === 'undefined' || stream_medium_uid === null) throw new Error('Required parameter "stream_medium_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/streams/{stream_medium_uid}/sources', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_medium_uid': Serializer.serialize(stream_medium_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'StreamSourceCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'StreamSource',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a new user set\n
   * @param channel_uid - channel uid
   * @param body - request body
   * @return {UserSetExtended}
   */
  createUserSet : function(channel_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/user_sets', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UserSetCreateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete an access policy\n
   * @param access_policy_uid - access policy uid
   * @return {AccessPolicy}
   */
  deleteAccessPolicy : function(access_policy_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof access_policy_uid === 'undefined' || access_policy_uid === null) throw new Error('Required parameter "access_policy_uid" is missing');
    

    var op = this.buildOperation('/access_policies/{access_policy_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'access_policy_uid': Serializer.serialize(access_policy_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccessPolicy',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete an active path\n
   * @param event_uid - uid of the event
   * @param path_uid - uid of the active path
   * @return {ActivePath}
   */
  deleteActivePath : function(event_uid, path_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof path_uid === 'undefined' || path_uid === null) throw new Error('Required parameter "path_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths/{path_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'path_uid': Serializer.serialize(path_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePath',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete an asset bundle content\n
   * @param asset_bundle_uid - asset bundle uid
   * @param bundle_content_uid - uid for the bundle content to delete
   * @return {BundleContent}
   */
  deleteBundleContent : function(asset_bundle_uid, bundle_content_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof asset_bundle_uid === 'undefined' || asset_bundle_uid === null) throw new Error('Required parameter "asset_bundle_uid" is missing');
    
    if (typeof bundle_content_uid === 'undefined' || bundle_content_uid === null) throw new Error('Required parameter "bundle_content_uid" is missing');
    

    var op = this.buildOperation('/assets/{asset_bundle_uid}/delete_content/{bundle_content_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'asset_bundle_uid': Serializer.serialize(asset_bundle_uid, 'String'), 
      
      'bundle_content_uid': Serializer.serialize(bundle_content_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'BundleContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete the event page\n
   * @param event_page_uid - event page uid
   * @return {EventPage}
   */
  deleteEventPage : function(event_page_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventPage',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete the event page content for a specific locale\n
   * @param event_page_uid - event page uid
   * @param content_locale - locale of the page content
   * @return {PageContentDeleteResponse}
   */
  deleteEventPageContent : function(event_page_uid, content_locale) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    
    if (typeof content_locale === 'undefined' || content_locale === null) throw new Error('Required parameter "content_locale" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}/contents/{content_locale}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String'), 
      
      'content_locale': Serializer.serialize(content_locale, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PageContentDeleteResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * delete an event session\n
   * @param session_uid - event session uid
   * @return {EventSession}
   */
  deleteEventSession : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSession',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Remove an public alias mapping\n
   * @param public_alias_uid - uid of the public alias
   * @return {PublicAlias}
   */
  deletePublicAlias : function(public_alias_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof public_alias_uid === 'undefined' || public_alias_uid === null) throw new Error('Required parameter "public_alias_uid" is missing');
    

    var op = this.buildOperation('/public_aliases/{public_alias_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'public_alias_uid': Serializer.serialize(public_alias_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a question content\n
   * @param question_content_uid - question content uid
   * @return {QuestionContent}
   */
  deleteQuestionContent : function(question_content_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_content_uid === 'undefined' || question_content_uid === null) throw new Error('Required parameter "question_content_uid" is missing');
    

    var op = this.buildOperation('/question_contents/{question_content_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_content_uid': Serializer.serialize(question_content_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a question manifest\n
   * @param question_manifest_uid - question manifest uid
   * @return {QuestionManifestDeleteResponse}
   */
  deleteQuestionManifest : function(question_manifest_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    

    var op = this.buildOperation('/question_manifests/{question_manifest_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionManifestDeleteResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a question set\n
   * @param question_set_uid - question set uid
   * @return {QuestionSetInfo}
   */
  deleteQuestionSet : function(question_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSetInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a slide deck\n
   * @param slide_deck_uid - slide deck uid
   * @return {SlideDeckInfo}
   */
  deleteSlideDeck : function(slide_deck_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeckInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete slides from slide deck\n
   * @param slide_deck_uid - slide deck uid
   * @param body - request body
   * @return {SlideDeck}
   */
  deleteSlides : function(slide_deck_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}/delete_slides', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SlideDeleteRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeck',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a stream medium\n
   * @param stream_medium_uid - stream medium uid
   * @return {StreamMedium}
   */
  deleteStreamMedium : function(stream_medium_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_medium_uid === 'undefined' || stream_medium_uid === null) throw new Error('Required parameter "stream_medium_uid" is missing');
    

    var op = this.buildOperation('/streams/{stream_medium_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_medium_uid': Serializer.serialize(stream_medium_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamMedium',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Delete a stream source\n
   * @param stream_source_uid - stream source uid
   * @return {StreamSource}
   */
  deleteStreamSource : function(stream_source_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_source_uid === 'undefined' || stream_source_uid === null) throw new Error('Required parameter "stream_source_uid" is missing');
    

    var op = this.buildOperation('/stream_sources/{stream_source_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_source_uid': Serializer.serialize(stream_source_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamSource',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Removes a whitelisted pattern from the user set\n
   * @param pattern_uid - whitelisted pattern uid
   * @return {WhitelistPattern}
   */
  deleteWhitelistPattern : function(pattern_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof pattern_uid === 'undefined' || pattern_uid === null) throw new Error('Required parameter "pattern_uid" is missing');
    

    var op = this.buildOperation('/whitelist_patterns/{pattern_uid}', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'pattern_uid': Serializer.serialize(pattern_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'WhitelistPattern',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * dismiss a published forum post\n
   * @param forum_post_uid - uid of the forum post
   * @return {ForumPost}
   */
  dismissForumPost : function(forum_post_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof forum_post_uid === 'undefined' || forum_post_uid === null) throw new Error('Required parameter "forum_post_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/posts/{forum_post_uid}/publish', 'DELETE');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'forum_post_uid': Serializer.serialize(forum_post_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPost',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Enroll an account to a user set\n
   * @param user_set_uid - user set uid
   * @param body - request body
   * @return {Enrollment}
   */
  enrollToUserSet : function(user_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}/enroll', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UserSetEnrollRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Enrollment',
      
      201: 'Enrollment',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a preview token to access a event in preview mode\n
   * @param event_uid - uid of the event to generate the token for
   * @param ttl - the number of seconds this token is valid for
   * @param session_ttl - the number of seconds each preview mode session is valid for
   * @return {PreviewToken}
   */
  generatePreviewToken : function(event_uid, ttl, session_ttl) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof ttl === 'undefined' || ttl === null) throw new Error('Required parameter "ttl" is missing');
    
    if (typeof session_ttl === 'undefined' || session_ttl === null) throw new Error('Required parameter "session_ttl" is missing');
    

    var op = this.buildOperation('/preview', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    
    // query parameters
    op.query = {};
    
    op.query['event_uid'] = Serializer.serialize(event_uid, 'String');
    
    
    op.query['ttl'] = Serializer.serialize(ttl, 'Integer');
    
    
    op.query['session_ttl'] = Serializer.serialize(session_ttl, 'Integer');
    
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PreviewToken',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get details of an access policy\n
   * @param access_policy_uid - access policy uid
   * @return {AccessPolicyExtended}
   */
  getAccessPolicy : function(access_policy_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof access_policy_uid === 'undefined' || access_policy_uid === null) throw new Error('Required parameter "access_policy_uid" is missing');
    

    var op = this.buildOperation('/access_policies/{access_policy_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'access_policy_uid': Serializer.serialize(access_policy_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccessPolicyExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a list of all access policies for the channel\n
   * @param channel_uid - channel uid
   * @return {AccessPolicyList}
   */
  getAccessPolicyList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/access_policies', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccessPolicyList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the displayable informations for a list of account uids\n
   * @param session_uid - the session uid
   * @param body - request body
   * @return {GetAccountDisplayInfoResponse}
   */
  getAccountDisplayInfos : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/account_display_infos', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'GetAccountDisplayInfoRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'GetAccountDisplayInfoResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the config and mappings for a given active path\n
   * @param event_uid - uid of the event
   * @param path_uid - uid of the active path
   * @return {ActivePath}
   */
  getActivePath : function(event_uid, path_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof path_uid === 'undefined' || path_uid === null) throw new Error('Required parameter "path_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths/{path_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'path_uid': Serializer.serialize(path_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePath',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * get the currently active polls\n
   * @param session_uid - the session that we are querying for
   * @return {ActivePollList}
   */
  getActivePolls : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/get_active_polls', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePollList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * get the currently active Questionnaires\n
   * @param session_uid - the session that we are querying for
   * @return {ActiveQuestionnaireList}
   */
  getActiveQuestionnaires : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/get_active_questionnaires', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActiveQuestionnaireList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * 
   * @param session_uid - the session for which the commands are to be loaded for
   * @return {EventSessionCommandList}
   */
  getActiveSessionCommands : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/event_session_commands/active', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSessionCommandList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all the possible filter options\n
   * @return {TranslationFilterOptionsResponse}
   */
  getAllFilterOptions : function() {
    
    

    // check if all required parameters are present
    

    var op = this.buildOperation('/translation_service/all_filter_options', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'TranslationFilterOptionsResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the archive activities for a given session\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @return {ArchiveActivityList}
   */
  getArchiveActivities : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/archive_session_activities', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ArchiveActivityList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get asset bundle with contents\n
   * @param asset_bundle_uid - asset bundle uid
   * @return {AssetBundle}
   */
  getAssetBundle : function(asset_bundle_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof asset_bundle_uid === 'undefined' || asset_bundle_uid === null) throw new Error('Required parameter "asset_bundle_uid" is missing');
    

    var op = this.buildOperation('/assets/{asset_bundle_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'asset_bundle_uid': Serializer.serialize(asset_bundle_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AssetBundle',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get information for all asset bundles in a channel\n
   * @param channel_uid - channel uid
   * @return {AssetBundleInfoList}
   */
  getAssetBundleList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/assets', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AssetBundleInfoList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get an asset bundle content\n
   * @param asset_bundle_uid - asset bundle uid
   * @param file_path - content path within the bundle
   * @return {BundleContent}
   */
  getBundleContent : function(asset_bundle_uid, file_path) {
    
    

    // check if all required parameters are present
    
    if (typeof asset_bundle_uid === 'undefined' || asset_bundle_uid === null) throw new Error('Required parameter "asset_bundle_uid" is missing');
    
    if (typeof file_path === 'undefined' || file_path === null) throw new Error('Required parameter "file_path" is missing');
    

    var op = this.buildOperation('/assets/{asset_bundle_uid}/contents/{file_path}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'asset_bundle_uid': Serializer.serialize(asset_bundle_uid, 'String'), 
      
      'file_path': Serializer.serialize(file_path, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'BundleContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a channel\n
   * @param channel_uid - uid of the channel
   * @return {Channel}
   */
  getChannel : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Channel',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all accounts belonging to the channel\n
   * @param channel_uid - uid of the channel
   * @return {AccountExtendedList}
   */
  getChannelAccounts : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/accounts', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccountExtendedList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all events belongs to this channel\n
   * @param channel_uid - channel uid
   * @return {EventList}
   */
  getChannelEvents : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/events', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a list of all channel for the current user\n
   * @return {ChannelList}
   */
  getChannelList : function() {
    
    

    // check if all required parameters are present
    

    var op = this.buildOperation('/channels', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ChannelList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all the page contents of a event page\n
   * @param event_page_uid - event page uid
   * @return {PageContentList}
   */
  getContentsForEventPage : function(event_page_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}/contents', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PageContentList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all question contents for the question set\n
   * @param question_set_uid - question set uid
   * @return {QuestionContentList}
   */
  getContentsForQuestionSet : function(question_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}/question_contents', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionContentList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a data set\n
   * @param data_set_uid - data set uid
   * @return {DataSet}
   */
  getDataSet : function(data_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof data_set_uid === 'undefined' || data_set_uid === null) throw new Error('Required parameter "data_set_uid" is missing');
    

    var op = this.buildOperation('/data_sets/{data_set_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'data_set_uid': Serializer.serialize(data_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DataSet',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all data sets for the event\n
   * @param event_uid - event uid
   * @return {DataSetList}
   */
  getDataSetList : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/data_sets', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DataSetList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all partitions for a data set\n
   * @param data_set_uid - data set uid
   * @return {DataSetPartitionList}
   */
  getDataSetPartitions : function(data_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof data_set_uid === 'undefined' || data_set_uid === null) throw new Error('Required parameter "data_set_uid" is missing');
    

    var op = this.buildOperation('/data_sets/{data_set_uid}/partitions', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'data_set_uid': Serializer.serialize(data_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DataSetPartitionList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get dictionary for a given locale\n
   * @param locale - the locale that we are looking for
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.category - the category that we are interested in
   * @return {DictionaryQueryResponse}
   */
  getDictionaryForLocale : function(locale, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof locale === 'undefined' || locale === null) throw new Error('Required parameter "locale" is missing');
    

    var op = this.buildOperation('/translation_service/get_dictionary', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    
    // query parameters
    op.query = {};
    
    op.query['locale'] = Serializer.serialize(locale, 'String');
    
    
    
    if(opts['category']) op.query['category'] = Serializer.serialize(opts['category'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DictionaryQueryResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a list of users belongs to this user set\n
   * @param user_set_uid - user set uid
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {Float} optionals.max_records - optional max records for return
   * @param {DateTime} optionals.from_date - optional from date filter in ISO8601 format
   * @param {DateTime} optionals.to_date - optional to date filter in ISO8601 format
   * @return {EnrollmentListResponse}
   */
  getEnrollments : function(user_set_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}/enrollments', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['max_records']) op.query['max_records'] = Serializer.serialize(opts['max_records'], 'Float');
    
    
    if(opts['from_date']) op.query['from_date'] = Serializer.serialize(opts['from_date'], 'DateTime');
    
    
    if(opts['to_date']) op.query['to_date'] = Serializer.serialize(opts['to_date'], 'DateTime');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EnrollmentListResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get Event with extended details\n
   * @param event_uid - event uid
   * @return {EventExtended}
   */
  getEvent : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all active paths and their mappings of the event\n
   * @param event_uid - uid of the event
   * @return {ActivePathList}
   */
  getEventActivePaths : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePathList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Lookup event level attendances
   * @param event_uid - event uid
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {Float} optionals.max_records - optional max records for return
   * @param {DateTime} optionals.from_date - optional from date filter in ISO8601 format
   * @param {DateTime} optionals.to_date - optional to date filter in ISO8601 format
   * @return {AttendanceResponse}
   */
  getEventAttendances : function(event_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/attendances/events/{event_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['max_records']) op.query['max_records'] = Serializer.serialize(opts['max_records'], 'Float');
    
    
    if(opts['from_date']) op.query['from_date'] = Serializer.serialize(opts['from_date'], 'DateTime');
    
    
    if(opts['to_date']) op.query['to_date'] = Serializer.serialize(opts['to_date'], 'DateTime');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AttendanceResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the event page\n
   * @param event_page_uid - event page uid
   * @return {EventPage}
   */
  getEventPage : function(event_page_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventPage',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get event page content for a specific locale\n
   * @param event_page_uid - event page uid
   * @param content_locale - locale of the page content
   * @return {PageContent}
   */
  getEventPageContent : function(event_page_uid, content_locale) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    
    if (typeof content_locale === 'undefined' || content_locale === null) throw new Error('Required parameter "content_locale" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}/contents/{content_locale}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String'), 
      
      'content_locale': Serializer.serialize(content_locale, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PageContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get an event session with extended details\n
   * @param session_uid - event session uid
   * @return {EventSessionExtended}
   */
  getEventSession : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSessionExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all sessions for the event\n
   * @param event_uid - event uid
   * @return {EventSessionList}
   */
  getEventSessionList : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/sessions', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSessionList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the page runtime configuration of event session\n
   * @param session_uid - event session uid
   * @return {SessionRuntime}
   */
  getEventSessionRuntime : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/runtime', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SessionRuntime',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the user set for the event\n
   * @param event_uid - event uid
   * @return {UserSetExtended}
   */
  getEventUserSet : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/user_set', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all forum posts (with extended information) of the session\n
   * @param session_uid - uid of the session
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.data_set_uid - uid of the data set. Default to the active data set of the event
   * @return {ForumPostExtendedList}
   */
  getExtendedForumPosts : function(session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/sessions/{session_uid}/extended', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['data_set_uid']) op.query['data_set_uid'] = Serializer.serialize(opts['data_set_uid'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostExtendedList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get dictionary for a given set of filter options\n
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.locale - the locale that we are looking for
   * @param {String} optionals.category - the category that we are interested in
   * @return {DictionaryFilterResponse}
   */
  getFilteredDictionary : function(optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    

    var op = this.buildOperation('/translation_service/filter_dictionary', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['locale']) op.query['locale'] = Serializer.serialize(opts['locale'], 'String');
    
    
    if(opts['category']) op.query['category'] = Serializer.serialize(opts['category'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DictionaryFilterResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all likes for the forum post\n
   * @param forum_post_uid - uid of the forum post
   * @return {ForumPostLikeList}
   */
  getForumPostLikes : function(forum_post_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof forum_post_uid === 'undefined' || forum_post_uid === null) throw new Error('Required parameter "forum_post_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/posts/{forum_post_uid}/likes', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'forum_post_uid': Serializer.serialize(forum_post_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostLikeList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all forum posts likes of the session submitted by the access user\n
   * @param session_uid - uid of the session
   * @return {ForumPostLikeList}
   */
  getForumPostLikesForAccount : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/sessions/{session_uid}/account/likes', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostLikeList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all forum posts of the session\n
   * @param session_uid - uid of the session
   * @return {ForumPostList}
   */
  getForumPosts : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/sessions/{session_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all forum posts (with extended information and subbmitter account identifier) of the session\n
   * @param session_uid - uid of the session
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.data_set_uid - uid of the data set. Default to the active data set of the event
   * @return {ForumPostExtendedList}
   */
  getForumPostsWithAccounts : function(session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/sessions/{session_uid}/with_accounts', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['data_set_uid']) op.query['data_set_uid'] = Serializer.serialize(opts['data_set_uid'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostExtendedList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the current live cast page\n
   * @param session_uid - session uid
   * @return {LiveCastSlidePage}
   */
  getLiveCastPage : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/live_cast/sessions/{session_uid}/page', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'LiveCastSlidePage',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * (LiveCast) Get the current LiveCast module state\n
   * @param session_uid - session uid
   * @return {LiveCastState}
   */
  getLiveCastState : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/live_cast/sessions/{session_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'LiveCastState',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all the event pages of the event\n
   * @param event_uid - event uid
   * @return {EventPageList}
   */
  getPagesForEvent : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/event_pages', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventPageList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the poll status\n
   * @param session_uid - the session that this poll is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param question_content_uid - the question content that is specific for this poll
   * @return {PollStatus}
   */
  getPollStatus : function(session_uid, question_manifest_uid, question_content_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof question_content_uid === 'undefined' || question_content_uid === null) throw new Error('Required parameter "question_content_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/{question_manifest_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    op.query['question_content_uid'] = Serializer.serialize(question_content_uid, 'String');
    
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PollStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * get all previous submissions from this user for this given session\n
   * @param session_uid - the session that this poll is relevant to
   * @return {QuestionSubmissionsList}
   */
  getPriorPollSubmissions : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/prior_submissions', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSubmissionsList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * get all previous submissions from this user for this given session\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @return {QuestionSubmissionsList}
   */
  getPriorQuestionnaireSubmissions : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/prior_submissions', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSubmissionsList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get an public alias mapping\n
   * @param public_alias_uid - uid of the public alias
   * @return {PublicAlias}
   */
  getPublicAlias : function(public_alias_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof public_alias_uid === 'undefined' || public_alias_uid === null) throw new Error('Required parameter "public_alias_uid" is missing');
    

    var op = this.buildOperation('/public_aliases/{public_alias_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'public_alias_uid': Serializer.serialize(public_alias_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get public alias by path\n
   * @param alias_path - path of the public alias
   * @return {PublicAlias}
   */
  getPublicAliasByPath : function(alias_path) {
    
    

    // check if all required parameters are present
    
    if (typeof alias_path === 'undefined' || alias_path === null) throw new Error('Required parameter "alias_path" is missing');
    

    var op = this.buildOperation('/public_aliases/path/{alias_path}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'alias_path': Serializer.serialize(alias_path, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all the public aliases of the channel\n
   * @param channel_uid - channel uid
   * @return {PublicAliasList}
   */
  getPublicAliasesForChannel : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/public_aliases', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAliasList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a question content\n
   * @param question_content_uid - question content uid
   * @return {QuestionContent}
   */
  getQuestionContent : function(question_content_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_content_uid === 'undefined' || question_content_uid === null) throw new Error('Required parameter "question_content_uid" is missing');
    

    var op = this.buildOperation('/question_contents/{question_content_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_content_uid': Serializer.serialize(question_content_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the list of all question content types\n
   * @return {QuestionContentTypeList}
   */
  getQuestionContentTypes : function() {
    
    

    // check if all required parameters are present
    

    var op = this.buildOperation('/question_content_types', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionContentTypeList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a question manifest\n
   * @param question_manifest_uid - question manifest uid
   * @return {QuestionManifest}
   */
  getQuestionManifest : function(question_manifest_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    

    var op = this.buildOperation('/question_manifests/{question_manifest_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionManifest',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the list of question manifests\n
   * @param channel_uid - channel uid
   * @return {QuestionManifestList}
   */
  getQuestionManifests : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/question_manifests', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionManifestList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a question set\n
   * @param question_set_uid - question set uid
   * @return {QuestionSet}
   */
  getQuestionSet : function(question_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSet',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the list of question sets\n
   * @param channel_uid - channel uid
   * @return {QuestionSetList}
   */
  getQuestionSetList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/question_sets', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSetList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all submissions for a question manifest\n
   * @param question_manifest_uid - question manifest uid
   * @param session_uid - the session uid
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {Float} optionals.max_records - optional max records for return
   * @param {DateTime} optionals.from_date - optional from date filter
   * @param {DateTime} optionals.to_date - optional to date filter
   * @return {QuestionSubmissionQueryResponse}
   */
  getQuestionSubmissions : function(question_manifest_uid, session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/question_submissions/{question_manifest_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String'), 
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['max_records']) op.query['max_records'] = Serializer.serialize(opts['max_records'], 'Float');
    
    
    if(opts['from_date']) op.query['from_date'] = Serializer.serialize(opts['from_date'], 'DateTime');
    
    
    if(opts['to_date']) op.query['to_date'] = Serializer.serialize(opts['to_date'], 'DateTime');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSubmissionQueryResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the questionnaire status\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @return {QuestionnaireStatus}
   */
  getQuestionnaireStatus : function(session_uid, question_manifest_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/{question_manifest_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionnaireStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Lookup session attendances by session uid, which also include corresponding event level attendances
   * @param session_uid - the session uid
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {Float} optionals.max_records - optional max records for return
   * @param {DateTime} optionals.from_date - optional from date filter in ISO8601 format
   * @param {DateTime} optionals.to_date - optional to date filter in ISO8601 format
   * @return {SessionAttendanceResponse}
   */
  getSessionAttendances : function(session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/attendances/sessions/{session_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['max_records']) op.query['max_records'] = Serializer.serialize(opts['max_records'], 'Float');
    
    
    if(opts['from_date']) op.query['from_date'] = Serializer.serialize(opts['from_date'], 'DateTime');
    
    
    if(opts['to_date']) op.query['to_date'] = Serializer.serialize(opts['to_date'], 'DateTime');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SessionAttendanceResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * get all session commands for a session
   * @param session_uid - the session for which the commands are to be loaded for
   * @return {EventSessionCommandList}
   */
  getSessionCommands : function(session_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/event_session_commands', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSessionCommandList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a list of users belonging to this session&#39;s user set\n
   * @param session_uid - the session uid
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {Float} optionals.max_records - optional max records for return
   * @param {DateTime} optionals.from_date - optional from date filter in ISO8601 format
   * @param {DateTime} optionals.to_date - optional to date filter in ISO8601 format
   * @return {EnrollmentListResponse}
   */
  getSessionEnrollments : function(session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/enrollments', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['max_records']) op.query['max_records'] = Serializer.serialize(opts['max_records'], 'Float');
    
    
    if(opts['from_date']) op.query['from_date'] = Serializer.serialize(opts['from_date'], 'DateTime');
    
    
    if(opts['to_date']) op.query['to_date'] = Serializer.serialize(opts['to_date'], 'DateTime');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EnrollmentListResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a slide deck\n
   * @param slide_deck_uid - slide deck uid
   * @return {SlideDeck}
   */
  getSlideDeck : function(slide_deck_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeck',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all slide deck for the channel\n
   * @param channel_uid - channel uid
   * @return {SlideDeckInfoList}
   */
  getSlideDeckList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/slide_decks', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeckInfoList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get stream medium object\n
   * @param stream_medium_uid - stream medium uid
   * @return {StreamMedium}
   */
  getStreamMedium : function(stream_medium_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_medium_uid === 'undefined' || stream_medium_uid === null) throw new Error('Required parameter "stream_medium_uid" is missing');
    

    var op = this.buildOperation('/streams/{stream_medium_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_medium_uid': Serializer.serialize(stream_medium_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamMedium',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all the streams for this channel\n
   * @param channel_uid - channel uid
   * @return {StreamMediumInfoList}
   */
  getStreamMediumList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/streams', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamMediumInfoList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get template manifests for a specific type\n
   * @param manifest_type - the type of template
   * @return {TemplateManifestList}
   */
  getTemplateManifestByType : function(manifest_type) {
    
    

    // check if all required parameters are present
    
    if (typeof manifest_type === 'undefined' || manifest_type === null) throw new Error('Required parameter "manifest_type" is missing');
    

    var op = this.buildOperation('/template_manifests/find_by_type', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    
    // query parameters
    op.query = {};
    
    op.query['manifest_type'] = Serializer.serialize(manifest_type, 'String');
    
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'TemplateManifestList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get user feedbacks submitted for a session\n
   * @param session_uid - uid of the session
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.data_set_uid - uid of the data set. Default to the active data set of the event
   * @return {UserFeedbackList}
   */
  getUserFeedbacks : function(session_uid, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    

    var op = this.buildOperation('/mods/user_feedback/{session_uid}/feedbacks', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['data_set_uid']) op.query['data_set_uid'] = Serializer.serialize(opts['data_set_uid'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserFeedbackList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a user set with extended details\n
   * @param user_set_uid - user set uid
   * @return {UserSetExtended}
   */
  getUserSet : function(user_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all user sets for channel\n
   * @param channel_uid - channel uid
   * @return {UserSetList}
   */
  getUserSetList : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/user_sets', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get all whitelisted patterns in the user set\n
   * @param user_set_uid - user set uid
   * @return {WhitelistPatternList}
   */
  getWhitelistPatterns : function(user_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}/whitelist', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'WhitelistPatternList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Like or unlike a forum post\n
   * @param forum_post_uid - uid of the forum post
   * @param body - request body
   * @return {ForumPostLike}
   */
  likeForumPost : function(forum_post_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof forum_post_uid === 'undefined' || forum_post_uid === null) throw new Error('Required parameter "forum_post_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/mods/forum_post/posts/{forum_post_uid}/likes', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'forum_post_uid': Serializer.serialize(forum_post_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'ForumPostLikeRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true },
    
      { name: 'previewToken', key: 'preview_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostLike',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Post a new user feedback\n
   * @param session_uid - uid of the session
   * @param body - request body
   * @return {UserFeedback}
   */
  postUserFeedback : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/mods/user_feedback/{session_uid}/feedbacks', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UserFeedbackPostRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      201: 'UserFeedback',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * publish a particular forum post\n
   * @param forum_post_uid - uid of the forum post
   * @return {ForumPost}
   */
  publishForumPost : function(forum_post_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof forum_post_uid === 'undefined' || forum_post_uid === null) throw new Error('Required parameter "forum_post_uid" is missing');
    

    var op = this.buildOperation('/mods/forum_post/posts/{forum_post_uid}/publish', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'forum_post_uid': Serializer.serialize(forum_post_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPost',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * publish statistics for a given poll\n
   * @param session_uid - the session that this poll is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {PollStatus}
   */
  publishPoll : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/{question_manifest_uid}/publish', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PollStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PollStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * publish statistics for a given questionnaire\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {QuestionnaireStatus}
   */
  publishQuestionnaire : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/{question_manifest_uid}/publish', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionnaireStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionnaireStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Rearrange the order of slide\n
   * @param slide_deck_uid - slide deck uid
   * @param body - request body
   * @return {SlideDeck}
   */
  rearrangeSlides : function(slide_deck_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}/rearrange', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SlideOrderRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeck',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Reorder question set contents\n
   * @param question_set_uid - question set uid
   * @param body - request body
   * @return {QuestionSet}
   */
  reorderQuestionContents : function(question_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}/reorder', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionContentOrderRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSet',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Set the active data set for an event\n
   * @param event_uid - event uid
   * @param body - request body
   * @return {DataSet}
   */
  setActiveDataSet : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/data_sets/active', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'ActiveDataSetUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DataSet',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * start the given poll\n
   * @param session_uid - the session that this poll is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {PollStatus}
   */
  startPoll : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/{question_manifest_uid}/start', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PollStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PollStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * start the given questionnaire\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {QuestionnaireStatus}
   */
  startQuestionnaire : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/{question_manifest_uid}/start', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionnaireStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionnaireStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * stop the given poll\n
   * @param session_uid - the session that this poll is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {PollStatus}
   */
  stopPoll : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/polling/{question_manifest_uid}/stop', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PollStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PollStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * stop the given questionnaire\n
   * @param session_uid - the session that this questionnaire is relevant to
   * @param question_manifest_uid - quesiton manifest associated uid
   * @param body - request body
   * @return {QuestionnaireStatus}
   */
  stopQuestionnaire : function(session_uid, question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/questionnaire/{question_manifest_uid}/stop', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionnaireStatusControlRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionnaireStatus',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Create a stream source for remote resource\n
   * @param stream_medium_uid - stream medium uid
   * @param body - request body
   * @return {PresignedUpload}
   */
  streamSourcePreUpload : function(stream_medium_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_medium_uid === 'undefined' || stream_medium_uid === null) throw new Error('Required parameter "stream_medium_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/streams/{stream_medium_uid}/sources/pre_upload', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_medium_uid': Serializer.serialize(stream_medium_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'StreamSourcePreUploadRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PresignedUpload',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * a runtime operation that switches active mapping for the given path\n
   * @param event_uid - uid of the event
   * @param path_uid - uid of the active path
   * @param body - request body
   * @return {ActivePath}
   */
  switchActiveMapping : function(event_uid, path_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof path_uid === 'undefined' || path_uid === null) throw new Error('Required parameter "path_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths/{path_uid}/switch_active_mapping', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'path_uid': Serializer.serialize(path_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SwitchMappingRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePath',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update an access policy\n
   * @param access_policy_uid - access policy uid
   * @param body - request body
   * @return {AccessPolicyExtended}
   */
  updateAccessPolicy : function(access_policy_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof access_policy_uid === 'undefined' || access_policy_uid === null) throw new Error('Required parameter "access_policy_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/access_policies/{access_policy_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'access_policy_uid': Serializer.serialize(access_policy_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AccessPolicyUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccessPolicyExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the roles assigned to an account\n
   * @param account_uid - uid of the account
   * @param body - request body
   * @return {AccountResourceRolesList}
   */
  updateAccountRoles : function(account_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/accounts/{account_uid}/roles', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'account_uid': Serializer.serialize(account_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'AccountRoleUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccountResourceRolesList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a active path and its mappings\n
   * @param event_uid - uid of the event
   * @param path_uid - uid of the active path
   * @param body - request body
   * @return {ActivePath}
   */
  updateActivePath : function(event_uid, path_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof path_uid === 'undefined' || path_uid === null) throw new Error('Required parameter "path_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/active_paths/{path_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'path_uid': Serializer.serialize(path_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PathMappingRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ActivePath',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a channel\n
   * @param channel_uid - uid of the channel
   * @param channel - channel data
   * @return {Channel}
   */
  updateChannel : function(channel_uid, channel) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof channel === 'undefined' || channel === null) throw new Error('Required parameter "channel" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(channel, 'ChannelData');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Channel',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a given dictionary definition\n
   * @param body - request body
   * @return {DictionaryEntry}
   */
  updateDictionaryDefinition : function(body) {
    
    

    // check if all required parameters are present
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/translation_service/update_definition', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UpdateDictionaryDefinitionRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'DictionaryEntry',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update an event\n
   * @param event_uid - event uid
   * @param body - event update request data
   * @return {EventExtended}
   */
  updateEvent : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/events/{event_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the event page\n
   * @param event_page_uid - event page uid
   * @param body - request body
   * @return {EventPage}
   */
  updateEventPage : function(event_page_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventPageUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventPage',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the page content of the event page\n
   * @param event_page_uid - event page uid
   * @param body - request body
   * @return {PageContent}
   */
  updateEventPageContent : function(event_page_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_page_uid === 'undefined' || event_page_uid === null) throw new Error('Required parameter "event_page_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/event_pages/{event_page_uid}/contents', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_page_uid': Serializer.serialize(event_page_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PageContentUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PageContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the info for an event session \n
   * @param session_uid - event session uid
   * @param body - update session info
   * @return {EventSession}
   */
  updateEventSession : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'EventSessionInfoRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventSession',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the state of a forum post\n
   * @param forum_post_uid - uid of the forum post
   * @param body - request body
   * @return {ForumPostExtended}
   */
  updateForumPost : function(forum_post_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof forum_post_uid === 'undefined' || forum_post_uid === null) throw new Error('Required parameter "forum_post_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/mods/forum_post/posts/{forum_post_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'forum_post_uid': Serializer.serialize(forum_post_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'ForumPostUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ForumPostExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * (LiveCast) Change slide page\nThis will also publish a LiveCastSlidePage command to the session pub sub channel\n
   * @param session_uid - session uid
   * @param body - request body
   * @return {LiveCastPageChangeResponse}
   */
  updateLiveCastPage : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/mods/live_cast/sessions/{session_uid}/page', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'LiveCastPageChangeRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'LiveCastPageChangeResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update an public alias\n
   * @param public_alias_uid - uid of the public alias
   * @param body - request body
   * @return {PublicAlias}
   */
  updatePublicAlias : function(public_alias_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof public_alias_uid === 'undefined' || public_alias_uid === null) throw new Error('Required parameter "public_alias_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/public_aliases/{public_alias_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'public_alias_uid': Serializer.serialize(public_alias_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'PublicAliasUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a question content\n
   * @param question_content_uid - question content uid
   * @param body - request body
   * @return {QuestionContent}
   */
  updateQuestionContent : function(question_content_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_content_uid === 'undefined' || question_content_uid === null) throw new Error('Required parameter "question_content_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/question_contents/{question_content_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_content_uid': Serializer.serialize(question_content_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionContentUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a question manifest\n
   * @param question_manifest_uid - question manifest uid
   * @param body - request body
   * @return {QuestionManifest}
   */
  updateQuestionManifest : function(question_manifest_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_manifest_uid === 'undefined' || question_manifest_uid === null) throw new Error('Required parameter "question_manifest_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/question_manifests/{question_manifest_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_manifest_uid': Serializer.serialize(question_manifest_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionManifestUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionManifest',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a question set\n
   * @param question_set_uid - question set uid
   * @param body - request body
   * @return {QuestionSetInfo}
   */
  updateQuestionSet : function(question_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof question_set_uid === 'undefined' || question_set_uid === null) throw new Error('Required parameter "question_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/question_sets/{question_set_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'question_set_uid': Serializer.serialize(question_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'QuestionSetUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'QuestionSetInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the module configurations for a event session\n
   * @param session_uid - event session uid
   * @param body - request body
   * @return {SessionModuleList}
   */
  updateSessionModules : function(session_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/modules', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SessionModuleUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SessionModuleList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update slide properties\n
   * @param slide_uid - slide uid
   * @param body - request body
   * @return {Slide}
   */
  updateSlide : function(slide_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_uid === 'undefined' || slide_uid === null) throw new Error('Required parameter "slide_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/slides/{slide_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_uid': Serializer.serialize(slide_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SlideUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Slide',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update the slide deck\n
   * @param slide_deck_uid - slide deck uid
   * @param body - request body
   * @return {SlideDeckInfo}
   */
  updateSlideDeck : function(slide_deck_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'SlideDeckUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeckInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update stream medium\n
   * @param stream_medium_uid - stream medium uid
   * @param body - request body
   * @return {StreamMedium}
   */
  updateStreamMedium : function(stream_medium_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof stream_medium_uid === 'undefined' || stream_medium_uid === null) throw new Error('Required parameter "stream_medium_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/streams/{stream_medium_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'stream_medium_uid': Serializer.serialize(stream_medium_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'StreamMediumUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'StreamMedium',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Update a user set\n
   * @param user_set_uid - user set uid
   * @param body - request body
   * @return {UserSetExtended}
   */
  updateUserSet : function(user_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}', 'PUT');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UserSetUpdateRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Upload an account feed for creating channel accounts\n
   * @param channel_uid - uid of the channel
   * @param file - account feed file
   * @return {AccountCreationResponseList}
   */
  uploadAccountFeed : function(channel_uid, file) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    
    if (typeof file === 'undefined' || file === null) throw new Error('Required parameter "file" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}/accounts/feed', 'POST');

    // api mime type
    op.consumes = ['multipart/form-data'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    
    // form parameters
    op.form = {};
    
    op.form['file'] = Serializer.serialize(file, 'Byte');
    
    
    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'AccountCreationResponseList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Upload a slide\n
   * @param slide_deck_uid - slide deck uid
   * @param files - image files
   * @return {SlideDeck}
   */
  uploadSlides : function(slide_deck_uid, files) {
    
    

    // check if all required parameters are present
    
    if (typeof slide_deck_uid === 'undefined' || slide_deck_uid === null) throw new Error('Required parameter "slide_deck_uid" is missing');
    
    if (typeof files === 'undefined' || files === null) throw new Error('Required parameter "files" is missing');
    

    var op = this.buildOperation('/slide_decks/{slide_deck_uid}/upload', 'POST');

    // api mime type
    op.consumes = ['multipart/form-data'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'slide_deck_uid': Serializer.serialize(slide_deck_uid, 'String')
      
    };
    

    

    

    
    // form parameters
    op.form = {};
    
    op.form['files'] = Serializer.serialize(files, 'Array[Byte]');
    
    
    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'SlideDeck',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  }
  
};

module.exports = OPERATIONS;

