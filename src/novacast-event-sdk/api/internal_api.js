'use strict';

var assign     = require('object-assign');
var Deferred   = require('deferred');

var ApiError   = require('../../core/api_error');
var Serializer = require('../serializer');

var OPERATIONS = {
  
  /*
   * 
   * Get a list of channels by uids\n
   * @param body - request body
   * @return {ChannelList}
   */
  batchGetChannel : function(body) {
    
    

    // check if all required parameters are present
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/channels/batch_get', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'ChannelBatchRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'ChannelList',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * a batch call to track multiple attendances by their access tokens\n
   * @param body - request body
   * @return {BatchTrackAttendanceResponse}
   */
  batchTrackAttendance : function(body) {
    
    

    // check if all required parameters are present
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/attendances/batch', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'BatchTrackAttendanceRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'BatchTrackAttendanceResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Check if the user has a particular permission\n
   * @param account_uid - account uid
   * @param permission - permission
   * @param resource_rn - resource name
   * @return {nil}
   */
  checkAccountPermission : function(account_uid, permission, resource_rn) {
    
    

    // check if all required parameters are present
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    
    if (typeof permission === 'undefined' || permission === null) throw new Error('Required parameter "permission" is missing');
    
    if (typeof resource_rn === 'undefined' || resource_rn === null) throw new Error('Required parameter "resource_rn" is missing');
    

    var op = this.buildOperation('/access/accounts/{account_uid}/permission', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'account_uid': Serializer.serialize(account_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    op.query['permission'] = Serializer.serialize(permission, 'String');
    
    
    op.query['resource_rn'] = Serializer.serialize(resource_rn, 'String');
    
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: '',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Enroll an account to a user set\n
   * @param user_set_uid - user set uid
   * @param body - request body
   * @return {Enrollment}
   */
  enrollToUserSet : function(user_set_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}/enroll', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'UserSetEnrollRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Enrollment',
      
      201: 'Enrollment',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Check if a user have access to the event\n
   * @param event_uid - event uid
   * @param account_uid - account uid
   * @param body - request body
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.preview_token - preview token
   * @return {FilterAccessResponse}
   */
  filterEventAccess : function(event_uid, account_uid, body, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/access/events/{event_uid}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    op.query['account_uid'] = Serializer.serialize(account_uid, 'String');
    
    
    
    if(opts['preview_token']) op.query['preview_token'] = Serializer.serialize(opts['preview_token'], 'String');
    
    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'FilterAccessRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'FilterAccessResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Check if a user have access to the event\n
   * @param event_uid - event uid
   * @param content_path - path to access the content
   * @param account_uid - account uid
   * @param body - request body
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.preview_token - preview token
   * @return {FilterAccessResponse}
   */
  filterEventContentAccess : function(event_uid, content_path, account_uid, body, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof content_path === 'undefined' || content_path === null) throw new Error('Required parameter "content_path" is missing');
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/access/events/{event_uid}/content{/content_path*}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'content_path': Serializer.serialize(content_path, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    op.query['account_uid'] = Serializer.serialize(account_uid, 'String');
    
    
    
    if(opts['preview_token']) op.query['preview_token'] = Serializer.serialize(opts['preview_token'], 'String');
    
    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'FilterAccessRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'FilterAccessResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Check if a user have access to the session\n
   * @param session_uid - session uid
   * @param account_uid - account uid
   * @param body - request body
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.preview_token - preview token
   * @return {FilterAccessResponse}
   */
  filterSessionAccess : function(session_uid, account_uid, body, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/access/sessions/{session_uid}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    op.query['account_uid'] = Serializer.serialize(account_uid, 'String');
    
    
    
    if(opts['preview_token']) op.query['preview_token'] = Serializer.serialize(opts['preview_token'], 'String');
    
    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'FilterAccessRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'FilterAccessResponse',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a channel\n
   * @param channel_uid - uid of the channel
   * @return {Channel}
   */
  getChannel : function(channel_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof channel_uid === 'undefined' || channel_uid === null) throw new Error('Required parameter "channel_uid" is missing');
    

    var op = this.buildOperation('/channels/{channel_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'channel_uid': Serializer.serialize(channel_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'Channel',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get event content by path\n
   * @param event_uid - event uid
   * @param content_path - path to access the content
   * @param {Object.<string, *>} optionals - optional parameters
   * @param {String} optionals.content_locale - an optional parameter to specify the locale for the content
   * @return {EventContent}
   */
  getContentByPath : function(event_uid, content_path, optionals) {
    
    var opts = assign({}, optionals);

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof content_path === 'undefined' || content_path === null) throw new Error('Required parameter "content_path" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/contents{/content_path*}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'content_path': Serializer.serialize(content_path, 'String')
      
    };
    

    

    
    // query parameters
    op.query = {};
    
    
    if(opts['content_locale']) op.query['content_locale'] = Serializer.serialize(opts['content_locale'], 'String');
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventContent',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get Event with extended details\n
   * @param event_uid - event uid
   * @return {EventExtended}
   */
  getEvent : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EventExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get the user set for the event\n
   * @param event_uid - event uid
   * @return {UserSetExtended}
   */
  getEventUserSet : function(event_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/user_set', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get public alias by path\n
   * @param alias_path - path of the public alias
   * @return {PublicAlias}
   */
  getPublicAliasByPath : function(alias_path) {
    
    

    // check if all required parameters are present
    
    if (typeof alias_path === 'undefined' || alias_path === null) throw new Error('Required parameter "alias_path" is missing');
    

    var op = this.buildOperation('/public_aliases/path/{alias_path}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'alias_path': Serializer.serialize(alias_path, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PublicAlias',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get runtime by path\n
   * @param event_uid - event uid
   * @param content_path - path to access the content
   * @return {PageRuntime}
   */
  getRuntimeByPath : function(event_uid, content_path) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof content_path === 'undefined' || content_path === null) throw new Error('Required parameter "content_path" is missing');
    

    var op = this.buildOperation('/events/{event_uid}/runtimes{/content_path*}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String'), 
      
      'content_path': Serializer.serialize(content_path, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PageRuntime',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get enrollment for an account belonging to this session&#39;s user set\n
   * @param session_uid - the session uid
   * @param account_uid - the account uid
   * @return {EnrollmentExtended}
   */
  getSessionEnrollmentByAccount : function(session_uid, account_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof session_uid === 'undefined' || session_uid === null) throw new Error('Required parameter "session_uid" is missing');
    
    if (typeof account_uid === 'undefined' || account_uid === null) throw new Error('Required parameter "account_uid" is missing');
    

    var op = this.buildOperation('/sessions/{session_uid}/enrollments/{account_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'session_uid': Serializer.serialize(session_uid, 'String'), 
      
      'account_uid': Serializer.serialize(account_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'EnrollmentExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Get a user set with extended details\n
   * @param user_set_uid - user set uid
   * @return {UserSetExtended}
   */
  getUserSet : function(user_set_uid) {
    
    

    // check if all required parameters are present
    
    if (typeof user_set_uid === 'undefined' || user_set_uid === null) throw new Error('Required parameter "user_set_uid" is missing');
    

    var op = this.buildOperation('/user_sets/{user_set_uid}', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'user_set_uid': Serializer.serialize(user_set_uid, 'String')
      
    };
    

    

    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'UserSetExtended',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Record user attendance\n
   * @param event_uid - event uid
   * @param body - request body
   * @return {nil}
   */
  trackAttendance : function(event_uid, body) {
    
    

    // check if all required parameters are present
    
    if (typeof event_uid === 'undefined' || event_uid === null) throw new Error('Required parameter "event_uid" is missing');
    
    if (typeof body === 'undefined' || body === null) throw new Error('Required parameter "body" is missing');
    

    var op = this.buildOperation('/attendances/events/{event_uid}', 'POST');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    
    // path parameters
    op.params = {
      
      'event_uid': Serializer.serialize(event_uid, 'String')
      
    };
    

    

    

    

    
    // http body (model)
    op.body = Serializer.serialize(body, 'TrackAttendanceRequest');
    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: '',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  },
  
  /*
   * 
   * Validates a preview token\n
   * @param token - preivew token
   * @return {PreviewTokenInfo}
   */
  validatePreviewToken : function(token) {
    
    

    // check if all required parameters are present
    
    if (typeof token === 'undefined' || token === null) throw new Error('Required parameter "token" is missing');
    

    var op = this.buildOperation('/preview/validate', 'GET');

    // api mime type
    op.consumes = ['application/json'];
    op.produces = ['application/json'];

    

    

    
    // query parameters
    op.query = {};
    
    op.query['token'] = Serializer.serialize(token, 'String');
    
    
    

    

    

    
    // authentication requirement
    op.auths = [
      { name: 'accessKey', key: 'access_token', in_query: true }
    ];
    

    // response types
    var resp_types = {
      
      200: 'PreviewTokenInfo',
      
      0: 'Error'
      
    };

    // execute the operation
    var op_defer = Deferred();

    op.execute().done(function(result) {
      // deserializes the response body and resolves the promise with the resulting object
      var return_obj = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);
      op_defer.resolve(return_obj);
    }, function(result) {
      if (result instanceof Error) {
        // the request fails without a response from the server and returns an Error

        // extends the Error with a 'messages' property so that
        // it is consistent with the properties of ApiError
        result.messages = [result.message];

        // resolves with the "extended" error object
        op_defer.reject(result);
      } else {
        // the server returns a non-2xx response
        // deserializes the body
        var api_error = Serializer.deserialize(result.body, resp_types[result.statusCode] || resp_types[0]);

        // converts the body content into a ApiError object
        var err_obj = new ApiError(api_error.messages, api_error.fields);
        err_obj.name = api_error.klass_name;

        // resolves the promise with the ApiError object
        op_defer.reject(err_obj);
      }
    });

    return op_defer.promise;
  }
  
};

module.exports = OPERATIONS;

